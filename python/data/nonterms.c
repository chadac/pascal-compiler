int SYNC_n_prog[1] = {T_EOF};
int[] n_prog() {
     int n_prog_1_t;
     switch(tok->tag) {
     case T_PROG:

          int T_PROG_1_entry = match(T_PROG);

          int T_ID_1_entry = match(T_ID);

          int T_LPAR_1_entry = match(T_LPAR);

          int[] n_idtl_1_data = n_idtl();
          int n_idtl_1_t = n_idtl_1_data[0];
          free(n_idtl_1_data);

          int T_RPAR_1_entry = match(T_RPAR);

          int T_SC_1_entry = match(T_SC);

          int[] n_progp_1_data = n_progp();
          int n_progp_1_t = n_progp_1_data[0];
          free(n_progp_1_data);
          int t2;
          if (n_idtl_1_t == TYPE_ERR ) {
               t2 = TYPE_ERR;
          }
          else if (n_progp_1_t == TYPE_ERR ) {
               t2 = TYPE_ERR;
          }
          else {
               t2 = TYPE_VOID;
          }
          n_prog_1_t = t2;
          break;
     default:
          report_error(tok->line, SYNERR_n_prog);
          while(!synch(SYNC_n_prog, 1, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_prog_1_t;
     return values;
}
int SYNC_n_progp[1] = {T_EOF}
int[] n_progp() {
     int n_progp_1_t;
     switch(tok->tag) {
     case T_PROC:

          int[] n_spds_1_data = n_spds();
          int n_spds_1_t = n_spds_1_data[0];
          free(n_spds_1_data);

          int[] n_cmpd_1_data = n_cmpd();
          int n_cmpd_1_t = n_cmpd_1_data[0];
          free(n_cmpd_1_data);

          int T_DOT_1_entry = match(T_DOT);
          int t15;
          if (n_spds_1_t == TYPE_ERR ) {
               t15 = TYPE_ERR;
          }
          else if (n_cmpd_1_t == TYPE_ERR ) {
               t15 = TYPE_ERR;
          }
          else {
               t15 = TYPE_VOID;
          }
          n_progp_1_t = t15;
          break;
     case T_BEG:

          int[] n_cmpd_1_data = n_cmpd();
          int n_cmpd_1_t = n_cmpd_1_data[0];
          free(n_cmpd_1_data);

          int T_DOT_1_entry = match(T_DOT);
          n_progp_1_t = n_cmpd_1_t;
          break;
     case T_VAR:

          int[] n_decl_1_data = n_decl();
          int n_decl_1_t = n_decl_1_data[0];
          free(n_decl_1_data);

          int[] n_progpp_1_data = n_progpp();
          int n_progpp_1_t = n_progpp_1_data[0];
          free(n_progpp_1_data);
          int t31;
          if (n_decl_1_t == TYPE_ERR ) {
               t31 = TYPE_ERR;
          }
          else if (n_progpp_1_t == TYPE_ERR ) {
               t31 = TYPE_ERR;
          }
          else {
               t31 = TYPE_VOID;
          }
          n_progp_1_t = t31;
          break;
     default:
          report_error(tok->line, SYNERR_n_progp);
          while(!synch(SYNC_n_progp, 1, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_progp_1_t;
     return values;
}
int SYNC_n_progpp[1] = {T_EOF}
int[] n_progpp() {
     int n_progpp_1_t;
     switch(tok->tag) {
     case T_PROC:

          int[] n_spds_1_data = n_spds();
          int n_spds_1_t = n_spds_1_data[0];
          free(n_spds_1_data);

          int[] n_cmpd_1_data = n_cmpd();
          int n_cmpd_1_t = n_cmpd_1_data[0];
          free(n_cmpd_1_data);

          int T_DOT_1_entry = match(T_DOT);
          int t44;
          if (n_spds_1_t == TYPE_ERR ) {
               t44 = TYPE_ERR;
          }
          else if (n_cmpd_1_t == TYPE_ERR ) {
               t44 = TYPE_ERR;
          }
          else {
               t44 = TYPE_VOID;
          }
          n_progpp_1_t = t44;
          break;
     case T_BEG:

          int[] n_cmpd_1_data = n_cmpd();
          int n_cmpd_1_t = n_cmpd_1_data[0];
          free(n_cmpd_1_data);

          int T_DOT_1_entry = match(T_DOT);
          n_progpp_1_t = n_cmpd_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_n_progpp);
          while(!synch(SYNC_n_progpp, 1, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_progpp_1_t;
     return values;
}
int SYNC_n_idtl[1] = {T_RPAR}
int[] n_idtl() {
     int n_idtl_1_t;
     switch(tok->tag) {
     case T_ID:

          int T_ID_1_entry = match(T_ID);
          t60 = check\_add\_blue\_node(T_ID_1_entry);
          n_idtlp_1_i = t60;
          int[] n_idtlp_1_data = n_idtlp(n_idtlp_1_i);
          int n_idtlp_1_t = n_idtlp_1_data[0];
          free(n_idtlp_1_data);
          n_idtl_1_t = n_idtlp_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_n_idtl);
          while(!synch(SYNC_n_idtl, 1, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_idtl_1_t;
     return values;
}
int SYNC_n_idtlp[1] = {T_RPAR}
int[] n_idtlp(int n_idtlp_1_i) {
     int n_idtlp_1_t;
     switch(tok->tag) {
     case T_COM:

          int T_COM_1_entry = match(T_COM);

          int T_ID_1_entry = match(T_ID);

          int[] n_idtlp_2_data = n_idtlp(n_idtlp_2_i);
          int n_idtlp_2_t = n_idtlp_2_data[0];
          free(n_idtlp_2_data);
          n_idtlp_1_t = n_idtlp_1_t;
          break;
     case T_RPAR:

          int T_EP_1_entry = match(T_EP);
          n_idtlp_1_t = TYPE_VOID;
          break;
     default:
          report_error(tok->line, SYNERR_n_idtlp);
          while(!synch(SYNC_n_idtlp, 1, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_idtlp_1_t;
     return values;
}
int SYNC_n_decl[2] = {T_PROC, T_BEG}
int[] n_decl() {
     int n_decl_1_t;
     switch(tok->tag) {
     case T_EP:
     case T_VAR:

          int[] n_declp_1_data = n_declp(n_declp_1_offset);
          int n_declp_1_t = n_declp_1_data[0];
          int n_declp_1_totalsize = n_declp_1_data[1];
          free(n_declp_1_data);
          n_decl_1_t = n_declp_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_n_decl);
          while(!synch(SYNC_n_decl, 2, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_decl_1_t;
     return values;
}
int SYNC_n_declp[2] = {T_PROC, T_BEG}
int[] n_declp(int n_declp_1_offset) {
     int n_declp_1_t;
     int n_declp_1_totalsize;
     switch(tok->tag) {
     case T_VAR:

          int T_VAR_1_entry = match(T_VAR);

          int T_ID_1_entry = match(T_ID);

          int T_C_1_entry = match(T_C);

          int[] n_type_1_data = n_type();
          int n_type_1_t = n_type_1_data[0];
          int n_type_1_width = n_type_1_data[1];
          free(n_type_1_data);

          int T_SC_1_entry = match(T_SC);

          int[] n_declp_2_data = n_declp(n_declp_2_offset);
          int n_declp_2_t = n_declp_2_data[0];
          int n_declp_2_totalsize = n_declp_2_data[1];
          free(n_declp_2_data);
          t76 = add_type(T_ID_1_entry, n_type_1_t);

          t79 = check_add_blue_node(T_ID_1_entry, n_type_1_t);

          n_declp_1_t = n_declp_1_t;
          n_declp_1_totalsize = n_declp_1_totalsize;
          break;
     case T_PROC:
     case T_BEG:

          int T_EP_1_entry = match(T_EP);
          n_declp_1_totalsize = n_declp_1_offset;
          n_declp_1_t = TYPE_VOID;
          break;
     default:
          report_error(tok->line, SYNERR_n_declp);
          while(!synch(SYNC_n_declp, 2, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 2);
     values[0] = n_declp_1_t;
     values[1] = n_declp_1_totalsize;
     return values;
}
int SYNC_n_type[2] = {T_SC, T_RPAR}
int[] n_type() {
     int n_type_1_t;
     int n_type_1_width;
     switch(tok->tag) {
     case T_INT:
     case T_REAL:

          int[] n_stdt_1_data = n_stdt();
          int n_stdt_1_t = n_stdt_1_data[0];
          int n_stdt_1_width = n_stdt_1_data[1];
          free(n_stdt_1_data);
          n_type_1_t = n_stdt_1_t;
          n_type_1_width = n_stdt_1_width;
          break;
     case T_ARR:

          int T_ARR_1_entry = match(T_ARR);

          int T_LBR_1_entry = match(T_LBR);

          int T_NUM_1_entry = match(T_NUM);

          int T_DD_1_entry = match(T_DD);

          int T_NUM_2_entry = match(T_NUM);

          int T_RBR_1_entry = match(T_RBR);

          int T_OF_1_entry = match(T_OF);

          int[] n_stdt_1_data = n_stdt();
          int n_stdt_1_t = n_stdt_1_data[0];
          int n_stdt_1_width = n_stdt_1_data[1];
          free(n_stdt_1_data);
          int t102;
          if (n_stdt_1_t == TYPE_INT ) {
               t102 = TYPE_AINT;
          }
          else if (n_stdt_1_t == TYPE_REAL ) {
               t102 = TYPE_AREAL;
          }
          else {
               t102 = TYPE_ERR;
          }
          n_type_1_t = t102;
          n_type_1_width = n_stdt_1_width;
          break;
     default:
          report_error(tok->line, SYNERR_n_type);
          while(!synch(SYNC_n_type, 2, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 2);
     values[0] = n_type_1_t;
     values[1] = n_type_1_width;
     return values;
}
int SYNC_n_stdt[2] = {T_SC, T_RPAR}
int[] n_stdt() {
     int n_stdt_1_t;
     int n_stdt_1_width;
     switch(tok->tag) {
     case T_INT:

          int T_INT_1_entry = match(T_INT);
          n_stdt_1_t = TYPE_INT;
          n_stdt_1_width = 4;
          break;
     case T_REAL:

          int T_REAL_1_entry = match(T_REAL);
          n_stdt_1_t = TYPE_REAL;
          n_stdt_1_width = 8;
          break;
     default:
          report_error(tok->line, SYNERR_n_stdt);
          while(!synch(SYNC_n_stdt, 2, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 2);
     values[0] = n_stdt_1_t;
     values[1] = n_stdt_1_width;
     return values;
}
int SYNC_n_spds[1] = {T_BEG}
int[] n_spds() {
     int n_spds_1_t;
     switch(tok->tag) {
     case T_PROC:
     case T_EP:

          int[] n_spdsp_1_data = n_spdsp();
          int n_spdsp_1_t = n_spdsp_1_data[0];
          free(n_spdsp_1_data);
          n_spds_1_t = n_spdsp_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_n_spds);
          while(!synch(SYNC_n_spds, 1, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_spds_1_t;
     return values;
}
int SYNC_n_spdsp[1] = {T_BEG}
int[] n_spdsp() {
     int n_spdsp_1_t;
     switch(tok->tag) {
     case T_PROC:

          int[] n_spd_1_data = n_spd();
          int n_spd_1_t = n_spd_1_data[0];
          free(n_spd_1_data);

          int T_SC_1_entry = match(T_SC);

          int[] n_spdsp_2_data = n_spdsp();
          int n_spdsp_2_t = n_spdsp_2_data[0];
          free(n_spdsp_2_data);
          int t133;
          if (n_spd_1_t == TYPE_ERR ) {
               t133 = TYPE_ERR;
          }
          else if (n_spdsp_1_t == TYPE_ERR ) {
               t133 = TYPE_ERR;
          }
          else {
               t133 = TYPE_VOID;
          }
          n_spdsp_1_t = t133;
          break;
     case T_BEG:

          int T_EP_1_entry = match(T_EP);
          n_spdsp_1_t = TYPE_VOID;
          break;
     default:
          report_error(tok->line, SYNERR_n_spdsp);
          while(!synch(SYNC_n_spdsp, 1, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_spdsp_1_t;
     return values;
}
int SYNC_n_spd[1] = {T_SC}
int[] n_spd() {
     int n_spd_1_t;
     switch(tok->tag) {
     case T_PROC:

          int[] n_sph_1_data = n_sph();
          int n_sph_1_t = n_sph_1_data[0];
          free(n_sph_1_data);

          int[] n_spdp_1_data = n_spdp();
          int n_spdp_1_t = n_spdp_1_data[0];
          free(n_spdp_1_data);
          int t149;
          if (n_sph_1_t == TYPE_ERR ) {
               t149 = TYPE_ERR;
          }
          else if (n_spdp_1_t == TYPE_ERR ) {
               t149 = TYPE_ERR;
          }
          else {
               t149 = TYPE_VOID;
          }
          n_spd_1_t = t149;
          break;
     default:
          report_error(tok->line, SYNERR_n_spd);
          while(!synch(SYNC_n_spd, 1, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_spd_1_t;
     return values;
}
int SYNC_n_spdp[1] = {T_SC}
int[] n_spdp() {
     int n_spdp_1_t;
     switch(tok->tag) {
     case T_PROC:

          int[] n_spds_1_data = n_spds();
          int n_spds_1_t = n_spds_1_data[0];
          free(n_spds_1_data);

          int[] n_cmpd_1_data = n_cmpd();
          int n_cmpd_1_t = n_cmpd_1_data[0];
          free(n_cmpd_1_data);
          int t162;
          if (n_spds_1_t == TYPE_ERR ) {
               t162 = TYPE_ERR;
          }
          else if (n_cmpd_1_t == TYPE_ERR ) {
               t162 = TYPE_ERR;
          }
          else {
               t162 = TYPE_VOID;
          }
          n_spdp_1_t = t162;
          break;
     case T_BEG:

          int[] n_cmpd_1_data = n_cmpd();
          int n_cmpd_1_t = n_cmpd_1_data[0];
          free(n_cmpd_1_data);
          n_spdp_1_t = n_cmpd_1_t;
          break;
     case T_VAR:

          int[] n_decl_1_data = n_decl();
          int n_decl_1_t = n_decl_1_data[0];
          free(n_decl_1_data);

          int[] n_spdpp_1_data = n_spdpp();
          int n_spdpp_1_t = n_spdpp_1_data[0];
          free(n_spdpp_1_data);
          int t178;
          if (n_decl_1_t == TYPE_ERR ) {
               t178 = TYPE_ERR;
          }
          else if (n_spdpp_1_t == TYPE_ERR ) {
               t178 = TYPE_ERR;
          }
          else {
               t178 = TYPE_VOID;
          }
          n_spdp_1_t = t178;
          break;
     default:
          report_error(tok->line, SYNERR_n_spdp);
          while(!synch(SYNC_n_spdp, 1, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_spdp_1_t;
     return values;
}
int SYNC_n_spdpp[1] = {T_SC}
int[] n_spdpp() {
     int n_spdpp_1_t;
     switch(tok->tag) {
     case T_PROC:

          int[] n_spds_1_data = n_spds();
          int n_spds_1_t = n_spds_1_data[0];
          free(n_spds_1_data);

          int[] n_cmpd_1_data = n_cmpd();
          int n_cmpd_1_t = n_cmpd_1_data[0];
          free(n_cmpd_1_data);
          int t191;
          if (n_spds_1_t == TYPE_ERR ) {
               t191 = TYPE_ERR;
          }
          else if (n_cmpd_1_t == TYPE_ERR ) {
               t191 = TYPE_ERR;
          }
          else {
               t191 = TYPE_VOID;
          }
          n_spdpp_1_t = t191;
          break;
     case T_BEG:

          int[] n_cmpd_1_data = n_cmpd();
          int n_cmpd_1_t = n_cmpd_1_data[0];
          free(n_cmpd_1_data);
          n_spdpp_1_t = n_cmpd_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_n_spdpp);
          while(!synch(SYNC_n_spdpp, 1, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_spdpp_1_t;
     return values;
}
int SYNC_n_sph[3] = {T_PROC, T_BEG, T_VAR}
int[] n_sph() {
     int n_sph_1_t;
     switch(tok->tag) {
     case T_PROC:

          int T_PROC_1_entry = match(T_PROC);

          int T_ID_1_entry = match(T_ID);
          t207 = check\_add\_blue\_node(T_ID_1_value);
          n_sphp_1_i = t207;
          int[] n_sphp_1_data = n_sphp(n_sphp_1_i);
          int n_sphp_1_t = n_sphp_1_data[0];
          free(n_sphp_1_data);
          n_sph_1_t = n_sphp_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_n_sph);
          while(!synch(SYNC_n_sph, 3, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_sph_1_t;
     return values;
}
int SYNC_n_sphp[3] = {T_PROC, T_BEG, T_VAR}
int[] n_sphp(int n_sphp_1_i) {
     int n_sphp_1_t;
     switch(tok->tag) {
     case T_LPAR:

          int[] n_args_1_data = n_args();
          int n_args_1_t = n_args_1_data[0];
          free(n_args_1_data);

          int T_SC_1_entry = match(T_SC);
          n_sphp_1_t = n_args_1_t;
          break;
     case T_SC:

          int T_SC_1_entry = match(T_SC);
          n_sphp_1_t = TYPE_VOID;
          break;
     default:
          report_error(tok->line, SYNERR_n_sphp);
          while(!synch(SYNC_n_sphp, 3, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_sphp_1_t;
     return values;
}
int SYNC_n_args[1] = {T_SC}
int[] n_args() {
     int n_args_1_t;
     switch(tok->tag) {
     case T_LPAR:

          int T_LPAR_1_entry = match(T_LPAR);

          int[] n_plst_1_data = n_plst(n_plst_1_i);
          int n_plst_1_t = n_plst_1_data[0];
          free(n_plst_1_data);

          int T_RPAR_1_entry = match(T_RPAR);
          n_args_1_t = n_plst_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_n_args);
          while(!synch(SYNC_n_args, 1, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_args_1_t;
     return values;
}
int SYNC_n_plst[1] = {T_RPAR}
int[] n_plst(int n_plst_1_i) {
     int n_plst_1_t;
     switch(tok->tag) {
     case T_ID:

          int T_ID_1_entry = match(T_ID);

          int T_C_1_entry = match(T_C);

          int[] n_type_1_data = n_type();
          int n_type_1_t = n_type_1_data[0];
          int n_type_1_width = n_type_1_data[1];
          free(n_type_1_data);
          t223 = push\_param\_node(n_type_1_t);
          n_plstp_1_i = t223;
          int[] n_plstp_1_data = n_plstp(n_plstp_1_i);
          int n_plstp_1_t = n_plstp_1_data[0];
          free(n_plstp_1_data);
          int t227;
          if (n_plst_1_i == TYPE_ERR ) {
               t227 = TYPE_ERR;
          }
          else {
               t227 = n_plstp_1_t;
          }
          n_plst_1_t = t227;
          break;
     default:
          report_error(tok->line, SYNERR_n_plst);
          while(!synch(SYNC_n_plst, 1, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_plst_1_t;
     return values;
}
int SYNC_n_plstp[1] = {T_RPAR}
int[] n_plstp(int n_plstp_1_i) {
     int n_plstp_1_t;
     switch(tok->tag) {
     case T_SC:

          int T_SC_1_entry = match(T_SC);

          int T_ID_1_entry = match(T_ID);

          int T_C_1_entry = match(T_C);

          int[] n_type_1_data = n_type();
          int n_type_1_t = n_type_1_data[0];
          int n_type_1_width = n_type_1_data[1];
          free(n_type_1_data);
          t236 = push\_param\_node(n_type_1_t);
          n_plstp_1_i = t236;
          int[] n_plstp_2_data = n_plstp(n_plstp_2_i);
          int n_plstp_2_t = n_plstp_2_data[0];
          free(n_plstp_2_data);
          int t240;
          if (n_plst_1_i == TYPE_ERR ) {
               t240 = TYPE_ERR;
          }
          else {
               t240 = n_plstp_1_t;
          }
          n_plstp_1_t = t240;
          break;
     case T_RPAR:

          int T_EP_1_entry = match(T_EP);
          n_plstp_1_t = TYPE_VOID;
          break;
     default:
          report_error(tok->line, SYNERR_n_plstp);
          while(!synch(SYNC_n_plstp, 1, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_plstp_1_t;
     return values;
}
int SYNC_n_cmpd[4] = {T_SC, T_END, T_EL, T_DOT}
int[] n_cmpd() {
     int n_cmpd_1_t;
     switch(tok->tag) {
     case T_BEG:

          int T_BEG_1_entry = match(T_BEG);

          int[] n_cmpdp_1_data = n_cmpdp();
          int n_cmpdp_1_t = n_cmpdp_1_data[0];
          free(n_cmpdp_1_data);
          n_cmpd_1_t = n_cmpdp_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_n_cmpd);
          while(!synch(SYNC_n_cmpd, 4, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_cmpd_1_t;
     return values;
}
int SYNC_n_cmpdp[4] = {T_SC, T_DOT, T_EL, T_END}
int[] n_cmpdp() {
     int n_cmpdp_1_t;
     switch(tok->tag) {
     case T_CALL:
     case T_WH:
     case T_BEG:
     case T_ID:
     case T_IF:

          int[] n_ostmt_1_data = n_ostmt();
          int n_ostmt_1_t = n_ostmt_1_data[0];
          free(n_ostmt_1_data);

          int T_END_1_entry = match(T_END);
          n_cmpdp_1_t = n_ostmt_1_t;
          break;
     case T_END:

          int T_END_1_entry = match(T_END);
          n_cmpdp_1_t = TYPE_VOID;
          break;
     default:
          report_error(tok->line, SYNERR_n_cmpdp);
          while(!synch(SYNC_n_cmpdp, 4, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_cmpdp_1_t;
     return values;
}
int SYNC_n_ostmt[1] = {T_END}
int[] n_ostmt() {
     int n_ostmt_1_t;
     switch(tok->tag) {
     case T_CALL:
     case T_WH:
     case T_BEG:
     case T_ID:
     case T_IF:

          int[] n_stmtlst_1_data = n_stmtlst();
          int n_stmtlst_1_t = n_stmtlst_1_data[0];
          free(n_stmtlst_1_data);
          n_ostmt_1_t = n_stmtlst_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_n_ostmt);
          while(!synch(SYNC_n_ostmt, 1, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_ostmt_1_t;
     return values;
}
int SYNC_n_stmtlst[1] = {T_END}
int[] n_stmtlst() {
     int n_stmtlst_1_t;
     switch(tok->tag) {
     case T_CALL:
     case T_WH:
     case T_BEG:
     case T_ID:
     case T_IF:

          int[] n_stmt_1_data = n_stmt();
          int n_stmt_1_t = n_stmt_1_data[0];
          free(n_stmt_1_data);

          int[] n_stmtlstp_1_data = n_stmtlstp();
          int n_stmtlstp_1_t = n_stmtlstp_1_data[0];
          free(n_stmtlstp_1_data);
          int t264;
          if (n_stmt_1_t == TYPE_ERR ) {
               t264 = TYPE_ERR;
          }
          else {
               t264 = n_stmtlstp_1_t;
          }
          n_stmtlst_1_t = t264;
          break;
     default:
          report_error(tok->line, SYNERR_n_stmtlst);
          while(!synch(SYNC_n_stmtlst, 1, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_stmtlst_1_t;
     return values;
}
int SYNC_n_stmtlstp[1] = {T_END}
int[] n_stmtlstp() {
     int n_stmtlstp_1_t;
     switch(tok->tag) {
     case T_SC:

          int T_SC_1_entry = match(T_SC);

          int[] n_stmt_1_data = n_stmt();
          int n_stmt_1_t = n_stmt_1_data[0];
          free(n_stmt_1_data);

          int[] n_stmtlstp_2_data = n_stmtlstp();
          int n_stmtlstp_2_t = n_stmtlstp_2_data[0];
          free(n_stmtlstp_2_data);
          int t273;
          if (n_stmt_1_t == TYPE_ERR ) {
               t273 = TYPE_ERR;
          }
          else {
               t273 = n_stmtlstp_1_t;
          }
          n_stmtlstp_1_t = t273;
          break;
     case T_END:

          int T_EP_1_entry = match(T_EP);
          n_stmtlstp_1_t = TYPE_VOID;
          break;
     default:
          report_error(tok->line, SYNERR_n_stmtlstp);
          while(!synch(SYNC_n_stmtlstp, 1, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_stmtlstp_1_t;
     return values;
}
int SYNC_n_stmt[3] = {T_SC, T_EL, T_END}
int[] n_stmt() {
     int n_stmt_1_t;
     switch(tok->tag) {
     case T_ID:

          int[] n_var_1_data = n_var();
          int n_var_1_t = n_var_1_data[0];
          free(n_var_1_data);

          int T_ASS_1_entry = match(T_ASS);

          int[] n_expr_1_data = n_expr();
          int n_expr_1_t = n_expr_1_data[0];
          free(n_expr_1_data);
          int t285;
          if (n_var_1_t == n_expr_1_t ) {
               t285 = TYPE_VOID;
          }
          else {
               t285 = TYPE_ERR;
          }
          n_stmt_1_t = t285;
          break;
     case T_CALL:

          int[] n_proc_1_data = n_proc(n_proc_1_i);
          int n_proc_1_t = n_proc_1_data[0];
          free(n_proc_1_data);
          n_stmt_1_t = n_proc_1_t;
          break;
     case T_BEG:

          int[] n_cmpd_1_data = n_cmpd();
          int n_cmpd_1_t = n_cmpd_1_data[0];
          free(n_cmpd_1_data);

          break;
     case T_WH:

          int T_WH_1_entry = match(T_WH);

          int[] n_expr_1_data = n_expr();
          int n_expr_1_t = n_expr_1_data[0];
          free(n_expr_1_data);

          int T_DO_1_entry = match(T_DO);

          int[] n_stmt_2_data = n_stmt();
          int n_stmt_2_t = n_stmt_2_data[0];
          free(n_stmt_2_data);
          int t300;
          if (n_expr_1_t == TYPE_BOOLEAN ) {
               t300 = n_stmt_1_t;
          }
          else {
               t300 = TYPE_ERR;
          }
          n_stmt_1_t = t300;
          break;
     case T_IF:

          int T_IF_1_entry = match(T_IF);

          int[] n_expr_1_data = n_expr();
          int n_expr_1_t = n_expr_1_data[0];
          free(n_expr_1_data);

          int T_TH_1_entry = match(T_TH);

          int[] n_stmt_2_data = n_stmt();
          int n_stmt_2_t = n_stmt_2_data[0];
          free(n_stmt_2_data);
          n_stmtp_1_i = n_stmt_1_t;
          int[] n_stmtp_1_data = n_stmtp(n_stmtp_1_i);
          int n_stmtp_1_t = n_stmtp_1_data[0];
          free(n_stmtp_1_data);
          int t312;
          if (n_expr_1_t == TYPE_BOOLEAN ) {
               t312 = n_stmtp_1_t;
          }
          else {
               t312 = TYPE_ERR;
          }
          n_stmt_1_t = t312;
          break;
     default:
          report_error(tok->line, SYNERR_n_stmt);
          while(!synch(SYNC_n_stmt, 3, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_stmt_1_t;
     return values;
}
int SYNC_n_stmtp[3] = {T_SC, T_EL, T_END}
int[] n_stmtp(int n_stmtp_1_i) {
     int n_stmtp_1_t;
     switch(tok->tag) {
     case T_SC:
     case T_EL:
     case T_END:

          int T_EP_1_entry = match(T_EP);
          n_stmtp_1_t = n_stmtp_1_i;
          break;
     case T_EL:

          int T_EL_1_entry = match(T_EL);

          int[] n_stmt_1_data = n_stmt();
          int n_stmt_1_t = n_stmt_1_data[0];
          free(n_stmt_1_data);
          int t324;
          if (n_stmt_1_t == n_stmtp_1_i ) {
               t324 = n_stmt_1_t;
          }
          else {
               t324 = TYPE_ERR;
          }
          n_stmtp_1_t = t324;
          break;
     default:
          report_error(tok->line, SYNERR_n_stmtp);
          while(!synch(SYNC_n_stmtp, 3, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_stmtp_1_t;
     return values;
}
int SYNC_n_var[1] = {T_ASS}
int[] n_var() {
     int n_var_1_t;
     switch(tok->tag) {
     case T_ID:

          int T_ID_1_entry = match(T_ID);
          t333 = lookup(T_ID_1_entry);
          n_varp_1_i = t333;
          int[] n_varp_1_data = n_varp(n_varp_1_i);
          int n_varp_1_t = n_varp_1_data[0];
          free(n_varp_1_data);
          n_var_1_t = n_varp_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_n_var);
          while(!synch(SYNC_n_var, 1, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_var_1_t;
     return values;
}
int SYNC_n_varp[1] = {T_ASS}
int[] n_varp(int n_varp_1_i) {
     int n_varp_1_t;
     switch(tok->tag) {
     case T_ASS:

          int T_EP_1_entry = match(T_EP);
          n_varp_1_t = n_varp_1_i;
          break;
     case T_LBR:

          int T_LBR_1_entry = match(T_LBR);

          int[] n_expr_1_data = n_expr();
          int n_expr_1_t = n_expr_1_data[0];
          free(n_expr_1_data);

          int T_RBR_1_entry = match(T_RBR);
          int t343;
          if (n_varp_1_i == TYPE_AINT && n_expr_1_t == TYPE_INT ) {
               t343 = TYPE_INT;
          }
          else if (n_varp_1_i == TYPE_AREAL && n_expr_1_t == TYPE_INT ) {
               t343 = TYPE_REAL;
          }
          else if (n_varp_1_i != TYPE_AINT ) {
               t343 = TYPE_ERR;
          }
          else if (n_varp_1_i != TYPE_AREAL ) {
               t343 = TYPE_ERR;
          }
          else if (n_expr_1_t != TYPE_INT ) {
               t343 = TYPE_ERR;
          }
          else {
               t343 = TYPE_ERR;
          }
          n_varp_1_t = t343;
          break;
     default:
          report_error(tok->line, SYNERR_n_varp);
          while(!synch(SYNC_n_varp, 1, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_varp_1_t;
     return values;
}
int SYNC_n_proc[3] = {T_SC, T_EL, T_END}
int[] n_proc(int n_proc_1_i) {
     int n_proc_1_t;
     switch(tok->tag) {
     case T_CALL:

          int T_CALL_1_entry = match(T_CALL);

          int T_ID_1_entry = match(T_ID);
          t372 = lookup(T_ID_1_entry);
          n_proc_1_i = t372;
          int[] n_procp_1_data = n_procp();
          int n_procp_1_t = n_procp_1_data[0];
          free(n_procp_1_data);
          n_proc_1_t = n_procp_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_n_proc);
          while(!synch(SYNC_n_proc, 3, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_proc_1_t;
     return values;
}
int SYNC_n_procp[3] = {T_SC, T_EL, T_END}
int[] n_procp() {
     int n_procp_1_t;
     switch(tok->tag) {
     case T_SC:
     case T_EL:
     case T_END:

          int T_EP_1_entry = match(T_EP);
          n_procp_1_t = TYPE_VOID;
          break;
     case T_LPAR:

          int T_LPAR_1_entry = match(T_LPAR);

          int[] n_exprl_1_data = n_exprl();
          int n_exprl_1_t = n_exprl_1_data[0];
          free(n_exprl_1_data);
          n_procp_1_t = n_exprl_1_t;
          int T_RPAR_1_entry = match(T_RPAR);

          break;
     default:
          report_error(tok->line, SYNERR_n_procp);
          while(!synch(SYNC_n_procp, 3, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_procp_1_t;
     return values;
}
int SYNC_n_exprl[1] = {T_RPAR}
int[] n_exprl() {
     int n_exprl_1_t;
     switch(tok->tag) {
     case T_PLS:
     case T_NOT:
     case T_ID:
     case T_NUM:
     case T_MIN:

          int[] n_expr_1_data = n_expr();
          int n_expr_1_t = n_expr_1_data[0];
          free(n_expr_1_data);

          int[] n_exprlp_1_data = n_exprlp();
          int n_exprlp_1_t = n_exprlp_1_data[0];
          free(n_exprlp_1_data);

          break;
     default:
          report_error(tok->line, SYNERR_n_exprl);
          while(!synch(SYNC_n_exprl, 1, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_exprl_1_t;
     return values;
}
int SYNC_n_exprlp[1] = {T_RPAR}
int[] n_exprlp() {
     int n_exprlp_1_t;
     switch(tok->tag) {
     case T_COM:

          int T_COM_1_entry = match(T_COM);

          int[] n_expr_1_data = n_expr();
          int n_expr_1_t = n_expr_1_data[0];
          free(n_expr_1_data);

          int[] n_exprlp_2_data = n_exprlp();
          int n_exprlp_2_t = n_exprlp_2_data[0];
          free(n_exprlp_2_data);
          int t399;
          if (n_expr_1_t == TYPE_ERR ) {
               t399 = TYPE_ERR;
          }
          else if (t406 != n_expr_1_t ) {
               t399 = TYPE_ERR;
          }
          else {
               t399 = n_exprlp_1_t;
          }
          n_exprl_1_t = t399;
          break;
     case T_RPAR:

          int T_EP_1_entry = match(T_EP);
          n_exprlp_1_t = TYPE_VOID;
          break;
     default:
          report_error(tok->line, SYNERR_n_exprlp);
          while(!synch(SYNC_n_exprlp, 1, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_exprlp_1_t;
     return values;
}
int SYNC_n_expr[8] = {T_SC, T_RBR, T_RPAR, T_END, T_TH, T_COM, T_DO, T_EL}
int[] n_expr() {
     int n_expr_1_t;
     switch(tok->tag) {
     case T_PLS:
     case T_NOT:
     case T_ID:
     case T_NUM:
     case T_MIN:

          int[] n_sexpr_1_data = n_sexpr();
          int n_sexpr_1_t = n_sexpr_1_data[0];
          free(n_sexpr_1_data);
          n_exprp_1_i = n_sexpr_1_t;
          int[] n_exprp_1_data = n_exprp(n_exprp_1_i);
          int n_exprp_1_t = n_exprp_1_data[0];
          free(n_exprp_1_data);

          break;
     default:
          report_error(tok->line, SYNERR_n_expr);
          while(!synch(SYNC_n_expr, 8, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_expr_1_t;
     return values;
}
int SYNC_n_exprp[8] = {T_SC, T_RBR, T_TH, T_END, T_RPAR, T_COM, T_DO, T_EL}
int[] n_exprp(int n_exprp_1_i) {
     int n_exprp_1_t;
     switch(tok->tag) {
     case T_SC:
     case T_RBR:
     case T_TH:
     case T_END:
     case T_RPAR:
     case T_COM:
     case T_DO:
     case T_EL:

          int T_EP_1_entry = match(T_EP);
          n_exprp_1_t = TYPE_VOID;
          break;
     case T_REL:

          int T_REL_1_entry = match(T_REL);

          int[] n_sexpr_1_data = n_sexpr();
          int n_sexpr_1_t = n_sexpr_1_data[0];
          free(n_sexpr_1_data);
          int t422;
          if (n_exprp_1_i == TYPE_INT && n_sexpr_1_t == TYPE_INT ) {
               t422 = TYPE_BOOLEAN;
          }
          else if (n_exprp_1_i == TYPE_REAL && n_sexpr_1_t == TYPE_REAL ) {
               t422 = TYPE_BOOLEAN;
          }
          else if (n_exprp_1_i == TYPE_REAL && n_sexpr_1_t == TYPE_INT ) {
               t422 = TYPE_BOOLEAN;
          }
          else if (n_exprp_1_i == TYPE_INT && n_sexpr_1_t == TYPE_REAL ) {
               t422 = TYPE_BOOLEAN;
          }
          else {
               t422 = TYPE_ERR;
          }
          n_exprp_1_t = t422;
          break;
     default:
          report_error(tok->line, SYNERR_n_exprp);
          while(!synch(SYNC_n_exprp, 8, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_exprp_1_t;
     return values;
}
int SYNC_n_sexpr[9] = {T_REL, T_SC, T_RBR, T_TH, T_END, T_RPAR, T_COM, T_DO, T_EL}
int[] n_sexpr() {
     int n_sexpr_1_t;
     switch(tok->tag) {
     case T_NOT:
     case T_NUM:
     case T_ID:

          int[] n_term_1_data = n_term();
          int n_term_1_t = n_term_1_data[0];
          free(n_term_1_data);
          n_sexprp_1_i = n_term_1_t;
          int[] n_sexprp_1_data = n_sexprp(n_sexprp_1_i);
          int n_sexprp_1_t = n_sexprp_1_data[0];
          free(n_sexprp_1_data);
          n_sexpr_1_t = n_sexprp_1_t;
          break;
     case T_PLS:
     case T_MIN:

          int[] n_sign_1_data = n_sign();
          free(n_sign_1_data);
          n_sexprp_1_i = n_term_1_t;
          int[] n_term_1_data = n_term();
          int n_term_1_t = n_term_1_data[0];
          free(n_term_1_data);
          n_sexpr_1_t = n_sexprp_1_t;
          int[] n_sexprp_1_data = n_sexprp(n_sexprp_1_i);
          int n_sexprp_1_t = n_sexprp_1_data[0];
          free(n_sexprp_1_data);

          break;
     default:
          report_error(tok->line, SYNERR_n_sexpr);
          while(!synch(SYNC_n_sexpr, 9, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_sexpr_1_t;
     return values;
}
int SYNC_n_sexprp[9] = {T_REL, T_SC, T_RBR, T_RPAR, T_END, T_TH, T_COM, T_DO, T_EL}
int[] n_sexprp(int n_sexprp_1_i) {
     int n_sexprp_1_t;
     switch(tok->tag) {
     case T_ADD:

          int T_ADD_1_entry = match(T_ADD);

          int[] n_term_1_data = n_term();
          int n_term_1_t = n_term_1_data[0];
          free(n_term_1_data);

          int[] n_sexprp_2_data = n_sexprp(n_sexprp_2_i);
          int n_sexprp_2_t = n_sexprp_2_data[0];
          free(n_sexprp_2_data);

          break;
     case T_REL:
     case T_SC:
     case T_RBR:
     case T_RPAR:
     case T_END:
     case T_TH:
     case T_COM:
     case T_DO:
     case T_EL:

          int T_EP_1_entry = match(T_EP);
          n_sexprp_1_t = n_sexprp_1_i;
          break;
     default:
          report_error(tok->line, SYNERR_n_sexprp);
          while(!synch(SYNC_n_sexprp, 9, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_sexprp_1_t;
     return values;
}
int SYNC_n_term[10] = {T_REL, T_ADD, T_SC, T_RBR, T_RPAR, T_END, T_TH, T_COM, T_DO, T_EL}
int[] n_term() {
     int n_term_1_t;
     switch(tok->tag) {
     case T_NUM:
     case T_NOT:
     case T_ID:

          int[] n_fac_1_data = n_fac();
          int n_fac_1_t = n_fac_1_data[0];
          free(n_fac_1_data);
          n_termp_1_i = n_fac_1_t;
          int[] n_termp_1_data = n_termp(n_termp_1_i);
          int n_termp_1_t = n_termp_1_data[0];
          free(n_termp_1_data);
          n_term_1_t = n_termp_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_n_term);
          while(!synch(SYNC_n_term, 10, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_term_1_t;
     return values;
}
int SYNC_n_termp[10] = {T_REL, T_ADD, T_SC, T_RBR, T_TH, T_END, T_RPAR, T_COM, T_DO, T_EL}
int[] n_termp(int n_termp_1_i) {
     int n_termp_1_t;
     switch(tok->tag) {
     case T_MUL:

          int T_MUL_1_entry = match(T_MUL);

          int[] n_fac_1_data = n_fac();
          int n_fac_1_t = n_fac_1_data[0];
          free(n_fac_1_data);

          int[] n_termp_2_data = n_termp(n_termp_2_i);
          int n_termp_2_t = n_termp_2_data[0];
          free(n_termp_2_data);

          break;
     case T_REL:
     case T_ADD:
     case T_SC:
     case T_RBR:
     case T_TH:
     case T_END:
     case T_RPAR:
     case T_COM:
     case T_DO:
     case T_EL:

          int T_EP_1_entry = match(T_EP);
          n_termp_1_t = n_termp_1_i;
          break;
     default:
          report_error(tok->line, SYNERR_n_termp);
          while(!synch(SYNC_n_termp, 10, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_termp_1_t;
     return values;
}
int SYNC_n_fac[11] = {T_SC, T_ADD, T_REL, T_RBR, T_TH, T_END, T_RPAR, T_COM, T_DO, T_MUL, T_EL}
int[] n_fac() {
     int n_fac_1_t;
     switch(tok->tag) {
     case T_NUM:

          int T_NUM_1_entry = match(T_NUM);
          t479 = lookup(T_NUM_1_entry);
          n_fac_1_t = t479;
          break;
     case T_NOT:

          int T_NOT_1_entry = match(T_NOT);

          int[] n_fac_2_data = n_fac();
          int n_fac_2_t = n_fac_2_data[0];
          free(n_fac_2_data);

          break;
     case T_ID:

          int T_ID_1_entry = match(T_ID);
          t484 = lookup(T_ID_1_entry);
          n_facp_1_i = t484;
          int[] n_facp_1_data = n_facp(n_facp_1_i);
          int n_facp_1_t = n_facp_1_data[0];
          free(n_facp_1_data);
          n_fac_1_t = n_facp_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_n_fac);
          while(!synch(SYNC_n_fac, 11, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_fac_1_t;
     return values;
}
int SYNC_n_facp[11] = {T_SC, T_ADD, T_REL, T_RBR, T_RPAR, T_END, T_TH, T_COM, T_DO, T_MUL, T_EL}
int[] n_facp(int n_facp_1_i) {
     int n_facp_1_t;
     switch(tok->tag) {
     case T_SC:
     case T_ADD:
     case T_REL:
     case T_RBR:
     case T_RPAR:
     case T_END:
     case T_TH:
     case T_COM:
     case T_DO:
     case T_MUL:
     case T_EL:

          int T_EP_1_entry = match(T_EP);
          n_facp_1_t = n_facp_1_i;
          break;
     case T_LPAR:

          int T_LPAR_1_entry = match(T_LPAR);

          int[] n_exprl_1_data = n_exprl();
          int n_exprl_1_t = n_exprl_1_data[0];
          free(n_exprl_1_data);

          int T_RPAR_1_entry = match(T_RPAR);
          int t494;
          if (n_exprlp_1_t == TYPE_VOID ) {
               t494 = n_facp_1_i;
          }
          else {
               t494 = TYPE_ERR;
          }
          n_facp_1_t = t494;
          break;
     case T_LBR:

          int T_LBR_1_entry = match(T_LBR);

          int[] n_expr_1_data = n_expr();
          int n_expr_1_t = n_expr_1_data[0];
          free(n_expr_1_data);

          int T_RBR_1_entry = match(T_RBR);
          int t503;
          if (n_facp_1_i == TYPE_AINT && n_expr_1_t == TYPE_INT ) {
               t503 = TYPE_INT;
          }
          else if (n_facp_1_i == TYPE_AREAL && n_expr_1_t == TYPE_INT ) {
               t503 = TYPE_REAL;
          }
          else if (n_facp_1_i != TYPE_AINT ) {
               t503 = TYPE_ERR;
          }
          else if (n_facp_1_i != TYPE_AREAL ) {
               t503 = TYPE_ERR;
          }
          else if (n_expr_1_t != TYPE_INT ) {
               t503 = TYPE_ERR;
          }
          else {
               t503 = TYPE_ERR;
          }
          n_facp_1_t = t503;
          break;
     default:
          report_error(tok->line, SYNERR_n_facp);
          while(!synch(SYNC_n_facp, 11, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_facp_1_t;
     return values;
}
int SYNC_n_sign[3] = {T_NOT, T_NUM, T_ID}
int[] n_sign() {
     switch(tok->tag) {
     case T_PLS:

          int T_PLS_1_entry = match(T_PLS);

          break;
     case T_MIN:

          int T_MIN_1_entry = match(T_MIN);

          break;
     default:
          report_error(tok->line, SYNERR_n_sign);
          while(!synch(SYNC_n_sign, 3, tok->tag))
               tok = get_token();
     }
     int * values = (int*)malloc(sizeof(int) * 0);
     return values;
}
