"""
# -*- coding: utf-8 -*-
Created on Thu Dec 11 23:24:32 2014

@author: Chad
"""

import re
import json
from itertools import takewhile, izip

SYMBS = {}
PRODS = {}
NTERMS = []
NTABLE = {}

class Symb:
    def __init__(self, T, N):
        self.T = T; #Terminal boolean
        self.N = N; #Name string
    def __repr__(self):
        return '\\%s%s' % (self.T,self.N,);
    def to_json(self):
        return self.__dict__        
    def key(self):
        return self.T+self.N;
def to_json(obj):
    if(isinstance(obj, (list, tuple))):
        return [to_json(o) for o in obj];
    elif(isinstance(obj, (dict))):
        D = {}
        for k in obj:
            D[k] = to_json(obj[k])
        return D
    else:
        return obj.to_json();
        
def get_sym(T,N):
    if not (T+N) in SYMBS:
        SYMBS[T+N] = Symb(T,N)
    return SYMBS[T+N]

def get_prod(N,P):
    i = 1;
    j = 1;
    k = 1;
    for N1 in NTERMS:
        if not N1 in NTABLE:
            continue
        if not N1 in N:
            i+=1
            continue
        for N2 in NTABLE[N1]:
            if not N2.key() == N:
                j += 1
                continue
            for P3 in PRODS[N2.key()]:
                if not P3 == P:
                    k += 1
                    continue
                else:
                    return (i,j,k)
    return (i,j,k)

EPSILON = get_sym('T','EP')
EOF = get_sym('T','EOF')

def print_grammar():
    for N in NTERMS:
        for P in PRODS[N]:
            print "\\%s -> %s" % (N, '~'.join([str(S) for S in P]))

def print_production(P):
    str = "(1)"
    i = 2;
    for S in P:
        str += "~%s~(%d)" % (S,i);
        i += 1;
    return str;

def write_massaged_grammar():
    code = ''
    code += '''\\begin{enumerate}[1.]'''
    for N in NTERMS:
        if not N in NTABLE:
            continue
        code += '''\item'''
        code += '''   \\begin{enumerate}[1.]'''
        for NT in NTABLE[N]:
            code += '''   \\item'''
            code += '''      \\begin{enumerate}[1.]'''
            for P in PRODS[NT.key()]:
                print '''       \\item \PROD{%s}{%s}''' % (NT, print_production(P))
            print '''       \\end{enumerate}'''
        print '''   \\end{enumerate}'''
    print '''\end{enumerate}'''

def add_prime(S):
    i = NTERMS.index(S.key());
    T = S.T;
    NP = S.N;
    while T+NP in SYMBS:
        NP += "'";
        i += 1;
    oNP = get_sym(S.T, NP);
    NTERMS.insert(i, oNP.key());
    N = S.N.replace("'",'');
    NTABLE[T+N].append(oNP)
    return oNP;

def read_file(file):
    with open(file, "r") as f:
        data = f.read();
        re_prod = r'\\PROD{\\N(\w+)}{([\w\\~\\\\\n]+)}';
        #print data
        dt = re.findall(re_prod, data);
        #print dt
        re_prod2 = r'\\(N|T)(\w+)';
        for d in dt:
            print d
            NT = 'N'+d[0]
            seqt = re.findall(re_prod2, d[1]);
            seq = []
            for s in seqt:
                seq.append(get_sym(s[0],s[1]));
            if not NT in PRODS:
                NS = get_sym('N',d[0])
                NTERMS.append(NT)
                NTABLE[NT]=[NS]
                PRODS[NT] = []
            PRODS[NT].append(seq)
    return

#NOTE: only call this after massaging
def nullable(N, level=0):
    for P in PRODS[N]:
        if EPSILON in P:
            return True
        #recursive definition
#        if SYMBS[N] in P:
#            continue
#        nv = True
#        for S in P:
#            if S.T == 'T' or not nullable(S.key(),level+1):
#                nv = False;
#        if nv:
#            return True
    return False

read_file("../data/grammar.tex")
print_grammar()

#remove epsilon productions

print
print "REMOVING EPSILON PRODUCTIONS"
def rem_epsilon_prods(N):
    print N
    for N2 in NTERMS:
        if N == N2:
            continue
        for P in PRODS[N2]:
            if SYMBS[N] in P:
                P2 = [S for S in P if not S == SYMBS[N]]
                PRODS[N2].append(P2)

for N in NTERMS:
    for P in PRODS[N]:
        if EPSILON in P:
            rem_epsilon_prods(N);
            PRODS[N].remove(P)
            break;
#print_grammar()
#print SYMBS
write_massaged_grammar();

#remove left recursion

print
print "REMOVING LEFT RECURSION"
def rem_deep_lrec(N,last):
    for P1 in PRODS[N]:
        if P1[0] in last:
            P2s = PRODS[P1[0].N]
            for P2 in P2s:
                PRODS[N].add(P2 + P1[1:])
            PRODS[N].remove(P1);

def rem_immd_lrec(N):
    ALPHA = []
    BETA = []
    for P in PRODS[N]:
        if P[0] == SYMBS[N]:
            ALPHA.append(P[1:])
        else:
            BETA.append(P)
    if not ALPHA:
        return;
    else:
        NP = add_prime(SYMBS[N]).key()
        if not BETA:
            BETA = [[]]
        PRODS[N] = []
        PRODS[NP] = []
        for B in BETA:
            PRODS[N].append(B + [SYMBS[NP]])
        for A in ALPHA:
            PRODS[NP].append(A + [SYMBS[NP]])
        PRODS[NP].append([EPSILON])
            

last = []
for N in NTERMS:
    rem_deep_lrec(N,last);
    rem_immd_lrec(N);
    last.append(N)
#print_grammar()
write_massaged_grammar();


#left factoring

print
print "LEFT FACTORING"
def has_prefix(P,PRE):
    pass

def allsame(x):
    return len(set(x)) == 1

def left_factor(N):
    #print N
    if len(PRODS[N])<=1:
        return;
    D = {}
    for P in PRODS[N]:
        if P[0] not in D:
            D[P[0]] = [P]
        else:
            D[P[0]].append(P)
    for PRE in D:
        #print D[PRE]
        if len(D[PRE])<=1:
            continue
        ALPHA = [i[0] for i in takewhile(allsame ,izip(*D[PRE]))]
        S = len(ALPHA)
        NP = add_prime(SYMBS[N]).key()
        BETA = [P[S:] for P in D[PRE]]
        PRODS[N] = [P for P in PRODS[N] if not P in D[PRE]]
        PRODS[N].append(ALPHA + [SYMBS[NP]])
        PRODS[NP] = []
        for P in BETA:
            if len(P)<=0:
                PRODS[NP].append([EPSILON])
            else:
                PRODS[NP].append(P)
for N in NTERMS:
    left_factor(N)
print_grammar()

#calculate first
print
print "CALCULATE FIRST"
print    
    
from compiler.ast import flatten
SFIRST = {}
def first(S):
    if not S in SFIRST:
        SFIRST[S] = CFirst(S)
    return SFIRST[S]    
class CFirst:
    def __init__(self, S):
        self.S = S;
        self.vals = None
        self.epsilon = []
    
    def evaluate(self, rem = []):
        if self.S.T=='T':
            return [self.S]
        else:
            return list(set(flatten([[S1 for S1 in F.evaluate(rem + [self]) if not S1 == EPSILON]\
                                        for F in self.vals if \
                                            (isinstance(F,CFirst))
                                            and not F in rem])))\
                        + [F for F in self.vals if isinstance(F,Symb)]\
                        + self.epsilon

    def __repr__(self):
        return "f(%s)" % (self.S.key())
    
PRODF = {}
def calc_first(N):
    f = []
    for P in PRODS[N]:
        f1 = []
        if EPSILON in P:
            f1.append(first(EPSILON))
        else:
            i = 0
            while True:
                if P[i].T == 'T':
                    f1.append(first(P[i]))
                    break;
                f1.append(first(P[i]))
                if not nullable(P[i].key()):
                    break
                i = i+1
                if i>=len(P):
                    f1.append(first(EPSILON))
                    break;
        PRODF[get_prod(N,P)] = f1
        f += f1
    return f
    
for N in NTERMS:
    f = calc_first(N)
    first(SYMBS[N]).vals = [i for i in f if not i == first(EPSILON)]
    print first(SYMBS[N]).vals
    if first(EPSILON) in f:
        first(SYMBS[N]).epsilon = [EPSILON]
for N in NTERMS:
    f = first(SYMBS[N])
    f.vals = f.evaluate()
    #print "%s: %s" % (N, f.vals,)
    
for T in PRODF:
    PRODF[T] = list(set(flatten([([X for X in F.vals if not X == EPSILON] if F.vals else F.S) for F in PRODF[T]])))
    
#calculate follow       
print
print "CALCULATING FOLLOWS"
print    

NFOLLOW = {}
def follow(N):
    if not N in NFOLLOW:
        NFOLLOW[N] = CFollow(N)
    return NFOLLOW[N]    
class CFollow:
    def __init__(self, S):
        self.S = S;
        self.vals = []
    
    def evaluate(self, rem = []):
        return list(set(flatten([F.evaluate(rem + [self])\
            for F in self.vals if \
                (isinstance(F,CFirst) or isinstance(F,CFollow))
                and not F in rem])))\
            + [F for F in self.vals if isinstance(F,Symb)]

    def __repr__(self):
        return "F(%s)" % (self.S.key())    
    
def calc_follow(N):
    F = []
    NS = SYMBS[N]
    for N2 in NTERMS:
        N2S = SYMBS[N2]
        for P in [P1 for P1 in PRODS[N2] if NS in P1]:
            i = P.index(NS)+1;
            if i>=len(P):
                F.append(follow(N2S))
            else:
                while True:
                    F.append(first(P[i]))
                    if not first(P[i]).vals or not EPSILON in first(P[i]).vals:
                        break;
                    i += 1
                    if i>=len(P):
                        F.append(follow(N2S))
                        break;
    return F

#print_grammar()

S0 = NTERMS[0]
follow(SYMBS[S0]).vals = [first(EOF)]
for N in NTERMS:
    F = calc_follow(N)
    follow(SYMBS[N]).vals += F

for N in NTERMS:
    F = follow(SYMBS[N])
    F.vals = F.evaluate()
    if EPSILON in F.vals: F.vals.remove(EPSILON)
   # print "%s: %s" % (N, F.vals,)



print "MASSAGED GRAMMAR:"
print
write_massaged_grammar();
print

def latex_first_follows():
    code = ''
    code += '''\\begin{tabular}{l|c|c}\n'''
    code += '''   \\textbf{Nonterminal} & \\textbf{first} & \\textbf{Follow}\\\\\\hline\n'''
    for N in NTERMS:
        if not N in NTABLE:
            continue
        for NT in NTABLE[N]:
            #fi = first(NT).vals
            ftxt = "$"
            for P in PRODS[NT.key()]:
                pid = get_prod(NT.key(),P)
                fi = PRODF[pid]
                ftxt += "\\underbrace{%s}_{%d.%d.%d}~" % ('~'.join([str(S) for S in fi]), pid[0],pid[1],pid[2])
            ftxt += "$"
            Fo = follow(NT).vals
            #print fi,Fo
            code += '''   %s & %s & %s\\\\\n''' % (NT, ftxt,
                                               '~'.join([str(S) for S in Fo]))
    code += '''\\end{tabular}\n'''
    return code;

def symbs_dict():
    return to_json(SYMBS)
    
def first_follows_dict():
    PROD_INFO = {}
    for N in NTERMS:
        if not N in NTABLE:
            continue;
        for NT in NTABLE[N]:
            PROD_INFO[NT.N] = [to_json(follow(NT).vals)]
            for P in PRODS[NT.key()]:
                pid = get_prod(NT.key(),P)
                if P[0] != EPSILON:
                    fi = PRODF[pid]
                else:
                    fi = follow(NT).vals
                #Fo = follow(NT).vals
                PROD_INFO[NT.N] += [{'P': to_json(P), 'pid': '%2d.%2d.%2d' % pid, 'f': to_json(fi)}]
    return PROD_INFO

print "FIRST FOLLOW TABLE"
print
print latex_first_follows()
print
    
with open('../data/ff.json', 'w') as f:
    f.write(json.dumps(first_follows_dict()))
with open('../data/symbs.json','w') as f:
    f.write(json.dumps(symbs_dict()))