import re
import json
from itertools import takewhile, izip, chain

SYMBS = []
PRODS = []

TAB='     ';

def debug(msg):
    print msg;
    pass

class Symb(object):
    def __init__(self, T, N):
        self.T = T; #Terminal boolean
        self.N = N; #Name string
        
    def __repr__(self):
        return '\\%s%s' % (self.T,self.N,);
        
    def from_json(self):
        return self.__dict__;
        
    def key(self):
        return self.T+self.N;

class Term(Symb):
    def __init__(self, T, N):
        super(Term, self).__init__(T,N.upper());

    def var_name(self):
        return 'T_%s' % (self.N);
        
    def store_var(self, n):
        return Var(self, n, 'entry');
        
    def compute(self, indent, n):
        if self == EPSILON:
            return indent*TAB+'//EPSILON\n';
        code = '';
        code += indent*TAB + '%s = match(%s);\n' % (self.store_var(n).name(), self.var_name());
        return code;

def format_nterm(txt):
    return txt.lower().replace('\'','p');

class NTerm(Symb):
    def __init__(self, T, N):
        super(NTerm, self).__init__(T,format_nterm(N));
        self.P = []
        self.args = []
        self.ret = []
        self.F = None
        self.vars = []
        self.svars = []
        self.tempc = 0;
        self.error = None;
    
    def calc_error(self):
        fs = chain(*[P.f for P in self.P]);
        self.error = CompilerError('SYNERR', 'Expected %s.' % (', '.join([f.var_name() for f in fs]),));
    
    def add_temp(self):
        t = self.tempc;
        self.tempc+=1;
        return t;
    
    def var_name(self):
        return 'n_%s' % (self.N);
    
    def store_var(self, n):
        return Var(self, n, 'data');
    
    def has_var(self, v):
        for v2 in self.vars:
            if v.S == v2.S and v.n == v2.n and v.a == v2.a:
                return True;
                
        return False;
        
    def add_var(self, v):
        if v.a == 'data':
            self.add_svar(v);
        elif not self.has_var(v):
            self.vars.append(v);   
   
    def add_svar(self, v):
        if not self.has_svar(v):
            self.svars.append(v);
            
    def has_svar(self, v):
        for v2 in self.svars:
            if v.S == v2.S and v.n == v2.n and v.a == v2.a:
                return True;
                
        return False;
   
    def get_arg(self, a):
        for v in self.args:
            if a == v:
                return v;
                
        return None
        
    def get_ret(self, a):
        for v in self.ret:
            if a == v:
                return v;
                
        return None
   
    def compute(self, indent, n):
        data_n = self.store_var(n).name();
        code = '';
        code += indent*TAB + data_n + ' = ' + self.var_name() + '(' + ','.join(a for a in self.get_args(n)) + ');\n';
        for i,v in enumerate(self.get_rets(n)):
            code += indent*TAB + '%s = %s[%d];\n' % (v,data_n,i,);
        code += indent*TAB + 'free(%s);\n' % (data_n,);
        return code;
    
    def method_declare(self, indent):
        code = '';
        code += indent*TAB + 'int* %s(%s);\n' % (self.var_name(), ','.join(['int %s' % (a,) for a in self.get_args(1)]));
        return code;
    
    def get_args(self, n):
        return ["%s_%d_%s" % (self.var_name(), n, a) for a in self.args];
        
    def get_rets(self, n):
        return ["%s_%d_%s" % (self.var_name(), n, a) for a in self.ret];
    
    def code(self, indent=1):
        SYNC = ', '.join([V.var_name().upper() for V in self.F]);
        NAME = self.var_name(); 
        SYNC_S = len(self.F);
        code = '';
        code += TAB*(indent) + 'int SYNC_%s[%d] = {%s};\n' % (NAME, SYNC_S, SYNC);
        code += TAB*(indent) + 'int* %s(%s) {\n' % (NAME,', '.join(['int %s' % (q,) for q in self.get_args(1)]));
        for v in self.get_rets(1):
            code += TAB*(indent+1) + 'int %s;\n' % (v);
        if self.vars:
            code += TAB*(indent+1) + 'int %s;\n' % (', '.join([v.name() for v in self.vars]),);
        if self.svars:
            code += TAB*(indent+1) + 'int %s;\n' % (', '.join(['*%s' % (v.name(),) for v in self.svars]),);
        if self.tempc>0:
            code += TAB*(indent+1) + 'int %s;\n' % (', '.join(['t%d' % (tid,) for tid in range(self.tempc)[1:]]),);
        code += TAB*(indent+1) + 'switch(tok->tag) {\n' ;
        code += '\n'.join([Pr.code(indent+1) for Pr in self.P]) + '\n' ;
        code += TAB*(indent+1) + 'default:\n';
        code += TAB*(indent+2) + 'report_error(tok->line, %s);\n' % (self.error.name(),) ;
        code += TAB*(indent+2) + 'while(!synch(SYNC_%s, %d, tok->tag))\n' % (NAME,SYNC_S) ;
        code += TAB*(indent+3) + 'tok = get_token();\n';
        code += TAB*(indent+1) + '}\n';
        code += TAB*(indent+1) + 'int *values = (int*)malloc(sizeof(int) * %d);\n' % (len(self.ret),);
        for i,v in enumerate(self.get_rets(1)):
            code += TAB*(indent+1) + 'values[%d] = %s;\n' % (i,v);
        code += TAB*(indent+1) + 'return values;\n';
        code += TAB*(indent) + '}\n';
        return code;

class Prod:
    def __init__(self, N, P, pid, f):
        self.N = Var(N,1,None);
        self.P = [];
        for i,V in enumerate(P):
            n = len([x for x in [N]+P[:i] if x == V])+1;
            self.P.append(Var(V,n,None));
            self.N.S.add_var(V.store_var(n));
        self.pid = pid;
        self.f = f;
        self.decs = [];
    
    def __repr__(self):
        p_str = [pr.var_name() for pr in self.P];
        f_str = [v.var_name() for v in self.f];
        return '%6s %s -> %s, f={%s}' % (self.pid, self.N.var_name(), ' '.join(p_str), ', '.join(f_str));
    
    def code(self, indent=1):
        code = '';
        #code += ''.join((indent*TAB)+'case %s: ;\n' % (V.var_name(),) for V in self.f);
        for V in self.f:
            if V == EPSILON:
                F = self.N.S.F;
                for V1 in F:
                    code += indent*TAB + 'case %s: ;\n' % (V1.var_name(),);
            else:
                code += indent*TAB + 'case %s: ;\n' % (V.var_name(),);
        for i,V in enumerate(self.P):
            ds = [d for d in self.decs if d.l == i+1];
            code += '\n'.join([d.compute(indent+1) for d in ds]) + '\n';
            code += V.S.compute(indent+1, V.n);
        i = len(self.P);
        ds = [d for d in self.decs if d.l == i+1];
        code += '\n'.join([d.compute(indent+1) for d in ds]) + '\n';
        code += ((indent+1)*TAB)+'break;';
        return code;

class Var:
    def __init__(self, S, n, a):
        self.S = S; #the terminal or nonterminal
        self.n = n; #the unique number identifier (in case of duplicate symbols)
        self.a = a; #sub-variable name
        
    def name(self):
        return '%s_%d_%s' % (self.S.var_name(),self.n,self.a)
        
    def code(self, indent=1):
        if self.S == EPSILON:
            return indent*TAB + '//EPSILON';
        elif self.S.T == 'T':
            return indent*TAB + 'match(%s);' % (self.S.var_name());
        else:
            return indent*TAB + '%s(%s);' % (self.S.var_name(),', '.join([]));

e_count = {'SYNERR':5, 'SEMERR':1}
e_offset = {'SYNERR':2000, 'SEMERR':3000}
errors = {'SYNERR': [], 'SEMERR': []}
class CompilerError:
    def __init__(self, e_type, e_msg):
        global e_count,errors
        self.e_type = e_type;
        self.e_msg = e_msg;
        self.e_id = e_count[e_type];
        e_count[e_type] += 1;
        errors[e_type].append(self);
    
    def name(self):
        return '%s_%d' % (self.e_type, self.e_id);
    
    def define(self, indent):
        return indent*TAB + '#define %s %d\n' % (self.name(), e_offset[self.e_type]+self.e_id);

    def error_string(self):
        return '"%s: %s"' % (self.e_type, self.e_msg,);

class Type:
    def __init__(self, name, tid):
        self.n = name;
        self.id = tid;
    def name(self):
        return 'TYPE_'+self.n;

class CInt:
    def __init__(self, i):
        self.i = i;
        
    def name(self):
        return self.i;

class Attr:
    def __init__(self, a):
        self.a = a;
    
    def name(self):
        return self.a;

TYPES = []
def add_type(name, tid):
    global TYPES;
    T = Type(name, tid);
    TYPES.append(T);
    return T
def get_type(name):
    global TYPES;
    for T in TYPES:
        if re.match('\s*'+T.n+'\s*',name):
            return T;
    return None
    
ERR = add_type('ERR',0);
VOID = add_type('VOID',1);
INT = add_type('INT',2);
REAL = add_type('REAL',3);
AINT = add_type('AINT',4);
AREAL = add_type('AREAL',5);
BOOLEAN = add_type('BOOLEAN',6);

print [t.name() for t in TYPES]

class Decoration(object):
    def __init__(self, P, l):
        global t_counter;
        self.P = P;
        self.l = l;
        self.d_id = t_counter;
        t_counter += 1;
        self.temp = None;
    
    def result(self):
        if not self.temp:
            self.temp = self.P.N.S.add_temp();
        return 't' + str(self.temp);

t_counter  = 0;
class D_ASSIGN(Decoration):
    def __init__(self, *data):
        super(D_ASSIGN, self).__init__(*data);
        self.lvar = None;
        self.rdec = None;
        
    def compute(self, indent):
        code = '';
        code += self.rdec.compute(indent);
        code += indent*TAB + self.lvar.result() + ' = ' + self.rdec.result() + ';';
        return code;
        
    def result(self):
        return self.lvar.result();
    
    @staticmethod
    def parse(P, l, txt):
        #print txt;
        R1 = re.match(r'([\\A-Za-z0-9\.\'\_]+)\s*=\s*(.*)\s*', txt);
        if not R1:
            return None
        else:
            print "dassign `%s` ~ `%s`" % (R1.group(1),R1.group(2));
            print R1.group(1),'***',R1.group(2);
            d = D_ASSIGN(P, l);
            print 'dassign right'
            d.lvar = D_VALUE.parse(P, l, R1.group(1));
            print 'dassign left'
            d.rdec = parse_decoration(P, l, R1.group(2));
            #print d.lvar.result();
            #print d.rdec.compute(0);
            return d;
            
class D_FCALL(Decoration):
    def __init__(self, *data):
        super(D_FCALL, self).__init__(*data);
        self.f = None 
        self.a = [] #arguments as variables
        
    def compute(self, indent):
        return indent*TAB + self.result() + ' = %s(%s);\n' % (self.f, ', '.join([v.result() for v in self.a]));
        
    @staticmethod
    def parse(P, l, txt):
        R1 = re.match('([A-Za-z0-9\\\_]+)\((.*)\)', txt);
        if not R1:
            return None;
        else:
            print "dfcall %s" % (R1.group(1),);
            d = D_FCALL(P, l);
            d.f = R1.group(1).replace('\_','_');
            if R1.group(2):
                for s in R1.group(2).split(','):
                    print "dfcall arg %s" % (s,);
                    d.a.append(D_VALUE.parse(P,l,s));
            d.result();
            return d;
            
class D_CASE(Decoration):
    def __init__(self, *data):
        super(D_CASE, self).__init__(*data);
        self.cases = []
    
    def compute(self, indent):
        code = '';
        #code += indent*TAB + 'int ' + self.result() + ';\n'
        for (v,c) in self.cases:
            code += c.compute(indent);
        code += indent*TAB + 'if (%s) {\n' % (self.cases[0][1].result(),);
        code += (indent+1)*TAB + '%s = %s;\n' % (self.result(), self.cases[0][0].result(),);
        code += indent*TAB + '}\n';
        for c in self.cases[1:]:
            if c[1].conds or c[1].nconds:
                code += indent*TAB + 'else if (%s) {\n' % (c[1].result(),);
            else:
                code += indent*TAB + 'else {\n';
            code += c[0].compute(indent+1);
            code += (indent+1)*TAB + '%s = %s;\n' % (self.result(), c[0].result(),);
            code += indent*TAB + '}\n';
        return code;
        
    @staticmethod
    def parse(P, l, txt):
        R1 = re.match(r'\\begin\{cases\}(.*)\\end\{cases\}', txt);
        if not R1:
            return None;
            
        d = D_CASE(P, l);
        cases = R1.group(1).split('\\\\');
        #print cases
        R2 = [re.match(r'\s*(.*?)\s*&\s*(.*?)\s*\;', c+';') for c in cases]
        #R2 = re.finditer(r'\s*(.*?)\s*&\s*(.*?)\s*\\\\',R1.group(1));
        for R in R2:
            print "dcase",R.group(1),"***",'`%s`'%(R.group(2),);
            v = D_VALUE.parse(P, l, R.group(1));
            c = D_COND.parse(P, l, R.group(2));
            d.cases.append((v,c));
            
        return d;
            
class D_COND(Decoration):
    def __init__(self, *data):
        super(D_COND, self).__init__(*data);
        self.conds = []
        self.nconds = []

    def compute(self, indent):
        code = '';
        for (v,r) in self.conds:
            code += v.compute(indent);
            code += r.compute(indent);
        for (v,r) in self.nconds:
            code += v.compute(indent);
            code += r.compute(indent);
        return code;
        
    def result(self):
        code = '';
        #print self.nconds;
        code += '&& '.join(['%s == %s ' % (s1.result(),s2.result()) for s1,s2 in self.conds]
            + ['%s != %s ' % (s1.result(),s2.result()) for s1,s2 in self.nconds]
            );
        return code;
        
    @staticmethod
    def parse(P, l, txt):
        conds = txt.split('\\land');
        d = D_COND(P, l);
        if re.match('\s*otherwise\s*',txt):
            print "dcond else"
            return d;
        for c in conds:
            R1 = re.match(r'\s*([\\A-Za-z0-9\'\.\(\)\_]+)\s*=\s*(.*)\s*', c);
            L = d.conds;
            if not R1:
                R1 = re.match(r'\s*([\\A-Za-z0-9\'\.\(\)\_]+)\s*\\neq\s*(.*)\s*', c);
                L = d.nconds;
            print "dcond `%s` `%s`" % (R1.group(1),R1.group(2));
            v = parse_decoration(P, l, R1.group(1));
            r = parse_decoration(P, l, R1.group(2));
            L.append((v,r));
            
        return d;

attrs = ['ROP_GT','ROP_GE','ROP_EQ','ROW_NEQ','ROP_LT','ROP_LE','AOP_PLUS','AOP_MINUS','AOP_OR','MOP_TIMES','MOP_DIV','MOP_IDIV','MOP_MOD','MOP_AND',]
class D_VALUE(Decoration):
    def __init__(self, *data):
        super(D_VALUE, self).__init__(*data);
        self.value = None;
        self.error = None;
        
    def compute(self, indent):
        if(self.error):
            return indent*TAB + 'report_error(tok->line, %s);\n' % (self.error.name());
        return '';
        #return indent*TAB + 'int ' + self.value.name() + ';\n';
        
    def result(self):
        return self.value.name();
        
    @staticmethod
    def parse(P, l, txt):
        d = D_VALUE(P, l);
        v = parse_var(txt);
        t = get_type(txt);
        R4 = re.match('\s*ERR\^\*\\\\msg\{(.*)\}\s*',txt)
        R3 = re.match('\s*([0-9]+)\s*', txt);
        R6 = re.match('\s*(temp[0-9]+)',txt);
        if R4:
            print "dvalue",'ERR*'
            d.value = ERR;
            d.error = CompilerError('SEMERR',R4.group(1));
        elif v:
            print "dvalue",v.name();
            if not v.S == P.N.S or v.n>1:
                P.N.S.add_var(v);
            d.value = v;
        elif t:
            print "dvalue",t.name();
            d.value = t;
        elif R3:
            d.value = CInt(R3.group(1));
        elif R6:
            d.value = Var(P.N.S, 1, R6.group(1));
            P.N.S.add_var(d.value);
        else:
            R5 = re.match('\s*(.*)\s*',txt);
            nv = R5.group(1).replace('\_','_');
            if nv in attrs:
                print 'dvalue',nv;
                d.value = Attr(nv);
            else:
                return None
        return d;

class D_EXPRESSION(Decoration):
    def __init__(self, *data):
        super(D_EXPRESSION, self).__init__(*data);
        self.l = None;
        self.r = None;
        self.op = None;
        
    def compute(self, indent):
        code = '';
        code += self.l.compute(indent);
        code += self.r.compute(indent);
        code += indent*TAB + '%s = %s %s %s;\n' % (self.result(), self.l.result(), self.op, self.r.result());
        return code;
    
    @staticmethod
    def parse(P, l, txt):
        d = D_EXPRESSION(P, l);
        R1 = re.match('(\*|\+|\-|\/)\s*\((.+?)\)\s*\((.+)\)',txt);
        if not R1:
            return None;
        print 'd_expression',R1.group(1),'***',R1.group(2),'***',R1.group(3);
        d.op = R1.group(1);
        d.l = parse_decoration(P, l, R1.group(2));
        d.r = parse_decoration(P, l, R1.group(3));
        return d;

def parse_var(vname):
    #print "parse_var", vname
    R1 = re.match(r'\s*(?:\\)?([A-Z\']+)(?:\_([0-9]+))?(?:\$\_([0-9]+)\$)?(?:\.([a-z]+))\s*',vname);
    if not R1:
        #print "parse_var", 'err'
        return None;
    else:
        S = format_nterm(R1.group(1));
        S = find_symb(S[0].upper(), S[1:]);
        if R1.group(2):
            n = int(R1.group(2));
        elif R1.group(3):
            n = int(R1.group(2));
        else:
            n = 1;
            
        a = R1.group(4);
        if (a == 'i' or a == 'offset' or a == 'parent') and isinstance(S, NTerm):
            if not S.get_arg(a):
                S.args.append(a);
                
        if (a == 't' or a == 'totalsize' or a == 'width' or a == 'entry') and isinstance(S, NTerm):
            if not S.get_ret(a):
                S.ret.append(a);

        return Var(S, n, a);
        
def parse_decoration(P, l, txt):
    ds = [D_CASE,D_ASSIGN,D_FCALL,D_EXPRESSION,D_VALUE];
    for d in ds:
        r = d.parse(P, l, txt);
        if r:
            return r;

def find_symb(T, N):
    if T == 'T':
        N1 = N.upper();
    else:
        N1 = format_nterm(N);
    #print T,N1
        
    for S in SYMBS:
        #print S
        if(T == S.T and N1 == S.N):
            return S;
            
    return None

NTERMS = []

with open('../data/symbs.json', 'r') as f:
    data = json.load(f);
    for k in data:
        if(data[k]['T'] == 'N'):
            SYMBS.append(NTerm(data[k]['T'],data[k]['N']));
        else:
            SYMBS.append(Term(data[k]['T'],data[k]['N']));

with open('../data/ff.json', 'r') as f:
    data = json.load(f);
    for N in data:
        V = find_symb('N', N);
        V.F = [find_symb(Q['T'],Q['N']) for Q in data[N][0]];
        for Pr in data[N][1:]:
            P = [find_symb(Q['T'],Q['N']) for Q in Pr['P']];
            f = [find_symb(Q['T'],Q['N']) for Q in Pr['f']];
            P1 = Prod(V, P, Pr['pid'], f);
            PRODS.append(P1);
            V.P.append(P1);
    PRODS.sort(key=lambda x: x.pid);
    for P in PRODS:
        if P.N.S not in NTERMS:
            NTERMS.append(P.N.S);
            P.N.S.calc_error();
    

def split_decorations(decs):
    Rs = re.finditer('\(([0-9]+)\)\s*\&\s*',decs);
    try:
        R = Rs.next();
    except StopIteration:
        return [];
    D = []
    try:
        while True:
            l = R.group(1);
            N = Rs.next();
            d1 = decs[R.end():N.start()]
            D.append([int(l),d1]);
            R = N;
            
    except StopIteration:
        d1 = decs[R.end():];
        D.append([int(l),d1]);
    return D;
        

with open('../data/decorations.tex', 'r') as f:
    f_str = f.read().replace('\n','');
    #print f_str;
    #R1 = re.search(r'\\begin\{enumerate\}\[1.\](.*)\\end\{enumerate\}', f_str).group(1);
    #print R1;
    R1 = re.findall(r'\\item \\PROD\{\\N([\w\']+)\}\{\(1\)((?:\~[\\A-Z\'\$\_0-9]+\~\([0-9]+\))*)\}\s*\\\[\s*\\left\\\{\s*\\begin\{array\}\{ll\}(.*?)\\end\{array\}\s*\\right\\\}\s*\\\]', f_str);
    #print len(R1)    
    #for R in R1:
    #    print R[0]
    Decs = [];
    i = 0;
    for R in R1:
        print R[0];
        #print R[0];
        P = PRODS[i];
        #print R[2];
        V = find_symb('N', R[0]);
        Ds = split_decorations(R[2]);
        for D in Ds:
            print D[1];
            print '***************'
            dec = parse_decoration(P, int(D[0]), D[1]);
            P.decs.append(dec);
            print '*********'
            print 'CODE'
            print '*********'
            print dec.compute(0);
            print '***************'
            #print
        i += 1;


EPSILON = find_symb('T','EP')
EOF = find_symb('T','EOF')

with open('../../src/nonterms.h','w') as f:
    f.write('#include "parse.h"\n#include "tokens.h"\n#include "error.h"\n\n');
    for N in NTERMS:
        f.write(N.method_declare(0));
        f.write('\n');
        
with open('../../src/nonterms.c','w') as f:
    f.write('#include "nonterms.h"\n\n');
    for N in NTERMS:
        f.write(N.code(0));

with open('../../src/generated_errors.h','w') as f:
    f.write('#ifndef GENERATED_ERRORS_H\n');
    f.write('#define GENERATED_ERRORS_H\n');
    for E in errors['SYNERR']:
        f.write(E.define(0));
    for E in errors['SEMERR']:
        f.write(E.define(0));
    f.write('\n\n');
    f.write('const char* gen_error_msg(int ERR_ID);\n');
    f.write('#endif // GENERATED_ERRORS_H')

with open('../../src/generated_errors.c','w') as f:
    f.write('#include "generated_errors.h"\n\n');
    f.write('const char* gen_error_msg(int ERR_ID) {\n');
    f.write(TAB + 'switch(ERR_ID) {\n');
    for E in errors['SYNERR']:
        code = '';
        code += TAB*2 + 'case %s:\n' % (E.name(),);
        code += TAB*3 + 'return %s;\n' % (E.error_string(),);
        f.write(code);
    for E in errors['SEMERR']:
        code = '';
        code += TAB*2 + 'case %s:\n' % (E.name(),);
        code += TAB*3 + 'return %s;\n' % (E.error_string(),);
        f.write(code);
    f.write(TAB*2 + 'default:\n');
    f.write(TAB*3 + 'return "Unrecognized error.";\n');
    f.write(TAB + '}\n')
    f.write('}\n');
for N in NTERMS:
    #print N.code(0);
    #print ;
    pass