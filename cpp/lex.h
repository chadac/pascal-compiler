#ifndef LEX_H
#define LEX_H

#include "cllist.h"

struct CLList *tokens;

struct CLList* lex();

int read_token(struct CLList **tree, int line, char **f, char **l);

int write_token_table(struct CLList **tree);
int write_symbl_table(struct CLList **tree);


#endif // LEX_H
