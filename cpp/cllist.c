#include "cllist.h"
#include "global.h"

struct CLList *cllist_f(int item) {
    struct CLList *root = malloc(sizeof(struct CLList));
    root->p = item;
    root->next = 0;
    root->prev = 0;
    return root;
}

/*int llist_add(CLList **root, int p, int at) {
CLList *item = malloc(sizeof(CLList));
item->p = p;
if (at == 0) {
item->next = *root;
*root = item;
return 0;
}
CLList *c = *root;
int ci = 0;
while (ci < at) {
c = c->next;
ci++;
}

}*/

int cllist_append(struct CLList **root, int p) {
    if (!*root) {
        *root = cllist_f(p);
        return 0;
    }
    struct CLList *item = cllist_f(p);
    struct CLList *c = *root;
    while (c->next) {
        c = c->next;
    }
    c->next = item;
    item->prev = c;
    return 1;
}

int cllist_pop(struct CLList **root) {
    struct CLList *c = *root;
    struct CLList *n = c->next;
    *root = n;
    int p = c->p;
    free(c);
    //while (c->next && c->next->next) {
    //    c = c->next;
    //}
    //int p = c->next->p;
    //free(c->next);
    //c->next = 0;
    return p;
}

int cllist_push(struct CLList **root, int p) {
    struct CLList *item = cllist_f(p);
    if(*root) {
        item->next = *root;
        (*root)->prev = item;
    }
    *root = item;
    return 1;
}

int cllist_at(struct CLList **root, int at) {
    struct CLList *lst = *root;
    for(int i = 0; i < at; i++)
        lst = lst->next;
    return lst->p;
}

int cllist_last(struct CLList **root) {
    struct CLList *last = *root;
    while(last->next)
        last = last->next;
    return last->p;
}
