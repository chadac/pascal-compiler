#include "tokens.h"
#include "global.h"


struct Token *token_f(int line, char* lexeme, int tag, int attr) {
    struct Token *t = (struct Token*) malloc(sizeof(struct Token));
    t->line = line;
    char* str = (char*)malloc((strlen(lexeme)+1)*sizeof(char));
    strcpy(str, lexeme);

    t->lexeme = str;
    t->tag = tag;
    t->attr = attr;
    return t;
}

struct Number *number_f(int type, int a, int b, int c) {
    struct Number *n = malloc(sizeof(struct Number));
    n->type = type;
    n->a = a;
    n->b = b;
    n->c = c;
    return n;
}
