#include "lex.h"
#include "tokens.h"
#include "error.h"
#include "global.h"
#include "rsword.h"
#include "machines.h"
#include "listing.h"
#include "cstring.h"

struct CLList* lex() {
    rstable_i();
    struct CLList *cline = read_source_file();
    char *f, *l;
    int lnum = 1;
    while(cline) {
        char* line = (char*)cline->p;
        printf("%4d %s", lnum, line);
        f = line;
        l = line;
        while (read_token(&tokens, lnum, &f, &l));
        lnum++;
        cline = cline->next;
    }
    cllist_append(&tokens, (int)token_f(lnum, "EOF", T_EOF, 0));
    write_token_table(&tokens);
    return tokens;
}

int read_token(struct CLList **tree, int line, char **f, char **l) {
    struct Token *t = 0;
    if (**l == '\n' || **l == '\r' || !**l) return 0;
    t = (struct Token*)machine_whitespace(line, f, l);
    if(!t)
        t = (struct Token*)machine_idres(line, f, l);
    if (!t)
        t = (struct Token*)machine_nlreal(line, f, l);
    if (!t)
        t = (struct Token*)machine_nreal(line, f, l);
    if (!t)
        t = (struct Token*)machine_nint(line, f, l);
    if (!t)
        t = (struct Token*)machine_mulop(line, f, l);
    if (!t)
        t = (struct Token*)machine_addop(line, f, l);
    if (!t)
        t = (struct Token*)machine_relop(line, f, l);
    if (!t)
        t = (struct Token*)machine_catchall(line, f, l);
    if (!t)
        return 0;
    *f = *l;
    if (t->tag == T_EMPTY) return 1;
    cllist_append(tree, (int)t);
    if (t->tag == T_LEXERR)
        report_error(line, t->attr);
    printf("%4d %12s %3d %2d\n", t->line, t->lexeme, t->tag, t->attr);
    return 1;
}

int write_token_table(struct CLList **tree) {
    const char* fname = cstrcat(PROG_NAME, "_tokens.txt");
    struct CLList *c = *tree;
    FILE *ft = fopen(fname,"w");
    while (c) {
        struct Token *t = (struct Token*) c->p;
        fprintf(ft,"%4d %12s %3d %2d\n",t->line, t->lexeme, t->tag, t->attr);
        c = c->next;
    }
    fclose(ft);
    return 0;
}

int write_symbl_table(struct CLList **tree) {
    return 0;
}
