#ifndef ERROR_H
#define ERROR_H

#include "cllist.h"
#include "global.h"
#include "generated_errors.h"

/* Lexerrs */
#define LEXERR_UNREC 1001
#define LEXERR_LONGINT 1010
#define LEXERR_LONGREAL 1011
#define LEXERR_LONGE 1012
#define LEXERR_INVALIDINT 1013
#define LEXERR_INVALIDREAL 1014
#define LEXERR_INVALIDE 1015
#define LEXERR_LEADZERO 1018
#define LEXERR_TAILZERO 1019
#define LEXERR_LONGRSID 1005

/* Synerrs */
#define SYNERR_UNREC 5001
#define SEMERR_NOTINSCOPE 5002
#define SEMERR_PROCINSCOPE 5003
#define SEMERR_VARINSCOPE 5004
#define SEMERR_TOOMANYPARAMS 5005
#define SEMERR_NOTENOUGHPARAMS 5006
#define SEMERR_PROCNOTINSCOPE 5007

//#define SYNERR_PROG 2002
//#define SYNERR_PROG1 2003
//#define SYNERR_PROG2 2004
//#define SYNERR_INDENTLIST 2005
//#define SYNERR_INDENTLIST1 2006
//#define SYNERR_DECLS 2007
//#define SYNERR_DECLS1 2008
//#define SYNERR_DECLS2 2009
//#define SYNERR_TYPE 2010
//#define SYNERR_STDTYPE 2011
//#define SYNERR_SUBPDECLS 2012
//#define SYNERR_SUBPDECLS1 2013
//#define SYNERR_SUBPDECL 2014
//#define SYNERR_SUBPDECL1 2015
//#define SYNERR_SUBPDECL2 2016
//#define SYNERR_SUBPHEAD 2017
//#define SYNERR_SUBPHEAD1 2018
//#define SYNERR_ARGS 2019
//#define SYNERR_PARAMLIST 2020
//#define SYNERR_PARAMLIST1 2021
//#define SYNERR_CMPDSTMT 2022
//#define SYNERR_CMPDSTMT1 2023
//#define SYNERR_OSTMTS 2024
//#define SYNERR_STMTLST 2025
//#define SYNERR_STMTLST1 2026
//#define SYNERR_STMT 2027
//#define SYNERR_STMT1 2028
//#define SYNERR_VAR 2029
//#define SYNERR_VAR1 2030
//#define SYNERR_PROCSTMT 2031
//#define SYNERR_PROCSTMT1 2032
//#define SYNERR_EXPRLIST 2033
//#define SYNERR_EXPRLIST1 2034
//#define SYNERR_SEXPR 2035
//#define SYNERR_SEXPR1 2036
//#define SYNERR_TERM 2037
//#define SYNERR_TERM1 2038
//#define SYNERR_FACTOR 2039
//#define SYNERR_FACTOR1 2040
//#define SYNERR_SIGN 2041
//#define SYNERR_EXPR 2042
//#define SYNERR_EXPR1 2043

bool init_error_reporting();
bool report_error(int line, int ERR_ID);
struct CLList* get_errors(int line);
const char* get_error_msg(int ERR_ID);

struct Error {
    int eid;
    int line;
    char* msg;
};

struct Error *error_f(int line, int eid, char* msg);

#endif // ERROR_H
