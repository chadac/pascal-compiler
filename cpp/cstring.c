#include "cstring.h"

char *strcpyn(char* Source, int Length) {
    char* dest = (char*)malloc((Length + 1)*sizeof(char));
    for (int i = 0; i < Length; i++)
        dest[i] = Source[i];
    dest[Length] = 0;
    return dest;
}

//for small strings
char *cstrcpy(const char* source) {
    char* c = source;
    while(c[0])
        c++;
    char* n = (char*)malloc(c-source+1);
    for(int i = 0; i < c-source; i++) {
        n[i] = source[i];
    }
    n[c-source]=0;
    return n;
}

//for two small strings
char *cstrcat(char* s1, char* s2) {
    int c1 = 0;
    while(s1[c1]) c1++;
    int c2 = 0;
    while(s2[c2]) c2++;
    char* n = (char*)malloc(c1+c2);
    n[0] = 0;
    strcat(n,s1);
    strcat(n,s2);
    return n;
}
