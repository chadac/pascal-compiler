#include "symbol.h"

struct Symbol* sym_f(char *name) {
    struct Symbol *s = malloc(sizeof(struct Symbol));
    s->name = cstrcpy(name);
    return s;
}

struct Symbol* get_symbol(const char *name) {
    struct CLList* c = sym_table;
    while(c) {
        struct Symbol* s = (struct Symbol*)c->p;
        if(strlen(name) == strlen(s->name) && strcmp(name, s->name) == 0)
            return s;
        c = c->next;
    }
    return 0;
}

struct Symbol* add_symbol(const char *name) {
    struct Symbol* s = get_symbol(name);
    if(!s) {
        s = sym_f(name);
        cllist_append(&sym_table, s);
    }
    return s;
}
