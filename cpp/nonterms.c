#include "nonterms.h"

int SYNC_n_prog[1] = {T_EOF};
int* n_prog(int n_prog_1_offset) {
     int n_prog_1_t;
     int n_prog_1_totalsize;
     int T_PROG_1_entry, T_ID_1_entry, T_LPAR_1_entry, T_RPAR_1_entry, T_SC_1_entry, n_prog_1_temp1, n_progp_1_parent, n_progp_1_offset, n_idtl_1_t, n_progp_1_t, n_progp_1_totalsize;
     int *n_idtl_1_data, *n_progp_1_data;
     int t1, t2, t3, t4;
     switch(tok->tag) {
     case T_PROG: ;
          n_prog_1_offset = 0;
          T_PROG_1_entry = match(T_PROG);

          T_ID_1_entry = match(T_ID);
          t1 = check_add_blue_node(T_ID_1_entry);
          n_prog_1_temp1 = t1;
          T_LPAR_1_entry = match(T_LPAR);

          n_idtl_1_data = n_idtl();
          n_idtl_1_t = n_idtl_1_data[0];
          free(n_idtl_1_data);

          T_RPAR_1_entry = match(T_RPAR);

          T_SC_1_entry = match(T_SC);
          if (n_prog_1_temp1 != TYPE_ERR ) {
               t2 = T_ID_1_entry;
          }
          else {
               t2 = 0;
          }
          n_progp_1_parent = t2;
          n_progp_1_offset = n_prog_1_offset;
          n_progp_1_data = n_progp(n_progp_1_parent,n_progp_1_offset);
          n_progp_1_t = n_progp_1_data[0];
          n_progp_1_totalsize = n_progp_1_data[1];
          free(n_progp_1_data);
          if (n_idtl_1_t == TYPE_ERR ) {
               t3 = TYPE_ERR;
          }
          else if (n_progp_1_t == TYPE_ERR ) {
               t3 = TYPE_ERR;
          }
          else {
               t3 = TYPE_VOID;
          }
          n_prog_1_t = t3;
          n_prog_1_totalsize = n_progp_1_totalsize;
          t4 = pop_blue_node();

          break;
     default:
          report_error(tok->line, SYNERR_5);
          while(!synch(SYNC_n_prog, 1, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 2);
     values[0] = n_prog_1_t;
     values[1] = n_prog_1_totalsize;
     return values;
}
int SYNC_n_progp[1] = {T_EOF};
int* n_progp(int n_progp_1_parent, int n_progp_1_offset) {
     int n_progp_1_t;
     int n_progp_1_totalsize;
     int T_DOT_1_entry, n_spds_1_parent, n_spds_1_offset, n_spds_1_totalsize, n_spds_1_t, n_cmpd_1_t, n_decl_1_offset, n_progpp_1_offset, n_decl_1_totalsize, n_progpp_1_parent, n_progpp_1_totalsize, n_decl_1_t, n_progpp_1_t;
     int *n_spds_1_data, *n_cmpd_1_data, *n_decl_1_data, *n_progpp_1_data;
     int t1, t2;
     switch(tok->tag) {
     case T_PROC: ;
          n_spds_1_parent = n_progp_1_parent;
          n_spds_1_offset = n_progp_1_offset;
          n_spds_1_data = n_spds(n_spds_1_parent,n_spds_1_offset);
          n_spds_1_totalsize = n_spds_1_data[0];
          n_spds_1_t = n_spds_1_data[1];
          free(n_spds_1_data);
          n_progp_1_totalsize = n_spds_1_totalsize;
          n_cmpd_1_data = n_cmpd();
          n_cmpd_1_t = n_cmpd_1_data[0];
          free(n_cmpd_1_data);

          T_DOT_1_entry = match(T_DOT);
          if (n_spds_1_t == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else if (n_cmpd_1_t == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else {
               t1 = TYPE_VOID;
          }
          n_progp_1_t = t1;
          break;
     case T_BEG: ;

          n_cmpd_1_data = n_cmpd();
          n_cmpd_1_t = n_cmpd_1_data[0];
          free(n_cmpd_1_data);

          T_DOT_1_entry = match(T_DOT);
          n_progp_1_t = n_cmpd_1_t;
          n_progp_1_totalsize = n_progp_1_offset;
          break;
     case T_VAR: ;
          n_decl_1_offset = n_progp_1_offset;
          n_decl_1_data = n_decl(n_decl_1_offset);
          n_decl_1_totalsize = n_decl_1_data[0];
          n_decl_1_t = n_decl_1_data[1];
          free(n_decl_1_data);
          n_progpp_1_offset = n_decl_1_totalsize;
          n_progpp_1_parent = n_progp_1_parent;
          n_progpp_1_data = n_progpp(n_progpp_1_offset,n_progpp_1_parent);
          n_progpp_1_totalsize = n_progpp_1_data[0];
          n_progpp_1_t = n_progpp_1_data[1];
          free(n_progpp_1_data);
          n_progp_1_totalsize = n_progpp_1_totalsize;
          if (n_decl_1_t == TYPE_ERR ) {
               t2 = TYPE_ERR;
          }
          else if (n_progpp_1_t == TYPE_ERR ) {
               t2 = TYPE_ERR;
          }
          else {
               t2 = TYPE_VOID;
          }
          n_progp_1_t = t2;
          break;
     default:
          report_error(tok->line, SYNERR_6);
          while(!synch(SYNC_n_progp, 1, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 2);
     values[0] = n_progp_1_t;
     values[1] = n_progp_1_totalsize;
     return values;
}
int SYNC_n_progpp[1] = {T_EOF};
int* n_progpp(int n_progpp_1_offset, int n_progpp_1_parent) {
     int n_progpp_1_totalsize;
     int n_progpp_1_t;
     int T_DOT_1_entry, n_spds_1_offset, n_spds_1_parent, n_spds_1_totalsize, n_spds_1_t, n_cmpd_1_t;
     int *n_spds_1_data, *n_cmpd_1_data;
     int t1;
     switch(tok->tag) {
     case T_PROC: ;
          n_spds_1_offset = n_progpp_1_offset;
          n_spds_1_parent = n_progpp_1_parent;
          n_spds_1_data = n_spds(n_spds_1_parent,n_spds_1_offset);
          n_spds_1_totalsize = n_spds_1_data[0];
          n_spds_1_t = n_spds_1_data[1];
          free(n_spds_1_data);

          n_cmpd_1_data = n_cmpd();
          n_cmpd_1_t = n_cmpd_1_data[0];
          free(n_cmpd_1_data);

          T_DOT_1_entry = match(T_DOT);
          n_progpp_1_totalsize = n_spds_1_totalsize;
          if (n_spds_1_t == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else if (n_cmpd_1_t == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else {
               t1 = TYPE_VOID;
          }
          n_progpp_1_t = t1;
          break;
     case T_BEG: ;

          n_cmpd_1_data = n_cmpd();
          n_cmpd_1_t = n_cmpd_1_data[0];
          free(n_cmpd_1_data);

          T_DOT_1_entry = match(T_DOT);
          n_progpp_1_totalsize = n_progpp_1_offset;
          n_progpp_1_t = n_cmpd_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_7);
          while(!synch(SYNC_n_progpp, 1, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 2);
     values[0] = n_progpp_1_totalsize;
     values[1] = n_progpp_1_t;
     return values;
}
int SYNC_n_idtl[1] = {T_RPAR};
int* n_idtl() {
     int n_idtl_1_t;
     int T_ID_1_entry, n_idtlp_1_t;
     int *n_idtlp_1_data;
     switch(tok->tag) {
     case T_ID: ;

          T_ID_1_entry = match(T_ID);

          n_idtlp_1_data = n_idtlp();
          n_idtlp_1_t = n_idtlp_1_data[0];
          free(n_idtlp_1_data);
          n_idtl_1_t = n_idtlp_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_8);
          while(!synch(SYNC_n_idtl, 1, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_idtl_1_t;
     return values;
}
int SYNC_n_idtlp[1] = {T_RPAR};
int* n_idtlp() {
     int n_idtlp_1_t;
     int T_COM_1_entry, T_ID_1_entry, T_EP_1_entry, n_idtlp_2_t;
     int *n_idtlp_2_data;
     switch(tok->tag) {
     case T_COM: ;

          T_COM_1_entry = match(T_COM);

          T_ID_1_entry = match(T_ID);

          n_idtlp_2_data = n_idtlp();
          n_idtlp_2_t = n_idtlp_2_data[0];
          free(n_idtlp_2_data);
          n_idtlp_1_t = n_idtlp_2_t;
          break;
     case T_RPAR: ;

          //EPSILON
          n_idtlp_1_t = TYPE_VOID;
          break;
     default:
          report_error(tok->line, SYNERR_9);
          while(!synch(SYNC_n_idtlp, 1, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_idtlp_1_t;
     return values;
}
int SYNC_n_decl[2] = {T_BEG, T_PROC};
int* n_decl(int n_decl_1_offset) {
     int n_decl_1_totalsize;
     int n_decl_1_t;
     int n_declp_1_offset, n_declp_1_t, n_declp_1_totalsize;
     int *n_declp_1_data;
     switch(tok->tag) {
     case T_VAR: ;
     case T_BEG: ;
     case T_PROC: ;
          n_declp_1_offset = 0;
          n_declp_1_data = n_declp(n_declp_1_offset);
          n_declp_1_t = n_declp_1_data[0];
          n_declp_1_totalsize = n_declp_1_data[1];
          free(n_declp_1_data);
          n_decl_1_t = n_declp_1_t;
          n_decl_1_totalsize = n_declp_1_totalsize;
          break;
     default:
          report_error(tok->line, SYNERR_10);
          while(!synch(SYNC_n_decl, 2, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 2);
     values[0] = n_decl_1_totalsize;
     values[1] = n_decl_1_t;
     return values;
}
int SYNC_n_declp[2] = {T_BEG, T_PROC};
int* n_declp(int n_declp_1_offset) {
     int n_declp_1_t;
     int n_declp_1_totalsize;
     int T_VAR_1_entry, T_ID_1_entry, T_C_1_entry, T_SC_1_entry, T_EP_1_entry, n_declp_1_temp1, n_type_1_t, n_type_1_width, n_declp_1_temp2, n_declp_2_offset, n_declp_2_t, n_declp_2_totalsize;
     int *n_type_1_data, *n_declp_2_data;
     int t1, t2, t3;
     switch(tok->tag) {
     case T_VAR: ;

          T_VAR_1_entry = match(T_VAR);

          T_ID_1_entry = match(T_ID);

          T_C_1_entry = match(T_C);

          n_type_1_data = n_type();
          n_type_1_t = n_type_1_data[0];
          n_type_1_width = n_type_1_data[1];
          free(n_type_1_data);

          T_SC_1_entry = match(T_SC);
          t1 = check_add_green_node(T_ID_1_entry, n_type_1_t, n_declp_1_offset, n_type_1_width);
          n_declp_1_temp1 = t1;
          t2 = n_declp_1_offset + n_type_1_width;
          n_declp_1_temp2 = t2;
          if (n_declp_1_temp1 != TYPE_ERR ) {
               t3 = n_declp_1_temp2;
          }
          else {
               t3 = n_declp_1_offset;
          }
          n_declp_2_offset = t3;
          n_declp_2_data = n_declp(n_declp_2_offset);
          n_declp_2_t = n_declp_2_data[0];
          n_declp_2_totalsize = n_declp_2_data[1];
          free(n_declp_2_data);
          n_declp_1_t = n_declp_2_t;
          n_declp_1_totalsize = n_declp_2_totalsize;
          break;
     case T_BEG: ;
     case T_PROC: ;

          //EPSILON
          n_declp_1_totalsize = n_declp_1_offset;
          n_declp_1_t = TYPE_VOID;
          break;
     default:
          report_error(tok->line, SYNERR_11);
          while(!synch(SYNC_n_declp, 2, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 2);
     values[0] = n_declp_1_t;
     values[1] = n_declp_1_totalsize;
     return values;
}
int SYNC_n_type[2] = {T_RPAR, T_SC};
int* n_type() {
     int n_type_1_t;
     int n_type_1_width;
     int T_ARR_1_entry, T_LBR_1_entry, T_NUM_1_entry, T_DD_1_entry, T_NUM_2_entry, T_RBR_1_entry, T_OF_1_entry, n_stdt_1_t, n_stdt_1_width;
     int *n_stdt_1_data;
     int t1, t2, t3, t4, t5;
     switch(tok->tag) {
     case T_REAL: ;
     case T_INT: ;

          n_stdt_1_data = n_stdt();
          n_stdt_1_t = n_stdt_1_data[0];
          n_stdt_1_width = n_stdt_1_data[1];
          free(n_stdt_1_data);
          n_type_1_t = n_stdt_1_t;
          n_type_1_width = n_stdt_1_width;
          break;
     case T_ARR: ;

          T_ARR_1_entry = match(T_ARR);

          T_LBR_1_entry = match(T_LBR);

          T_NUM_1_entry = match(T_NUM);

          T_DD_1_entry = match(T_DD);

          T_NUM_2_entry = match(T_NUM);

          T_RBR_1_entry = match(T_RBR);

          T_OF_1_entry = match(T_OF);

          n_stdt_1_data = n_stdt();
          n_stdt_1_t = n_stdt_1_data[0];
          n_stdt_1_width = n_stdt_1_data[1];
          free(n_stdt_1_data);
          if (n_stdt_1_t == TYPE_INT ) {
               t1 = TYPE_AINT;
          }
          else if (n_stdt_1_t == TYPE_REAL ) {
               t1 = TYPE_AREAL;
          }
          else {
               t1 = TYPE_ERR;
          }
          n_type_1_t = t1;
          t2 = get_number_a(T_NUM_2_entry);
          t3 = get_number_a(T_NUM_1_entry);
          t4 = t2 - t3;
          t5 = n_stdt_1_width * t4;
          n_type_1_width = t5;
          break;
     default:
          report_error(tok->line, SYNERR_12);
          while(!synch(SYNC_n_type, 2, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 2);
     values[0] = n_type_1_t;
     values[1] = n_type_1_width;
     return values;
}
int SYNC_n_stdt[2] = {T_RPAR, T_SC};
int* n_stdt() {
     int n_stdt_1_t;
     int n_stdt_1_width;
     int T_INT_1_entry, T_REAL_1_entry;
     switch(tok->tag) {
     case T_INT: ;

          T_INT_1_entry = match(T_INT);
          n_stdt_1_t = TYPE_INT;
          n_stdt_1_width = 4;
          break;
     case T_REAL: ;

          T_REAL_1_entry = match(T_REAL);
          n_stdt_1_t = TYPE_REAL;
          n_stdt_1_width = 8;
          break;
     default:
          report_error(tok->line, SYNERR_13);
          while(!synch(SYNC_n_stdt, 2, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 2);
     values[0] = n_stdt_1_t;
     values[1] = n_stdt_1_width;
     return values;
}
int SYNC_n_spds[1] = {T_BEG};
int* n_spds(int n_spds_1_parent, int n_spds_1_offset) {
     int n_spds_1_totalsize;
     int n_spds_1_t;
     int n_spdsp_1_offset, n_spdsp_1_parent, n_spdsp_1_totalsize, n_spdsp_1_t;
     int *n_spdsp_1_data;
     switch(tok->tag) {
     case T_BEG: ;
     case T_PROC: ;
          n_spdsp_1_offset = n_spds_1_offset;
          n_spdsp_1_parent = n_spds_1_parent;
          n_spdsp_1_data = n_spdsp(n_spdsp_1_offset,n_spdsp_1_parent);
          n_spdsp_1_totalsize = n_spdsp_1_data[0];
          n_spdsp_1_t = n_spdsp_1_data[1];
          free(n_spdsp_1_data);
          n_spds_1_totalsize = n_spdsp_1_totalsize;
          n_spds_1_t = n_spdsp_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_14);
          while(!synch(SYNC_n_spds, 1, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 2);
     values[0] = n_spds_1_totalsize;
     values[1] = n_spds_1_t;
     return values;
}
int SYNC_n_spdsp[1] = {T_BEG};
int* n_spdsp(int n_spdsp_1_offset, int n_spdsp_1_parent) {
     int n_spdsp_1_totalsize;
     int n_spdsp_1_t;
     int T_SC_1_entry, T_EP_1_entry, n_spd_1_offset, n_spd_1_parent, n_spdsp_2_offset, n_spd_1_totalsize, n_spdsp_2_parent, n_spdsp_2_totalsize, n_spd_1_t, n_spdsp_2_t;
     int *n_spd_1_data, *n_spdsp_2_data;
     int t1;
     switch(tok->tag) {
     case T_PROC: ;
          n_spd_1_offset = n_spdsp_1_offset;
          n_spd_1_parent = n_spdsp_1_parent;
          n_spd_1_data = n_spd(n_spd_1_offset,n_spd_1_parent);
          n_spd_1_totalsize = n_spd_1_data[0];
          n_spd_1_t = n_spd_1_data[1];
          free(n_spd_1_data);
          n_spdsp_2_offset = n_spd_1_totalsize;
          T_SC_1_entry = match(T_SC);
          n_spdsp_2_parent = n_spdsp_1_parent;
          n_spdsp_2_data = n_spdsp(n_spdsp_2_offset,n_spdsp_2_parent);
          n_spdsp_2_totalsize = n_spdsp_2_data[0];
          n_spdsp_2_t = n_spdsp_2_data[1];
          free(n_spdsp_2_data);
          n_spdsp_1_totalsize = n_spdsp_2_totalsize;
          if (n_spd_1_t == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else if (n_spdsp_2_t == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else {
               t1 = TYPE_VOID;
          }
          n_spdsp_1_t = t1;
          break;
     case T_BEG: ;

          //EPSILON
          n_spdsp_1_totalsize = n_spdsp_1_offset;
          n_spdsp_1_t = TYPE_VOID;
          break;
     default:
          report_error(tok->line, SYNERR_15);
          while(!synch(SYNC_n_spdsp, 1, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 2);
     values[0] = n_spdsp_1_totalsize;
     values[1] = n_spdsp_1_t;
     return values;
}
int SYNC_n_spd[1] = {T_SC};
int* n_spd(int n_spd_1_offset, int n_spd_1_parent) {
     int n_spd_1_totalsize;
     int n_spd_1_t;
     int n_sph_1_offset, n_sph_1_parent, n_spdp_1_offset, n_sph_1_totalsize, n_spdp_1_parent, n_sph_1_entry, n_spdp_1_totalsize, n_sph_1_t, n_spdp_1_t;
     int *n_sph_1_data, *n_spdp_1_data;
     int t1, t2;
     switch(tok->tag) {
     case T_PROC: ;
          n_sph_1_offset = 0;
          n_sph_1_parent = n_spd_1_parent;
          n_sph_1_data = n_sph(n_sph_1_offset,n_sph_1_parent);
          n_sph_1_totalsize = n_sph_1_data[0];
          n_sph_1_entry = n_sph_1_data[1];
          n_sph_1_t = n_sph_1_data[2];
          free(n_sph_1_data);
          n_spdp_1_offset = n_sph_1_totalsize;
          n_spdp_1_parent = n_sph_1_entry;
          n_spdp_1_data = n_spdp(n_spdp_1_offset,n_spdp_1_parent);
          n_spdp_1_totalsize = n_spdp_1_data[0];
          n_spdp_1_t = n_spdp_1_data[1];
          free(n_spdp_1_data);
          n_spd_1_totalsize = n_spdp_1_totalsize;
          if (n_sph_1_t == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else if (n_spdp_1_t == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else {
               t1 = TYPE_VOID;
          }
          n_spd_1_t = t1;
          t2 = pop_blue_node();

          break;
     default:
          report_error(tok->line, SYNERR_16);
          while(!synch(SYNC_n_spd, 1, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 2);
     values[0] = n_spd_1_totalsize;
     values[1] = n_spd_1_t;
     return values;
}
int SYNC_n_spdp[1] = {T_SC};
int* n_spdp(int n_spdp_1_offset, int n_spdp_1_parent) {
     int n_spdp_1_totalsize;
     int n_spdp_1_t;
     int n_spds_1_offset, n_spds_1_parent, n_spds_1_totalsize, n_spds_1_t, n_cmpd_1_t, n_decl_1_offset, n_spdpp_1_offset, n_decl_1_totalsize, n_spdpp_1_parent, n_spdpp_1_totalsize, n_decl_1_t, n_spdpp_1_t;
     int *n_spds_1_data, *n_cmpd_1_data, *n_decl_1_data, *n_spdpp_1_data;
     int t1, t2;
     switch(tok->tag) {
     case T_PROC: ;
          n_spds_1_offset = n_spdp_1_offset;
          n_spds_1_parent = n_spdp_1_parent;
          n_spds_1_data = n_spds(n_spds_1_parent,n_spds_1_offset);
          n_spds_1_totalsize = n_spds_1_data[0];
          n_spds_1_t = n_spds_1_data[1];
          free(n_spds_1_data);
          n_spdp_1_totalsize = n_spds_1_totalsize;
          n_cmpd_1_data = n_cmpd();
          n_cmpd_1_t = n_cmpd_1_data[0];
          free(n_cmpd_1_data);
          if (n_spds_1_t == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else if (n_cmpd_1_t == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else {
               t1 = TYPE_VOID;
          }
          n_spdp_1_t = t1;
          break;
     case T_BEG: ;

          n_cmpd_1_data = n_cmpd();
          n_cmpd_1_t = n_cmpd_1_data[0];
          free(n_cmpd_1_data);
          n_spdp_1_totalsize = n_spdp_1_offset;
          n_spdp_1_t = n_cmpd_1_t;
          break;
     case T_VAR: ;
          n_decl_1_offset = n_spdp_1_offset;
          n_decl_1_data = n_decl(n_decl_1_offset);
          n_decl_1_totalsize = n_decl_1_data[0];
          n_decl_1_t = n_decl_1_data[1];
          free(n_decl_1_data);
          n_spdpp_1_offset = n_decl_1_totalsize;
          n_spdpp_1_parent = n_spdp_1_parent;
          n_spdpp_1_data = n_spdpp(n_spdpp_1_offset,n_spdpp_1_parent);
          n_spdpp_1_totalsize = n_spdpp_1_data[0];
          n_spdpp_1_t = n_spdpp_1_data[1];
          free(n_spdpp_1_data);
          n_spdp_1_totalsize = n_spdpp_1_totalsize;
          if (n_decl_1_t == TYPE_ERR ) {
               t2 = TYPE_ERR;
          }
          else if (n_spdpp_1_t == TYPE_ERR ) {
               t2 = TYPE_ERR;
          }
          else {
               t2 = TYPE_VOID;
          }
          n_spdp_1_t = t2;
          break;
     default:
          report_error(tok->line, SYNERR_17);
          while(!synch(SYNC_n_spdp, 1, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 2);
     values[0] = n_spdp_1_totalsize;
     values[1] = n_spdp_1_t;
     return values;
}
int SYNC_n_spdpp[1] = {T_SC};
int* n_spdpp(int n_spdpp_1_offset, int n_spdpp_1_parent) {
     int n_spdpp_1_totalsize;
     int n_spdpp_1_t;
     int n_spds_1_offset, n_spds_1_parent, n_spds_1_totalsize, n_spds_1_t, n_cmpd_1_t;
     int *n_spds_1_data, *n_cmpd_1_data;
     int t1;
     switch(tok->tag) {
     case T_PROC: ;
          n_spds_1_offset = n_spdpp_1_offset;
          n_spds_1_parent = n_spdpp_1_parent;
          n_spds_1_data = n_spds(n_spds_1_parent,n_spds_1_offset);
          n_spds_1_totalsize = n_spds_1_data[0];
          n_spds_1_t = n_spds_1_data[1];
          free(n_spds_1_data);
          n_spdpp_1_totalsize = n_spds_1_totalsize;
          n_cmpd_1_data = n_cmpd();
          n_cmpd_1_t = n_cmpd_1_data[0];
          free(n_cmpd_1_data);
          if (n_spds_1_t == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else if (n_cmpd_1_t == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else {
               t1 = TYPE_VOID;
          }
          n_spdpp_1_t = t1;
          break;
     case T_BEG: ;

          n_cmpd_1_data = n_cmpd();
          n_cmpd_1_t = n_cmpd_1_data[0];
          free(n_cmpd_1_data);
          n_spdpp_1_totalsize = n_spdpp_1_offset;
          n_spdpp_1_t = n_cmpd_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_18);
          while(!synch(SYNC_n_spdpp, 1, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 2);
     values[0] = n_spdpp_1_totalsize;
     values[1] = n_spdpp_1_t;
     return values;
}
int SYNC_n_sph[3] = {T_BEG, T_VAR, T_PROC};
int* n_sph(int n_sph_1_offset, int n_sph_1_parent) {
     int n_sph_1_totalsize;
     int n_sph_1_entry;
     int n_sph_1_t;
     int T_PROC_1_entry, T_ID_1_entry, n_sphp_1_offset, n_sph_1_temp1, n_sphp_1_i, n_sphp_1_totalsize, n_sphp_1_t;
     int *n_sphp_1_data;
     int t1, t2, t3, t4;
     switch(tok->tag) {
     case T_PROC: ;

          T_PROC_1_entry = match(T_PROC);

          T_ID_1_entry = match(T_ID);
          n_sphp_1_offset = n_sph_1_offset;
          t1 = check_add_blue_node(T_ID_1_entry);
          n_sph_1_temp1 = t1;
          t2 = add_subproc(n_sph_1_temp1, n_sph_1_parent);

          if (n_sph_1_temp1 == 0 ) {
               t3 = TYPE_ERR;
          }
          else {
               t3 = TYPE_VOID;
          }
          n_sphp_1_i = t3;
          if (n_sphp_1_i != TYPE_ERR ) {
               t4 = T_ID_1_entry;
          }
          else {
               t4 = 0;
          }
          n_sph_1_entry = t4;
          n_sphp_1_data = n_sphp(n_sphp_1_offset,n_sphp_1_i);
          n_sphp_1_totalsize = n_sphp_1_data[0];
          n_sphp_1_t = n_sphp_1_data[1];
          free(n_sphp_1_data);
          n_sph_1_totalsize = n_sphp_1_totalsize;
          n_sph_1_t = n_sphp_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_19);
          while(!synch(SYNC_n_sph, 3, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 3);
     values[0] = n_sph_1_totalsize;
     values[1] = n_sph_1_entry;
     values[2] = n_sph_1_t;
     return values;
}
int SYNC_n_sphp[3] = {T_BEG, T_VAR, T_PROC};
int* n_sphp(int n_sphp_1_offset, int n_sphp_1_i) {
     int n_sphp_1_totalsize;
     int n_sphp_1_t;
     int T_SC_1_entry, n_args_1_offset, n_args_1_totalsize, n_args_1_t;
     int *n_args_1_data;
     switch(tok->tag) {
     case T_LPAR: ;
          n_args_1_offset = n_sphp_1_offset;
          n_args_1_data = n_args(n_args_1_offset);
          n_args_1_totalsize = n_args_1_data[0];
          n_args_1_t = n_args_1_data[1];
          free(n_args_1_data);

          T_SC_1_entry = match(T_SC);
          n_sphp_1_totalsize = n_args_1_totalsize;
          n_sphp_1_t = n_args_1_t;
          break;
     case T_SC: ;

          T_SC_1_entry = match(T_SC);
          n_sphp_1_totalsize = n_sphp_1_offset;
          n_sphp_1_t = TYPE_VOID;
          break;
     default:
          report_error(tok->line, SYNERR_20);
          while(!synch(SYNC_n_sphp, 3, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 2);
     values[0] = n_sphp_1_totalsize;
     values[1] = n_sphp_1_t;
     return values;
}
int SYNC_n_args[1] = {T_SC};
int* n_args(int n_args_1_offset) {
     int n_args_1_totalsize;
     int n_args_1_t;
     int T_LPAR_1_entry, T_RPAR_1_entry, n_plst_1_t;
     int *n_plst_1_data;
     switch(tok->tag) {
     case T_LPAR: ;

          T_LPAR_1_entry = match(T_LPAR);

          n_plst_1_data = n_plst();
          n_plst_1_t = n_plst_1_data[0];
          free(n_plst_1_data);

          T_RPAR_1_entry = match(T_RPAR);
          n_args_1_t = n_plst_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_21);
          while(!synch(SYNC_n_args, 1, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 2);
     values[0] = n_args_1_totalsize;
     values[1] = n_args_1_t;
     return values;
}
int SYNC_n_plst[1] = {T_RPAR};
int* n_plst() {
     int n_plst_1_t;
     int T_ID_1_entry, T_C_1_entry, n_type_1_t, n_type_1_width, n_plstp_1_t;
     int *n_type_1_data, *n_plstp_1_data;
     int t1, t2;
     switch(tok->tag) {
     case T_ID: ;

          T_ID_1_entry = match(T_ID);

          T_C_1_entry = match(T_C);

          n_type_1_data = n_type();
          n_type_1_t = n_type_1_data[0];
          n_type_1_width = n_type_1_data[1];
          free(n_type_1_data);
          t1 = add_param(T_ID_1_entry, n_type_1_t);

          n_plstp_1_data = n_plstp();
          n_plstp_1_t = n_plstp_1_data[0];
          free(n_plstp_1_data);
          n_type_1_width = 0;
          if (n_plstp_1_t == TYPE_ERR ) {
               t2 = TYPE_ERR;
          }
          else if (n_type_1_t == TYPE_ERR ) {
               t2 = TYPE_ERR;
          }
          else {
               t2 = n_plstp_1_t;
          }
          n_plst_1_t = t2;
          break;
     default:
          report_error(tok->line, SYNERR_22);
          while(!synch(SYNC_n_plst, 1, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_plst_1_t;
     return values;
}
int SYNC_n_plstp[1] = {T_RPAR};
int* n_plstp() {
     int n_plstp_1_t;
     int T_SC_1_entry, T_ID_1_entry, T_C_1_entry, T_EP_1_entry, n_type_1_t, n_type_1_width, n_plstp_2_t;
     int *n_type_1_data, *n_plstp_2_data;
     int t1, t2;
     switch(tok->tag) {
     case T_SC: ;

          T_SC_1_entry = match(T_SC);

          T_ID_1_entry = match(T_ID);

          T_C_1_entry = match(T_C);

          n_type_1_data = n_type();
          n_type_1_t = n_type_1_data[0];
          n_type_1_width = n_type_1_data[1];
          free(n_type_1_data);
          t1 = add_param(T_ID_1_entry, n_type_1_t);

          n_type_1_width = 0;
          n_plstp_2_data = n_plstp();
          n_plstp_2_t = n_plstp_2_data[0];
          free(n_plstp_2_data);
          if (n_plstp_2_t == TYPE_ERR ) {
               t2 = TYPE_ERR;
          }
          else if (n_type_1_t == TYPE_ERR ) {
               t2 = TYPE_ERR;
          }
          else {
               t2 = n_plstp_2_t;
          }
          n_plstp_1_t = t2;
          break;
     case T_RPAR: ;

          //EPSILON
          n_plstp_1_t = TYPE_VOID;
          break;
     default:
          report_error(tok->line, SYNERR_23);
          while(!synch(SYNC_n_plstp, 1, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_plstp_1_t;
     return values;
}
int SYNC_n_cmpd[4] = {T_END, T_EL, T_DOT, T_SC};
int* n_cmpd() {
     int n_cmpd_1_t;
     int T_BEG_1_entry, n_cmpdp_1_t;
     int *n_cmpdp_1_data;
     switch(tok->tag) {
     case T_BEG: ;

          T_BEG_1_entry = match(T_BEG);

          n_cmpdp_1_data = n_cmpdp();
          n_cmpdp_1_t = n_cmpdp_1_data[0];
          free(n_cmpdp_1_data);
          n_cmpd_1_t = n_cmpdp_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_24);
          while(!synch(SYNC_n_cmpd, 4, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_cmpd_1_t;
     return values;
}
int SYNC_n_cmpdp[4] = {T_SC, T_END, T_DOT, T_EL};
int* n_cmpdp() {
     int n_cmpdp_1_t;
     int T_END_1_entry, n_ostmt_1_t;
     int *n_ostmt_1_data;
     switch(tok->tag) {
     case T_ID: ;
     case T_BEG: ;
     case T_IF: ;
     case T_CALL: ;
     case T_WH: ;

          n_ostmt_1_data = n_ostmt();
          n_ostmt_1_t = n_ostmt_1_data[0];
          free(n_ostmt_1_data);

          T_END_1_entry = match(T_END);
          n_cmpdp_1_t = n_ostmt_1_t;
          break;
     case T_END: ;

          T_END_1_entry = match(T_END);
          n_cmpdp_1_t = TYPE_VOID;
          break;
     default:
          report_error(tok->line, SYNERR_25);
          while(!synch(SYNC_n_cmpdp, 4, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_cmpdp_1_t;
     return values;
}
int SYNC_n_ostmt[1] = {T_END};
int* n_ostmt() {
     int n_ostmt_1_t;
     int n_stmtlst_1_t;
     int *n_stmtlst_1_data;
     switch(tok->tag) {
     case T_ID: ;
     case T_BEG: ;
     case T_IF: ;
     case T_CALL: ;
     case T_WH: ;

          n_stmtlst_1_data = n_stmtlst();
          n_stmtlst_1_t = n_stmtlst_1_data[0];
          free(n_stmtlst_1_data);
          n_ostmt_1_t = n_stmtlst_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_26);
          while(!synch(SYNC_n_ostmt, 1, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_ostmt_1_t;
     return values;
}
int SYNC_n_stmtlst[1] = {T_END};
int* n_stmtlst() {
     int n_stmtlst_1_t;
     int n_stmt_1_t, n_stmtlstp_1_t;
     int *n_stmt_1_data, *n_stmtlstp_1_data;
     int t1;
     switch(tok->tag) {
     case T_ID: ;
     case T_BEG: ;
     case T_IF: ;
     case T_CALL: ;
     case T_WH: ;

          n_stmt_1_data = n_stmt();
          n_stmt_1_t = n_stmt_1_data[0];
          free(n_stmt_1_data);

          n_stmtlstp_1_data = n_stmtlstp();
          n_stmtlstp_1_t = n_stmtlstp_1_data[0];
          free(n_stmtlstp_1_data);
          if (n_stmt_1_t == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else {
               t1 = n_stmtlstp_1_t;
          }
          n_stmtlst_1_t = t1;
          break;
     default:
          report_error(tok->line, SYNERR_27);
          while(!synch(SYNC_n_stmtlst, 1, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_stmtlst_1_t;
     return values;
}
int SYNC_n_stmtlstp[1] = {T_END};
int* n_stmtlstp() {
     int n_stmtlstp_1_t;
     int T_SC_1_entry, T_EP_1_entry, n_stmt_1_t, n_stmtlstp_2_t;
     int *n_stmt_1_data, *n_stmtlstp_2_data;
     int t1;
     switch(tok->tag) {
     case T_SC: ;

          T_SC_1_entry = match(T_SC);

          n_stmt_1_data = n_stmt();
          n_stmt_1_t = n_stmt_1_data[0];
          free(n_stmt_1_data);

          n_stmtlstp_2_data = n_stmtlstp();
          n_stmtlstp_2_t = n_stmtlstp_2_data[0];
          free(n_stmtlstp_2_data);
          if (n_stmt_1_t == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else {
               t1 = n_stmtlstp_2_t;
          }
          n_stmtlstp_1_t = t1;
          break;
     case T_END: ;

          //EPSILON
          n_stmtlstp_1_t = TYPE_VOID;
          break;
     default:
          report_error(tok->line, SYNERR_28);
          while(!synch(SYNC_n_stmtlstp, 1, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_stmtlstp_1_t;
     return values;
}
int SYNC_n_stmt[3] = {T_EL, T_END, T_SC};
int* n_stmt() {
     int n_stmt_1_t;
     int T_ASS_1_entry, T_WH_1_entry, T_DO_1_entry, T_IF_1_entry, T_TH_1_entry, n_var_1_t, n_expr_1_t, n_proc_1_t, n_cmpd_1_t, n_stmt_2_t, n_stmtp_1_i, n_stmtp_1_t;
     int *n_var_1_data, *n_expr_1_data, *n_proc_1_data, *n_cmpd_1_data, *n_stmt_2_data, *n_stmtp_1_data;
     int t1, t2, t3;
     switch(tok->tag) {
     case T_ID: ;

          n_var_1_data = n_var();
          n_var_1_t = n_var_1_data[0];
          free(n_var_1_data);

          T_ASS_1_entry = match(T_ASS);

          n_expr_1_data = n_expr();
          n_expr_1_t = n_expr_1_data[0];
          free(n_expr_1_data);
          if (n_var_1_t == n_expr_1_t ) {
               t1 = TYPE_VOID;
          }
          else if (n_var_1_t == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else if (n_expr_1_t == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else {
               report_error(tok->line, SEMERR_1);
               t1 = TYPE_ERR;
          }
          n_stmt_1_t = t1;
          break;
     case T_CALL: ;

          n_proc_1_data = n_proc();
          n_proc_1_t = n_proc_1_data[0];
          free(n_proc_1_data);
          n_stmt_1_t = n_proc_1_t;
          break;
     case T_BEG: ;

          n_cmpd_1_data = n_cmpd();
          n_cmpd_1_t = n_cmpd_1_data[0];
          free(n_cmpd_1_data);

          break;
     case T_WH: ;

          T_WH_1_entry = match(T_WH);

          n_expr_1_data = n_expr();
          n_expr_1_t = n_expr_1_data[0];
          free(n_expr_1_data);

          T_DO_1_entry = match(T_DO);

          n_stmt_2_data = n_stmt();
          n_stmt_2_t = n_stmt_2_data[0];
          free(n_stmt_2_data);
          if (n_expr_1_t == TYPE_BOOLEAN ) {
               t2 = n_stmt_2_t;
          }
          else if (n_expr_1_t == TYPE_ERR ) {
               t2 = TYPE_ERR;
          }
          else {
               report_error(tok->line, SEMERR_2);
               t2 = TYPE_ERR;
          }
          n_stmt_1_t = t2;
          break;
     case T_IF: ;

          T_IF_1_entry = match(T_IF);

          n_expr_1_data = n_expr();
          n_expr_1_t = n_expr_1_data[0];
          free(n_expr_1_data);

          T_TH_1_entry = match(T_TH);

          n_stmt_2_data = n_stmt();
          n_stmt_2_t = n_stmt_2_data[0];
          free(n_stmt_2_data);
          n_stmtp_1_i = n_stmt_1_t;
          n_stmtp_1_data = n_stmtp(n_stmtp_1_i);
          n_stmtp_1_t = n_stmtp_1_data[0];
          free(n_stmtp_1_data);
          if (n_expr_1_t == TYPE_BOOLEAN ) {
               t3 = n_stmtp_1_t;
          }
          else if (n_expr_1_t == TYPE_ERR ) {
               t3 = TYPE_ERR;
          }
          else {
               report_error(tok->line, SEMERR_3);
               t3 = TYPE_ERR;
          }
          n_stmt_1_t = t3;
          break;
     default:
          report_error(tok->line, SYNERR_29);
          while(!synch(SYNC_n_stmt, 3, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_stmt_1_t;
     return values;
}
int SYNC_n_stmtp[2] = {T_SC, T_END};
int* n_stmtp(int n_stmtp_1_i) {
     int n_stmtp_1_t;
     int T_EP_1_entry, T_EL_1_entry, n_stmt_1_t;
     int *n_stmt_1_data;
     int t1;
     switch(tok->tag) {
     case T_SC: ;
     case T_END: ;

          //EPSILON
          n_stmtp_1_t = n_stmtp_1_i;
          break;
     case T_EL: ;

          T_EL_1_entry = match(T_EL);

          n_stmt_1_data = n_stmt();
          n_stmt_1_t = n_stmt_1_data[0];
          free(n_stmt_1_data);
          if (n_stmt_1_t == TYPE_VOID && n_stmtp_1_i == TYPE_VOID ) {
               t1 = TYPE_VOID;
          }
          else {
               t1 = TYPE_ERR;
          }
          n_stmtp_1_t = t1;
          break;
     default:
          report_error(tok->line, SYNERR_30);
          while(!synch(SYNC_n_stmtp, 2, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_stmtp_1_t;
     return values;
}
int SYNC_n_var[1] = {T_ASS};
int* n_var() {
     int n_var_1_t;
     int T_ID_1_entry, n_varp_1_i, n_varp_1_t;
     int *n_varp_1_data;
     int t1;
     switch(tok->tag) {
     case T_ID: ;

          T_ID_1_entry = match(T_ID);
          t1 = lookup(T_ID_1_entry);
          n_varp_1_i = t1;
          n_varp_1_data = n_varp(n_varp_1_i);
          n_varp_1_t = n_varp_1_data[0];
          free(n_varp_1_data);
          n_var_1_t = n_varp_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_31);
          while(!synch(SYNC_n_var, 1, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_var_1_t;
     return values;
}
int SYNC_n_varp[1] = {T_ASS};
int* n_varp(int n_varp_1_i) {
     int n_varp_1_t;
     int T_EP_1_entry, T_LBR_1_entry, T_RBR_1_entry, n_expr_1_t;
     int *n_expr_1_data;
     int t1;
     switch(tok->tag) {
     case T_ASS: ;

          //EPSILON
          n_varp_1_t = n_varp_1_i;
          break;
     case T_LBR: ;

          T_LBR_1_entry = match(T_LBR);

          n_expr_1_data = n_expr();
          n_expr_1_t = n_expr_1_data[0];
          free(n_expr_1_data);

          T_RBR_1_entry = match(T_RBR);
          if (n_varp_1_i == TYPE_AINT && n_expr_1_t == TYPE_INT ) {
               t1 = TYPE_INT;
          }
          else if (n_varp_1_i == TYPE_AREAL && n_expr_1_t == TYPE_INT ) {
               t1 = TYPE_REAL;
          }
          else if (n_varp_1_i == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else if (n_varp_1_i != TYPE_AINT && n_varp_1_i != TYPE_AREAL ) {
               report_error(tok->line, SEMERR_4);
               t1 = TYPE_ERR;
          }
          else if (n_expr_1_t != TYPE_INT && n_expr_1_t != TYPE_ERR ) {
               report_error(tok->line, SEMERR_5);
               t1 = TYPE_ERR;
          }
          else {
               t1 = TYPE_ERR;
          }
          n_varp_1_t = t1;
          break;
     default:
          report_error(tok->line, SYNERR_32);
          while(!synch(SYNC_n_varp, 1, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_varp_1_t;
     return values;
}
int SYNC_n_proc[3] = {T_SC, T_EL, T_END};
int* n_proc() {
     int n_proc_1_t;
     int T_CALL_1_entry, T_ID_1_entry, n_proc_1_temp1, n_procp_1_i, n_procp_1_t;
     int *n_procp_1_data;
     int t1, t2, t3, t4;
     switch(tok->tag) {
     case T_CALL: ;

          T_CALL_1_entry = match(T_CALL);

          T_ID_1_entry = match(T_ID);
          t1 = in_scope(T_ID_1_entry);
          n_proc_1_temp1 = t1;
          t2 = push_params(n_proc_1_temp1);
          n_procp_1_i = t2;
          n_procp_1_data = n_procp(n_procp_1_i);
          n_procp_1_t = n_procp_1_data[0];
          free(n_procp_1_data);
          if (n_proc_1_temp1 == 0 ) {
               t3 = TYPE_ERR;
          }
          else if (n_procp_1_t == TYPE_ERR ) {
               t3 = TYPE_ERR;
          }
          else {
               t3 = TYPE_VOID;
          }
          n_proc_1_t = t3;
          t4 = pop_params(n_proc_1_t);

          break;
     default:
          report_error(tok->line, SYNERR_33);
          while(!synch(SYNC_n_proc, 3, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_proc_1_t;
     return values;
}
int SYNC_n_procp[3] = {T_END, T_EL, T_SC};
int* n_procp(int n_procp_1_i) {
     int n_procp_1_t;
     int T_EP_1_entry, T_LPAR_1_entry, T_RPAR_1_entry, n_exprl_1_i, n_exprl_1_t;
     int *n_exprl_1_data;
     switch(tok->tag) {
     case T_END: ;
     case T_EL: ;
     case T_SC: ;

          //EPSILON
          n_procp_1_t = n_procp_1_i;
          break;
     case T_LPAR: ;

          T_LPAR_1_entry = match(T_LPAR);
          n_exprl_1_i = n_procp_1_i;
          n_exprl_1_data = n_exprl(n_exprl_1_i);
          n_exprl_1_t = n_exprl_1_data[0];
          free(n_exprl_1_data);
          n_procp_1_t = n_exprl_1_t;
          T_RPAR_1_entry = match(T_RPAR);

          break;
     default:
          report_error(tok->line, SYNERR_34);
          while(!synch(SYNC_n_procp, 3, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_procp_1_t;
     return values;
}
int SYNC_n_exprl[1] = {T_RPAR};
int* n_exprl(int n_exprl_1_i) {
     int n_exprl_1_t;
     int n_exprlp_1_i, n_exprl_1_temp1, n_exprlp_1_t, n_expr_1_t;
     int *n_expr_1_data, *n_exprlp_1_data;
     int t1, t2;
     switch(tok->tag) {
     case T_PLS: ;
     case T_ID: ;
     case T_MIN: ;
     case T_LPAR: ;
     case T_NUM: ;
     case T_NOT: ;

          n_expr_1_data = n_expr();
          n_expr_1_t = n_expr_1_data[0];
          free(n_expr_1_data);
          n_exprlp_1_i = n_exprl_1_i;
          n_exprlp_1_data = n_exprlp(n_exprlp_1_i);
          n_exprlp_1_t = n_exprlp_1_data[0];
          free(n_exprlp_1_data);
          t1 = pop_param(n_exprlp_1_t);
          n_exprl_1_temp1 = t1;
          if (n_expr_1_t == TYPE_ERR ) {
               t2 = TYPE_ERR;
          }
          else if (n_exprlp_1_t == TYPE_ERR ) {
               t2 = TYPE_ERR;
          }
          else if (n_exprl_1_temp1 == TYPE_ERR ) {
               t2 = TYPE_ERR;
          }
          else if (n_exprl_1_temp1 != n_expr_1_t ) {
               report_error(tok->line, SEMERR_7);
               t2 = TYPE_ERR;
          }
          else {
               t2 = n_exprlp_1_t;
          }
          n_exprl_1_t = t2;
          break;
     default:
          report_error(tok->line, SYNERR_35);
          while(!synch(SYNC_n_exprl, 1, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_exprl_1_t;
     return values;
}
int SYNC_n_exprlp[1] = {T_RPAR};
int* n_exprlp(int n_exprlp_1_i) {
     int n_exprlp_1_t;
     int T_COM_1_entry, T_EP_1_entry, n_exprlp_2_i, n_exprlp_1_temp1, n_exprlp_2_t, n_expr_1_t;
     int *n_expr_1_data, *n_exprlp_2_data;
     int t1, t2;
     switch(tok->tag) {
     case T_COM: ;

          T_COM_1_entry = match(T_COM);

          n_expr_1_data = n_expr();
          n_expr_1_t = n_expr_1_data[0];
          free(n_expr_1_data);
          n_exprlp_2_i = n_exprlp_1_i;
          n_exprlp_2_data = n_exprlp(n_exprlp_2_i);
          n_exprlp_2_t = n_exprlp_2_data[0];
          free(n_exprlp_2_data);
          t1 = pop_param(n_exprlp_2_t);
          n_exprlp_1_temp1 = t1;
          if (n_expr_1_t == TYPE_ERR ) {
               t2 = TYPE_ERR;
          }
          else if (n_exprlp_2_t == TYPE_ERR ) {
               t2 = TYPE_ERR;
          }
          else if (n_exprlp_1_temp1 == TYPE_ERR ) {
               t2 = TYPE_ERR;
          }
          else if (n_exprlp_1_temp1 != n_expr_1_t ) {
               report_error(tok->line, SEMERR_8);
               t2 = TYPE_ERR;
          }
          else {
               t2 = n_exprlp_2_t;
          }
          n_exprlp_1_t = t2;
          break;
     case T_RPAR: ;

          //EPSILON
          n_exprlp_1_t = n_exprlp_1_i;
          break;
     default:
          report_error(tok->line, SYNERR_36);
          while(!synch(SYNC_n_exprlp, 1, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_exprlp_1_t;
     return values;
}
int SYNC_n_expr[8] = {T_DO, T_EL, T_RPAR, T_RBR, T_END, T_COM, T_TH, T_SC};
int* n_expr() {
     int n_expr_1_t;
     int n_exprp_1_i, n_sexpr_1_t, n_exprp_1_t;
     int *n_sexpr_1_data, *n_exprp_1_data;
     switch(tok->tag) {
     case T_LPAR: ;
     case T_ID: ;
     case T_MIN: ;
     case T_PLS: ;
     case T_NUM: ;
     case T_NOT: ;

          n_sexpr_1_data = n_sexpr();
          n_sexpr_1_t = n_sexpr_1_data[0];
          free(n_sexpr_1_data);
          n_exprp_1_i = n_sexpr_1_t;
          n_exprp_1_data = n_exprp(n_exprp_1_i);
          n_exprp_1_t = n_exprp_1_data[0];
          free(n_exprp_1_data);
          n_expr_1_t = n_exprp_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_37);
          while(!synch(SYNC_n_expr, 8, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_expr_1_t;
     return values;
}
int SYNC_n_exprp[8] = {T_DO, T_EL, T_RPAR, T_RBR, T_END, T_COM, T_TH, T_SC};
int* n_exprp(int n_exprp_1_i) {
     int n_exprp_1_t;
     int T_EP_1_entry, T_REL_1_entry, n_sexpr_1_t;
     int *n_sexpr_1_data;
     int t1;
     switch(tok->tag) {
     case T_DO: ;
     case T_EL: ;
     case T_RPAR: ;
     case T_RBR: ;
     case T_END: ;
     case T_COM: ;
     case T_TH: ;
     case T_SC: ;

          //EPSILON
          n_exprp_1_t = n_exprp_1_i;
          break;
     case T_REL: ;

          T_REL_1_entry = match(T_REL);

          n_sexpr_1_data = n_sexpr();
          n_sexpr_1_t = n_sexpr_1_data[0];
          free(n_sexpr_1_data);
          if (n_exprp_1_i == TYPE_INT && n_sexpr_1_t == TYPE_INT ) {
               t1 = TYPE_BOOLEAN;
          }
          else if (n_exprp_1_i == TYPE_REAL && n_sexpr_1_t == TYPE_REAL ) {
               t1 = TYPE_BOOLEAN;
          }
          else if (n_exprp_1_i == TYPE_REAL && n_sexpr_1_t == TYPE_INT ) {
               t1 = TYPE_BOOLEAN;
          }
          else if (n_exprp_1_i == TYPE_INT && n_sexpr_1_t == TYPE_REAL ) {
               t1 = TYPE_BOOLEAN;
          }
          else if (n_exprp_1_i == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else if (n_sexpr_1_t == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else {
               report_error(tok->line, SEMERR_9);
               t1 = TYPE_ERR;
          }
          n_exprp_1_t = t1;
          break;
     default:
          report_error(tok->line, SYNERR_38);
          while(!synch(SYNC_n_exprp, 8, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_exprp_1_t;
     return values;
}
int SYNC_n_sexpr[9] = {T_DO, T_EL, T_REL, T_RPAR, T_RBR, T_END, T_COM, T_TH, T_SC};
int* n_sexpr() {
     int n_sexpr_1_t;
     int n_sexprp_1_i, n_term_1_t, n_sexprp_1_t;
     int *n_term_1_data, *n_sexprp_1_data, *n_sign_1_data;
     switch(tok->tag) {
     case T_ID: ;
     case T_NUM: ;
     case T_LPAR: ;
     case T_NOT: ;

          n_term_1_data = n_term();
          n_term_1_t = n_term_1_data[0];
          free(n_term_1_data);
          n_sexprp_1_i = n_term_1_t;
          n_sexprp_1_data = n_sexprp(n_sexprp_1_i);
          n_sexprp_1_t = n_sexprp_1_data[0];
          free(n_sexprp_1_data);
          n_sexpr_1_t = n_sexprp_1_t;
          break;
     case T_PLS: ;
     case T_MIN: ;

          n_sign_1_data = n_sign();
          free(n_sign_1_data);
          n_sexprp_1_i = n_term_1_t;
          n_term_1_data = n_term();
          n_term_1_t = n_term_1_data[0];
          free(n_term_1_data);
          n_sexpr_1_t = n_sexprp_1_t;
          n_sexprp_1_data = n_sexprp(n_sexprp_1_i);
          n_sexprp_1_t = n_sexprp_1_data[0];
          free(n_sexprp_1_data);

          break;
     default:
          report_error(tok->line, SYNERR_39);
          while(!synch(SYNC_n_sexpr, 9, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_sexpr_1_t;
     return values;
}
int SYNC_n_sexprp[9] = {T_DO, T_EL, T_REL, T_RPAR, T_RBR, T_END, T_COM, T_TH, T_SC};
int* n_sexprp(int n_sexprp_1_i) {
     int n_sexprp_1_t;
     int T_ADD_1_entry, T_EP_1_entry, n_sexprp_2_i, n_term_1_t, n_sexprp_2_t;
     int *n_term_1_data, *n_sexprp_2_data;
     int t1;
     switch(tok->tag) {
     case T_ADD: ;

          T_ADD_1_entry = match(T_ADD);

          n_term_1_data = n_term();
          n_term_1_t = n_term_1_data[0];
          free(n_term_1_data);
          if (n_sexprp_1_i == TYPE_INT && n_term_1_t == TYPE_INT && T_ADD_1_entry == AOP_MINUS ) {
               t1 = TYPE_INT;
          }
          else if (n_sexprp_1_i == TYPE_INT && n_term_1_t == TYPE_INT && T_ADD_1_entry == AOP_PLUS ) {
               t1 = TYPE_INT;
          }
          else if (n_sexprp_1_i == TYPE_REAL && n_term_1_t == TYPE_REAL && T_ADD_1_entry == AOP_PLUS ) {
               t1 = TYPE_REAL;
          }
          else if (n_sexprp_1_i == TYPE_REAL && n_term_1_t == TYPE_REAL && T_ADD_1_entry == AOP_MINUS ) {
               t1 = TYPE_REAL;
          }
          else if (n_sexprp_1_i == TYPE_BOOLEAN && n_term_1_t == TYPE_BOOLEAN && T_ADD_1_entry == AOP_OR ) {
               t1 = TYPE_BOOLEAN;
          }
          else if (n_sexprp_1_i == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else if (n_term_1_t == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else if (T_ADD_1_entry == AOP_MINUS ) {
               report_error(tok->line, SEMERR_10);
               t1 = TYPE_ERR;
          }
          else if (T_ADD_1_entry == AOP_PLUS ) {
               report_error(tok->line, SEMERR_11);
               t1 = TYPE_ERR;
          }
          else if (T_ADD_1_entry == AOP_OR ) {
               report_error(tok->line, SEMERR_12);
               t1 = TYPE_ERR;
          }
          else {
               t1 = TYPE_ERR;
          }
          n_sexprp_2_i = t1;
          n_sexprp_2_data = n_sexprp(n_sexprp_2_i);
          n_sexprp_2_t = n_sexprp_2_data[0];
          free(n_sexprp_2_data);
          n_sexprp_1_t = n_sexprp_2_t;
          break;
     case T_DO: ;
     case T_EL: ;
     case T_REL: ;
     case T_RPAR: ;
     case T_RBR: ;
     case T_END: ;
     case T_COM: ;
     case T_TH: ;
     case T_SC: ;

          //EPSILON
          n_sexprp_1_t = n_sexprp_1_i;
          break;
     default:
          report_error(tok->line, SYNERR_40);
          while(!synch(SYNC_n_sexprp, 9, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_sexprp_1_t;
     return values;
}
int SYNC_n_term[10] = {T_DO, T_EL, T_REL, T_RPAR, T_RBR, T_END, T_COM, T_ADD, T_SC, T_TH};
int* n_term() {
     int n_term_1_t;
     int n_termp_1_i, n_fac_1_t, n_termp_1_t;
     int *n_fac_1_data, *n_termp_1_data;
     switch(tok->tag) {
     case T_ID: ;
     case T_NUM: ;
     case T_LPAR: ;
     case T_NOT: ;

          n_fac_1_data = n_fac();
          n_fac_1_t = n_fac_1_data[0];
          free(n_fac_1_data);
          n_termp_1_i = n_fac_1_t;
          n_termp_1_data = n_termp(n_termp_1_i);
          n_termp_1_t = n_termp_1_data[0];
          free(n_termp_1_data);
          n_term_1_t = n_termp_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_41);
          while(!synch(SYNC_n_term, 10, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_term_1_t;
     return values;
}
int SYNC_n_termp[10] = {T_DO, T_EL, T_REL, T_RPAR, T_RBR, T_END, T_COM, T_ADD, T_SC, T_TH};
int* n_termp(int n_termp_1_i) {
     int n_termp_1_t;
     int T_MUL_1_entry, T_EP_1_entry, n_termp_2_i, n_fac_1_t, n_termp_2_t;
     int *n_fac_1_data, *n_termp_2_data;
     int t1;
     switch(tok->tag) {
     case T_MUL: ;

          T_MUL_1_entry = match(T_MUL);

          n_fac_1_data = n_fac();
          n_fac_1_t = n_fac_1_data[0];
          free(n_fac_1_data);
          if (n_fac_1_t == TYPE_INT && n_termp_1_i == TYPE_INT && T_MUL_1_entry == MOP_TIMES ) {
               t1 = TYPE_INT;
          }
          else if (n_fac_1_t == TYPE_REAL && n_termp_1_i == TYPE_REAL && T_MUL_1_entry == MOP_TIMES ) {
               t1 = TYPE_REAL;
          }
          else if (n_fac_1_t == TYPE_INT && n_termp_1_i == TYPE_INT && T_MUL_1_entry == MOP_IDIV ) {
               t1 = TYPE_INT;
          }
          else if (n_fac_1_t == TYPE_REAL && n_termp_1_i == TYPE_REAL && T_MUL_1_entry == MOP_DIV ) {
               t1 = TYPE_REAL;
          }
          else if (n_fac_1_t == TYPE_BOOLEAN && n_termp_1_i == TYPE_BOOLEAN && T_MUL_1_entry == MOP_AND ) {
               t1 = TYPE_BOOLEAN;
          }
          else if (n_fac_1_t == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else if (n_termp_1_i == TYPE_ERR ) {
               t1 = TYPE_ERR;
          }
          else if (T_MUL_1_entry == MOP_AND ) {
               report_error(tok->line, SEMERR_13);
               t1 = TYPE_ERR;
          }
          else if (T_MUL_1_entry == MOP_TIMES ) {
               report_error(tok->line, SEMERR_14);
               t1 = TYPE_ERR;
          }
          else if (T_MUL_1_entry == MOP_IDIV ) {
               report_error(tok->line, SEMERR_15);
               t1 = TYPE_ERR;
          }
          else if (T_MUL_1_entry == MOP_DIV ) {
               report_error(tok->line, SEMERR_16);
               t1 = TYPE_ERR;
          }
          else {
               t1 = TYPE_ERR;
          }
          n_termp_2_i = t1;
          n_termp_2_data = n_termp(n_termp_2_i);
          n_termp_2_t = n_termp_2_data[0];
          free(n_termp_2_data);
          n_termp_1_t = n_termp_2_t;
          break;
     case T_DO: ;
     case T_EL: ;
     case T_REL: ;
     case T_RPAR: ;
     case T_RBR: ;
     case T_END: ;
     case T_COM: ;
     case T_ADD: ;
     case T_SC: ;
     case T_TH: ;

          //EPSILON
          n_termp_1_t = n_termp_1_i;
          break;
     default:
          report_error(tok->line, SYNERR_42);
          while(!synch(SYNC_n_termp, 10, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_termp_1_t;
     return values;
}
int SYNC_n_fac[11] = {T_DO, T_EL, T_REL, T_MUL, T_RPAR, T_RBR, T_END, T_COM, T_ADD, T_SC, T_TH};
int* n_fac() {
     int n_fac_1_t;
     int T_NUM_1_entry, T_NOT_1_entry, T_LPAR_1_entry, T_RPAR_1_entry, T_ID_1_entry, n_fac_2_t, n_expr_1_t, n_facp_1_i, n_facp_1_t;
     int *n_fac_2_data, *n_expr_1_data, *n_facp_1_data;
     int t1, t2, t3;
     switch(tok->tag) {
     case T_NUM: ;

          T_NUM_1_entry = match(T_NUM);
          t1 = get_number_type(T_NUM_1_entry);
          n_fac_1_t = t1;
          break;
     case T_NOT: ;

          T_NOT_1_entry = match(T_NOT);

          n_fac_2_data = n_fac();
          n_fac_2_t = n_fac_2_data[0];
          free(n_fac_2_data);
          if (n_fac_2_t == TYPE_BOOLEAN ) {
               t2 = TYPE_BOOLEAN;
          }
          else if (n_fac_2_t == TYPE_ERR ) {
               t2 = TYPE_ERR;
          }
          else {
               report_error(tok->line, SEMERR_17);
               t2 = TYPE_ERR;
          }
          n_fac_1_t = t2;
          break;
     case T_LPAR: ;

          T_LPAR_1_entry = match(T_LPAR);

          n_expr_1_data = n_expr();
          n_expr_1_t = n_expr_1_data[0];
          free(n_expr_1_data);
          n_fac_1_t = n_expr_1_t;
          T_RPAR_1_entry = match(T_RPAR);

          break;
     case T_ID: ;

          T_ID_1_entry = match(T_ID);
          t3 = lookup(T_ID_1_entry);
          n_facp_1_i = t3;
          n_facp_1_data = n_facp(n_facp_1_i);
          n_facp_1_t = n_facp_1_data[0];
          free(n_facp_1_data);
          n_fac_1_t = n_facp_1_t;
          break;
     default:
          report_error(tok->line, SYNERR_43);
          while(!synch(SYNC_n_fac, 11, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_fac_1_t;
     return values;
}
int SYNC_n_facp[11] = {T_DO, T_EL, T_REL, T_MUL, T_RPAR, T_RBR, T_END, T_COM, T_ADD, T_SC, T_TH};
int* n_facp(int n_facp_1_i) {
     int n_facp_1_t;
     int T_EP_1_entry, T_LPAR_1_entry, T_RPAR_1_entry, T_LBR_1_entry, T_RBR_1_entry, n_exprl_1_i, n_exprl_1_t, n_expr_1_t;
     int *n_exprl_1_data, *n_expr_1_data;
     int t1, t2;
     switch(tok->tag) {
     case T_DO: ;
     case T_EL: ;
     case T_REL: ;
     case T_MUL: ;
     case T_RPAR: ;
     case T_RBR: ;
     case T_END: ;
     case T_COM: ;
     case T_ADD: ;
     case T_SC: ;
     case T_TH: ;

          //EPSILON
          n_facp_1_t = n_facp_1_i;
          break;
     case T_LPAR: ;

          T_LPAR_1_entry = match(T_LPAR);
          n_exprl_1_i = TYPE_ERR;
          n_exprl_1_data = n_exprl(n_exprl_1_i);
          n_exprl_1_t = n_exprl_1_data[0];
          free(n_exprl_1_data);

          T_RPAR_1_entry = match(T_RPAR);
          if (1 == 1 ) {
               t1 = TYPE_ERR;
          }
          else if (n_exprl_1_t == TYPE_VOID ) {
               t1 = n_facp_1_i;
          }
          else {
               t1 = TYPE_ERR;
          }
          n_facp_1_t = t1;
          break;
     case T_LBR: ;

          T_LBR_1_entry = match(T_LBR);

          n_expr_1_data = n_expr();
          n_expr_1_t = n_expr_1_data[0];
          free(n_expr_1_data);

          T_RBR_1_entry = match(T_RBR);
          if (n_facp_1_i == TYPE_AINT && n_expr_1_t == TYPE_INT ) {
               t2 = TYPE_INT;
          }
          else if (n_facp_1_i == TYPE_AREAL && n_expr_1_t == TYPE_INT ) {
               t2 = TYPE_REAL;
          }
          else if (n_facp_1_i != TYPE_AINT && n_facp_1_i != TYPE_AREAL ) {
               report_error(tok->line, SEMERR_19);
               t2 = TYPE_ERR;
          }
          else if (n_expr_1_t != TYPE_INT ) {
               report_error(tok->line, SEMERR_20);
               t2 = TYPE_ERR;
          }
          else {
               t2 = TYPE_ERR;
          }
          n_facp_1_t = t2;
          break;
     default:
          report_error(tok->line, SYNERR_44);
          while(!synch(SYNC_n_facp, 11, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 1);
     values[0] = n_facp_1_t;
     return values;
}
int SYNC_n_sign[4] = {T_ID, T_NUM, T_LPAR, T_NOT};
int* n_sign() {
     int T_PLS_1_entry, T_MIN_1_entry;
     switch(tok->tag) {
     case T_PLS: ;

          T_PLS_1_entry = match(T_PLS);

          break;
     case T_MIN: ;

          T_MIN_1_entry = match(T_MIN);

          break;
     default:
          report_error(tok->line, SYNERR_45);
          while(!synch(SYNC_n_sign, 4, tok->tag))
               tok = get_token();
     }
     int *values = (int*)malloc(sizeof(int) * 0);
     return values;
}
