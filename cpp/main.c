#include "global.h"
#include "lex.h"
#include "listing.h"
#include "parse.h"
#include "rsword.h"
#include "symbol.h"

int main(int argc, const char* argv[])
{
    PROG_NAME = "cor3";
    printf("Reading source file...\n");
    read_source_file();
    printf("Lexical analysis...\n");
    lex();
    //print_sym_table();
    printf("Parsing...\n");
    parse();
    printf("Finished.\n");
    write_listing_file();
    write_allocation_table();
    return 0;
}

void print_sym_table() {
    struct CLList* i = sym_table;
    while(i) {
        struct Symbol* s = (struct Symbol*)i->p;
        printf("%s\n", s->name);
        i = i->next;
    }
}
