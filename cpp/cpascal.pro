TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_CFLAGS += -std=c99

SOURCES += main.c \
    tokens.c \
    lex.c \
    rsword.c \
    machines.c \
    cstring.c \
    cllist.c \
    error.c \
    listing.c \
    parse.c \
    symbol.c \
    stack.c \
    nonterms.c \
    generated_errors.c

HEADERS += \
    machines.h \
    tokens.h \
    lex.h \
    error.h \
    listing.h \
    rsword.h \
    cllist.h \
    cstring.h \
    global.h \
    parse.h \
    carray.h \
    symbol.h \
    type.h \
    stack.h \
    nonterms.h \
    generated_errors.h

OTHER_FILES += \
    rswords.txt \
    ../build-cpascal-Desktop-Debug/source1_tokens.txt \
    ../build-cpascal-Desktop-Debug/source1.pas \
    ../build-cpascal-Desktop-Debug/source1_listing.txt \
    ../build-cpascal-Desktop-Debug/source2.pas \
    ../build-cpascal-Desktop-Debug/source3.pas \
    ../build-cpascal-Desktop-Debug/source3_tokens.txt \
    ../build-cpascal-Desktop-Debug/source3_listing.txt

DISTFILES += \
    ../build-cpascal-Desktop_Qt_5_4_0_MinGW_32bit-Debug/rswords.txt \
    ../build-cpascal-Desktop_Qt_5_4_0_MinGW_32bit-Debug/source1.pas \
    ../build-cpascal-Desktop_Qt_5_4_0_MinGW_32bit-Debug/source1_listing.txt \
    ../build-cpascal-Desktop_Qt_5_4_0_MinGW_32bit-Debug/source1_tokens.txt \
    ../build-cpascal-Desktop_Qt_5_4_0_MinGW_32bit-Debug/source2.pas \
    ../build-cpascal-Desktop_Qt_5_4_0_MinGW_32bit-Debug/source3.pas \
    ../build-cpascal-Desktop_Qt_5_4_0_MinGW_32bit-Debug/source3_listing.txt \
    ../build-cpascal-Desktop_Qt_5_4_0_MinGW_32bit-Debug/source3_tokens.txt \
    ../build-cpascal-Desktop-Debug/rswords.txt \
    source1.pas \
    source1_listing.txt \
    source1_tokens.txt \
    source2.pas \
    source3.pas \
    source3_listing.txt \
    source3_tokens.txt \
    source3_alloc_table.txt \
    source4.pas \
    source4_alloc_table.txt \
    source4_listing.txt \
    source4_tokens.txt \
    cor3.pas \
    cor3_alloc_table.txt \
    cor3_listing.txt \
    cor3_tokens.txt

