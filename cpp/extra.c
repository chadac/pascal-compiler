void n_program();
void n_program1();
void n_program2();

void n_identlst();
void n_identlst1();

void n_decls();
void n_decls1();

void n_type();

void n_stdtype();

void n_subpdecls();
void n_subpdecls1();

void n_subpdecl();
void n_subpdecl1();
void n_subpdecl2();

void n_subphead();
void n_subphead1();
void n_subphead2();

void n_args();

void n_paramlst();
void n_paramlst1();

void n_cmpdstmt();
void n_cmpdstmt1();

void n_ostmts();

void n_stmtlst();
void n_stmtlst1();

void n_stmt();
void n_stmt1();

void n_var();
void n_var1();

void n_procstmt();
void n_procstmt1();

void n_exprlst();
void n_exprlst1();

void n_expr();
void n_expr1();

void n_simpexpr();
void n_simpexpr1();

void n_term();
void n_term1();

void n_factor();
void n_factor1();

void n_sign();

int SYNCH_NPROG[1] = {T_EOF};
void n_program() {
    switch(tok->tag) {
    case T_PROG:
        match(T_PROG);
        match(T_ID);
        match(T_LPAR);
        n_identlst();
        match(T_RPAR);
        match(T_SC);
        n_program1();
        break;
    default:
        report_error(tok->line, SYNERR_PROG);
        while(!synch(SYNCH_NPROG, 1, tok->tag))
            tok = get_token();
    }
}

int SYNCH_NRPOG1[1] = {T_EOF};
void n_program1() {
    switch(tok->tag) {
    case T_PROC:
        n_subpdecls();
        n_cmpdstmt();
        match(T_DOT);
        break;
    case T_BEG:
        n_cmpdstmt();
        match(T_DOT);
        break;
    case T_VAR:
        n_decls();
        n_program2();
        break;
    default:
        report_error(tok->line, SYNERR_PROG1);
        while(!synch(SYNCH_NRPOG1, 1, tok->tag))
            tok = get_token();
    }
}

int SYNCH_NRPOG2[1] = {T_EOF};
void n_program2() {
    switch(tok->tag) {
    case T_PROC:
        n_subpdecls();
        n_cmpdstmt();
        match(T_DOT);
        break;
    case T_BEG:
        n_cmpdstmt();
        match(T_DOT);
        break;
    default:
        report_error(tok->line, SYNERR_PROG2);
        while(!synch(SYNCH_NRPOG2, 1, tok->tag))
            tok = get_token();
    }
}

int SYNCH_IDENTLST[1] = {T_RPAR};
void n_identlst() {
    switch(tok->tag) {
    case T_ID:
        match(T_ID);
        n_identlst1();
        break;
    default:
        report_error(tok->line, SYNERR_INDENTLIST);
        while(!synch(SYNCH_IDENTLST, 1, tok->tag))
            tok = get_token();
    }
}

int SYNCH_IDENTLST1[1] = {T_RPAR};
void n_identlst1() {
    switch(tok->tag) {
    case T_COM:
        match(T_COM);
        match(T_ID);
        n_identlst1();
        break;
    case T_RPAR:
        break;
    default:
        report_error(tok->line, SYNERR_INDENTLIST1);
        while(!synch(SYNCH_IDENTLST1, 1, tok->tag))
            tok = get_token();
    }
}

int SYNCH_DECLS[2] = {T_PROC, T_BEG};
void n_decls() {
    switch(tok->tag) {
    case T_VAR:
    case T_PROC:
    case T_BEG:
        n_decls1();
        break;
    default:
        report_error(tok->line, SYNERR_DECLS);
        while(!synch(SYNCH_DECLS, 2, tok->tag))
            tok = get_token();
    }
}

int SYNCH_NDECLS1[2] = {T_PROC, T_BEG};
void n_decls1() {
    switch(tok->tag) {
    case T_VAR:
        match(T_VAR);
        match(T_ID);
        match(T_C);
        n_type();
        match(T_SC);
        n_decls1();
        break;
    case T_PROC:
    case T_BEG:
        break;
    default:
        report_error(tok->line, SYNERR_DECLS1);
        while(!synch(SYNCH_NDECLS1, 2, tok->tag))
            tok = get_token();
    }
}

int SYNCH_TYPE[2] = {T_RPAR, T_SC};
void n_type() {
    switch(tok->tag) {
    case T_REAL:
    case T_INT:
        n_stdtype();
        break;
    case T_ARR:
        match(T_ARR);
        match(T_LBR);
        match(T_NUM);
        match(T_DD);
        match(T_NUM);
        match(T_RBR);
        match(T_OF);
        n_stdtype();
        break;
    default:
        report_error(tok->line, SYNERR_TYPE);
        while(!synch(SYNCH_TYPE, 2, tok->tag))
            tok = get_token();
    }
}

int SYNCH_STDTYPE[2] = {T_RPAR,T_SC};
void n_stdtype() {
    switch(tok->tag) {
    case T_INT:
        match(T_INT);
        break;
    case T_REAL:
        match(T_REAL);
        break;
    default:
        report_error(tok->line, SYNERR_STDTYPE);
        while(!synch(SYNCH_STDTYPE, 2, tok->tag))
            tok = get_token();
    }
}

int SYNCH_SUBPDECLS[1] = {T_BEG};
void n_subpdecls() {
    switch(tok->tag) {
    case T_PROC:
    case T_BEG:
        n_subpdecls1();
        break;
    default:
        report_error(tok->line, SYNERR_SUBPDECLS);
        while(!synch(SYNCH_SUBPDECLS, 1, tok->tag))
            tok = get_token();
    }
}

int SYNCH_SUBPDECLS1[1] = {T_BEG};
void n_subpdecls1() {
    switch(tok->tag) {
    case T_PROC:
        n_subpdecl();
        match(T_SC);
        n_subpdecls1();
        break;
    case T_BEG:
        break;
    default:
        report_error(tok->line, SYNERR_SUBPDECLS1);
        while(!synch(SYNCH_SUBPDECLS1, 1, tok->tag))
            tok = get_token();
    }
}

int SYNCH_SUBPDECL[1] = {T_SC};
void n_subpdecl() {
    switch(tok->tag) {
    case T_PROC:
        n_subphead();
        n_subpdecl1();
        break;
    default:
        report_error(tok->line, SYNERR_SUBPDECL);
        while(!synch(SYNCH_SUBPDECL, 1, tok->tag))
            tok = get_token();
    }
}

int SYNCH_SUBPDECL1[1] = {T_SC};
void n_subpdecl1() {
    switch(tok->tag) {
    case T_PROC:
        n_subpdecls();
        n_cmpdstmt();
        break;
    case T_BEG:
        n_cmpdstmt();
        break;
    case T_VAR:
        n_decls();
        n_subpdecl2();
        break;
    default:
        report_error(tok->line, SYNERR_SUBPDECL1);
        while(!synch(SYNCH_SUBPDECL1, 1, tok->tag))
            tok = get_token();
    }
}

int SYNCH_SUBPDECL2[1] = {T_SC};
void n_subpdecl2() {
    switch(tok->tag) {
    case T_PROC:
        n_subpdecls();
        n_cmpdstmt();
        break;
    case T_BEG:
        n_cmpdstmt();
        break;
    default:
        report_error(tok->line, SYNERR_SUBPDECL2);
        while(!synch(SYNCH_SUBPDECL2, 1, tok->tag))
            tok = get_token();
    }
}

int SYNCH_SUBPHEAD[3] = {T_PROC, T_BEG, T_VAR};
void n_subphead() {
    switch(tok->tag) {
    case T_PROC:
        match(T_PROC);
        match(T_ID);
        n_subphead1();
        break;
    default:
        report_error(tok->line, SYNERR_SUBPHEAD);
        while(!synch(SYNCH_SUBPHEAD, 3, tok->tag))
            tok = get_token();
    }
}

int SYNCH_SUBPHEAD1[3] = {T_PROC, T_BEG, T_VAR};
void n_subphead1() {
    switch(tok->tag) {
    case T_LPAR:
        n_args();
        match(T_SC);
        break;
    case T_SC:
        match(T_SC);
        break;
    default:
        report_error(tok->line, SYNERR_SUBPHEAD1);
        while(!synch(SYNCH_SUBPHEAD1, 3, tok->tag))
            tok = get_token();
    }
}

int SYNCH_ARGS[1] = {T_SC};
void n_args() {
    switch(tok->tag) {
    case T_LPAR:
        match(T_LPAR);
        n_paramlst();
        match(T_RPAR);
        break;
    default:
        report_error(tok->line, SYNERR_ARGS);
        while(!synch(SYNCH_ARGS, 1, tok->tag))
            tok = get_token();
    }
}

int SYNCH_PARAMLST[1] = {T_RPAR};
void n_paramlst() {
    switch(tok->tag) {
    case T_ID:
        match(T_ID);
        match(T_C);
        n_type();
        n_paramlst1();
        break;
    default:
        report_error(tok->line, SYNERR_PARAMLIST);
        while(!synch(SYNCH_PARAMLST, 1, tok->tag))
            tok = get_token();
    }
}

int SYNCH_PARAMLST1[1] = {T_RPAR};
void n_paramlst1() {
    switch(tok->tag) {
    case T_SC:
        match(T_SC);
        match(T_ID);
        match(T_C);
        n_type();
        n_paramlst1();
        break;
    case T_RPAR:
        break;
    default:
        report_error(tok->line, SYNERR_PARAMLIST1);
        while(!synch(SYNCH_PARAMLST1, 1, tok->tag))
            tok = get_token();
    }
}

int SYNCH_CMPDSTMT[4] = {T_DOT, T_EL, T_SC, T_END};
void n_cmpdstmt() {
    switch(tok->tag) {
    case T_BEG:
        match(T_BEG);
        n_cmpdstmt1();
        break;
    default:
        report_error(tok->line, SYNERR_CMPDSTMT);
        while(!synch(SYNCH_CMPDSTMT, 4, tok->tag))
            tok = get_token();
    }
}

int SYNCH_CMPDSTMT1[4] = {T_DOT, T_EL, T_SC, T_END};
void n_cmpdstmt1() {
    switch(tok->tag) {
    case T_CALL:
    case T_BEG:
    case T_ID:
    case T_WH:
    case T_IF:
        n_ostmts();
        match(T_END);
        break;
    case T_END:
        match(T_END);
        break;
    default:
        report_error(tok->line, SYNERR_CMPDSTMT1);
        while(!synch(SYNCH_CMPDSTMT1, 4, tok->tag))
            tok = get_token();
    }
}

int SYNCH_OSTMTS[1] = {T_END};
void n_ostmts() {
    switch(tok->tag) {
    case T_CALL:
    case T_BEG:
    case T_ID:
    case T_WH:
    case T_IF:
        n_stmtlst();
        break;
    default:
        report_error(tok->line, SYNERR_OSTMTS);
        while(!synch(SYNCH_OSTMTS, 1, tok->tag))
            tok = get_token();
    }
}

int SYNCH_STMTLST[1] = {T_END};
void n_stmtlst() {
    switch(tok->tag) {
    case T_CALL:
    case T_BEG:
    case T_ID:
    case T_WH:
    case T_IF:
        n_stmt();
        n_stmtlst1();
        break;
    default:
        report_error(tok->line, SYNERR_STMTLST);
        while(!synch(SYNCH_STMTLST, 1, tok->tag))
            tok = get_token();
    }
}

int SYNCH_STMTLST1[1] = {T_END};
void n_stmtlst1() {
    switch(tok->tag) {
    case T_SC:
        match(T_SC);
        n_stmt();
        n_stmtlst1();
        break;
    case T_END:
        break;
    default:
        report_error(tok->line, SYNERR_STMTLST1);
        while(!synch(SYNCH_STMTLST1, 1, tok->tag))
            tok = get_token();
    }
}

int SYNCH_STMT[3] = {T_EL, T_SC, T_END};
void n_stmt() {
    switch(tok->tag) {
    case T_ID:
        n_var();
        match(T_ASS);
        n_expr();
        break;
    case T_CALL:
        n_procstmt();
        break;
    case T_BEG:
        n_cmpdstmt();
        break;
    case T_WH:
        match(T_WH);
        n_expr();
        match(T_DO);
        n_stmt();
        break;
    case T_IF:
        match(T_IF);
        n_expr();
        match(T_TH);
        n_stmt();
        n_stmt1();
        break;
    default:
        report_error(tok->line, SYNERR_STMT);
        while(!synch(SYNCH_STMT, 3, tok->tag))
            tok = get_token();
    }
}

int SYNCH_STMT1[3] = {T_SC, T_EL, T_END};
void n_stmt1() {
    switch(tok->tag) {
    case T_SC:
    case T_END:
        break;
    case T_EL:
        match(T_EL);
        n_stmt();
        break;
    default:
        report_error(tok->line, SYNERR_STMT1);
        while(!synch(SYNCH_STMT1, 3, tok->tag))
            tok = get_token();
    }
}

int SYNCH_VAR[1] = {T_ASS};
void n_var() {
    switch(tok->tag) {
    case T_ID:
        match(T_ID);
        n_var1();
        break;
    default:
        report_error(tok->line, SYNERR_VAR);
        while(!synch(SYNCH_VAR, 1, tok->tag))
            tok = get_token();
    }
}

int SYNCH_VAR1[1] = {T_ASS};
void n_var1() {
    switch(tok->tag) {
    case T_ASS:
        break;
    case T_LBR:
        match(T_LBR);
        n_expr();
        match(T_RBR);
        break;
    default:
        report_error(tok->line, SYNERR_VAR1);
        while(!synch(SYNCH_VAR1, 1, tok->tag))
            tok = get_token();
    }
}

int SYNCH_PROCSTMT[3] = {T_SC, T_EL, T_END};
void n_procstmt() {
    switch(tok->tag) {
    case T_CALL:
        match(T_CALL);
        match(T_ID);
        n_procstmt1();
        break;
    default:
        report_error(tok->line, SYNERR_PROCSTMT);
        while(!synch(SYNCH_PROCSTMT, 3, tok->tag))
            tok = get_token();
    }
}

int SYNCH_PROCSTMT1[3] = {T_SC, T_EL, T_END};
void n_procstmt1() {
    switch(tok->tag) {
    case T_EL:
    case T_SC:
    case T_END:
        break;
    case T_LPAR:
        match(T_LPAR);
        n_exprlst();
        match(T_RPAR);
    default:
        report_error(tok->line, SYNERR_PROCSTMT1);
        while(!synch(SYNCH_PROCSTMT1, 3, tok->tag))
            tok = get_token();
    }
}

int SYNCH_EXPRLST[1] = {T_RPAR};
void n_exprlst() {
    switch(tok->tag) {
    case T_NOT:
    case T_NUM:
    case T_ID:
    case T_PLS:
    case T_MIN:
        n_expr();
        n_exprlst1();
        break;
    default:
        report_error(tok->line, SYNERR_EXPRLIST);
        while(!synch(SYNCH_EXPRLST, 1, tok->tag))
            tok = get_token();
    }
}

int SYNCH_EXPRLST1[1] = {T_RPAR};
void n_exprlst1() {
    switch(tok->tag) {
    case T_COM:
        match(T_COM);
        n_expr();
        n_exprlst1();
        break;
    case T_RPAR:
        break;
    default:
        report_error(tok->line, SYNERR_EXPRLIST1);
        while(!synch(SYNCH_EXPRLST1, 1, tok->tag))
            tok = get_token();
    }
}

int SYNCH_EXPR[8] = {T_TH, T_EL, T_DO, T_RPAR, T_SC, T_END, T_RBR, T_COM};
void n_expr() {
    switch(tok->tag) {
    case T_NOT:
    case T_NUM:
    case T_ID:
    case T_PLS:
    case T_MIN:
        n_simpexpr();
        n_expr1();
        break;
    default:
        report_error(tok->line, SYNERR_EXPR);
        while(!synch(SYNCH_EXPR, 8, tok->tag))
            tok = get_token();
    }
}

int SYNCH_EXPR1[8] = {T_TH, T_EL, T_DO, T_RPAR, T_SC, T_END, T_RBR, T_COM};
void n_expr1() {
    switch(tok->tag) {
    case T_TH:
    case T_EL:
    case T_DO:
    case T_RPAR:
    case T_SC:
    case T_END:
    case T_RBR:
    case T_COM:
        break;
    case T_REL:
        match(T_REL);
        n_simpexpr();
        break;
    default:
        report_error(tok->line, SYNERR_EXPR1);
        while(!synch(SYNCH_EXPR1, 8, tok->tag))
            tok = get_token();
    }
}

int SYNCH_SIMPEXPR[9] = {T_TH, T_EL, T_DO, T_RPAR, T_SC, T_END, T_RBR, T_COM, T_REL};
void n_simpexpr() {
    switch(tok->tag) {
    case T_NOT:
    case T_ID:
    case T_NUM:
        n_term();
        n_simpexpr1();
        break;
    case T_PLS:
    case T_MIN:
        n_sign();
        n_term();
        n_simpexpr1();
        break;
    default:
        report_error(tok->line, SYNERR_SEXPR);
        while(!synch(SYNCH_SIMPEXPR, 9, tok->tag))
            tok = get_token();
    }
}

int SYNCH_SIMPEXPR1[9] = {T_TH, T_EL, T_DO, T_RPAR, T_SC, T_END, T_RBR, T_COM, T_REL};
void n_simpexpr1() {
    switch(tok->tag) {
    case T_ADD:
        match(T_ADD);
        n_term();
        n_simpexpr1();
    case T_TH:
    case T_EL:
    case T_DO:
    case T_RPAR:
    case T_SC:
    case T_END:
    case T_RBR:
    case T_REL:
    case T_COM:
        break;
    default:
        report_error(tok->line, SYNERR_SEXPR1);
        while(!synch(SYNCH_SIMPEXPR1, 9, tok->tag))
            tok = get_token();
    }
}

int SYNCH_TERM[10] = {T_TH, T_EL, T_DO, T_RPAR, T_SC, T_END, T_RBR, T_COM, T_REL, T_ADD};
void n_term() {
    switch(tok->tag) {
    case T_NOT:
    case T_ID:
    case T_NUM:
        n_factor();
        n_term1();
        break;
    default:
        report_error(tok->line, SYNERR_TERM);
        while(!synch(SYNCH_TERM, 10, tok->tag))
            tok = get_token();
    }
}

int SYNCH_TERM1[10] = {T_TH, T_EL, T_DO, T_RPAR, T_SC, T_END, T_RBR, T_COM, T_REL, T_ADD};
void n_term1() {
    switch(tok->tag) {
    case T_MUL:
        match(T_MUL);
        n_factor();
        n_term1();
        break;
    case T_TH:
    case T_ADD:
    case T_EL:
    case T_DO:
    case T_RPAR:
    case T_SC:
    case T_END:
    case T_RBR:
    case T_REL:
    case T_COM:
        break;
    default:
        report_error(tok->line, SYNERR_TERM1);
        while(!synch(SYNCH_TERM1, 10, tok->tag))
            tok = get_token();
    }
}

int SYNCH_FACTOR[11] = {T_TH, T_EL, T_DO, T_RPAR, T_SC, T_END, T_RBR, T_COM, T_REL, T_ADD, T_MUL};
void n_factor() {
    switch(tok->tag) {
    case T_NUM:
        match(T_NUM);
        break;
    case T_NOT:
        match(T_NOT);
        n_factor();
        break;
    case T_ID:
        match(T_ID);
        n_factor1();
        break;
    default:
        report_error(tok->line, SYNERR_FACTOR);
        while(!synch(SYNCH_FACTOR, 11, tok->tag))
            tok = get_token();
    }
}

int SYNCH_FACTOR1[11] = {T_TH, T_EL, T_DO, T_RPAR, T_SC, T_END, T_RBR, T_COM, T_REL, T_ADD, T_MUL};
void n_factor1() {
    switch(tok->tag) {
    case T_TH:
    case T_ADD:
    case T_EL:
    case T_DO:
    case T_RPAR:
    case T_SC:
    case T_END:
    case T_MUL:
    case T_RBR:
    case T_REL:
    case T_COM:
        break;
    case T_LPAR:
        match(T_LPAR);
        n_exprlst();
        match(T_RPAR);
        break;
    case T_LBR:
        match(T_LBR);
        n_expr();
        match(T_RBR);
        break;
    default:
        report_error(tok->line, SYNERR_FACTOR1);
        while(!synch(SYNCH_FACTOR1, 11, tok->tag))
            tok = get_token();
    }
}

int SYNCH_SIGN[3] = {T_NOT, T_ID, T_NUM};
void n_sign() {
    switch(tok->tag) {
    case T_PLS:
        match(T_PLS);
    case T_MIN:
        match(T_MIN);
    default:
        report_error(tok->line, SYNERR_SIGN);
        while(!synch(SYNCH_SIGN, 3, tok->tag))
            tok = get_token();
    }
}
