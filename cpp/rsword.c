#include "rsword.h"
#include "cllist.h"

struct CLList *rstable;
const char* RSTABLE_FILE = "rswords.txt";

struct RsWord *rsword_f(const char* word, int tag_id, int attr) {
    struct RsWord *o = (struct RsWord*)malloc(sizeof(struct RsWord));
    o->word = word;
    o->tag_id = tag_id;
    o->attr = attr;
    return o;
}

void rstable_i() {
    rstable = 0;
    _rstable_fopen(RSTABLE_FILE);
}

struct RsWord *rstable_find_word(char* fword) {
    struct CLList *current = rstable;
    do {
        char* cword = ((struct RsWord*)current->p)->word;
        //printf("%s | %s\n", fword, current->word);
        if (strlen(cword) == strlen(fword) && strcmp(cword,fword) == 0)
            return (struct RsWord*)current->p;
        current = current->next;
    } while (current);
    return 0;
}

void rstable_print() {
    struct CLList *crw = rstable;
    //printf("%p\n", crw);
    while (crw) {
        struct RsWord *w = (struct RsWord*)crw->p;
        //printf("%p\n", crw->p);
        printf("%s %d %d\n", w->word, w->tag_id, w->attr);
        crw = crw->next;
    }
    struct RsWord *w = (struct RsWord*)crw->p;
    printf("%s %d %d\n", w->word, w->tag_id, w->attr);
}

void _rstable_add_item(unsigned char *word, int tag_id, int attr) {
    struct CLList **crw = &rstable;
    struct RsWord *w = rsword_f(word,tag_id,attr);
    //printf("stored: %s %d %d\n", w->word, w->tag_id, w->attr);
    cllist_append(crw, (int)w);
//    if (crw->word) {
//        while (crw->next) crw = crw->next;
//        crw->next = (struct RsWord*) calloc(sizeof(struct RsWord),4);
//        crw = crw->next;
//    }
//    crw->word = word;
//    crw->tag_id = tag_id;
//    crw->attr = attr;
//    return true;
}

void _rstable_fopen(const char* fname) {
    struct RsWord *crw = rstable;
    FILE *fp;
    fp = fopen(fname, "r");
    unsigned char *word;
    int tag_id = 0;
    int attr = 0;
    do {
        word = (char*)malloc(11 * sizeof(char));
        fscanf(fp, "%s %d %d", word, &tag_id, &attr);
        if(feof(fp)) break;
        printf("%s %d %d\n", word, tag_id, attr);
        _rstable_add_item(word, tag_id, attr);
    } while (!feof(fp));
    fclose(fp);
}

int rstable_test() {
    printf("--------------------\n");
    printf("Initialize table\n");
    rstable_i();

    printf("--------------------\n");
    printf("Populate table");
    _rstable_fopen("reserved_word_table.txt");

    printf("--------------------\n");
    printf("Resulting table\n");
    rstable_print();

    printf("--------------------\n");
    printf("Find rsword \"real\"\n");
    struct RsWord *rw = rstable_find_word("real");
    printf("%s %d %d\n\n", rw->word, rw->tag_id, rw->attr);

    printf("--------------------\n");
    printf("Find rsword \"rea1l\"\n");
    rw = rstable_find_word("rea1l");
    printf("%p\n\n", rw);

    return 0;
}

/*int main(int argc, char *argv[]) {
    //printf("%d\n", sizeof(struct RsWord));
    rstable_test();

    char in[10];
    fgets(in, 10, stdin);
    return 0;
}*/
