#ifndef TOKENS_H
#define TOKENS_H

#include "type.h"

/* Token IDs */
#define T_EOF 0
#define T_EMPTY 1
#define T_DOT 2
#define T_VAR 3
#define T_CALL 4
#define T_OF 5
#define T_BEG 10
#define T_END 11
#define T_PROG 12
#define T_FUNC 13
#define T_PROC 14
#define T_IF 15
#define T_TH 16
#define T_EL 17
#define T_WH 18
#define T_DO 19
#define T_ARR 20
#define T_INT 21
#define T_REAL 22
#define T_NUM 23
#define T_STRING 24
#define T_NOT 25
#define T_REL 30
#define T_MUL 31
#define T_ADD 32
#define T_ASS 33
#define T_COM 40
#define T_SC 41
#define T_LPAR 42
#define T_RPAR 43
#define T_LBR 44
#define T_RBR 45
#define T_PLS 46
#define T_MIN 47
#define T_C 48
#define T_DD 49
#define T_ID 50
#define T_LEXERR 99

/* Token Attribute Values*/

/* Relops */
#define ROP_GT  1
#define ROP_GE  2
#define ROP_EQ  3
#define ROP_NEQ 4
#define ROP_LT  5
#define ROP_LE  6

/* Addops */
#define AOP_PLUS 1
#define AOP_MINUS 2
#define AOP_OR 3

/* Mulops */
#define MOP_TIMES 1
#define MOP_DIV 2
#define MOP_IDIV 3
#define MOP_MOD 4
#define MOP_AND 5

struct Token {
    unsigned int line;
    char* lexeme;
    unsigned int tag;
    unsigned int attr;
};

struct Number {
    int type;
    int a;
    int b;
    int c;
};

struct Token *token_f(int line, char* lexeme, int tag, int attr);
struct Number *number_f(int type, int a, int b, int c);

#endif // TOKENS_H
