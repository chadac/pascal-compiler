#ifndef CSTRING_H
#define CSTRING_H

char *strcpyn(char* Source, int Length);
char *cstrcpy(const char* source);
char *cstrcat(char* s1, char* s2);

#endif // CSTRING_H
