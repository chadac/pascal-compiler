#include "parse.h"
#include "tokens.h"
#include "cllist.h"
#include "error.h"
#include "lex.h"
#include "nonterms.h"
#include "symbol.h"
#include "stack.h"

struct CLList* tok_ptr = 0;

struct Token* get_token() {
    if(!tok) parse_line = 0;
    else parse_line = tok->line;
    if(!tok_ptr->next) return (struct Token*)tok_ptr->p;
    tok_ptr = tok_ptr->next;
    struct Token* t = (struct Token*)tok_ptr->p;
    //printf("%s\n",t->lexeme);
    return t;
}

void parse() {
    tok_ptr = tokens;
    parse_line = 0;
    tok = (struct Token*)tok_ptr->p;
    printf("%s\n",tok->lexeme);
    //n_program();
    n_prog(0);
    match(T_EOF);
}

int match(unsigned int T) {
    printf("%15s %d : %d\n",tok->lexeme, T,tok->tag);
    int a = tok->attr;
    if(T == tok->tag && T != T_EOF) {
        tok = get_token();
    }
    else if(T == tok->tag && T == T_EOF) {
        //exit(0);
    }
    else {
        report_error(tok->line, SYNERR_UNREC);
        tok = get_token();
        return 0;
    }
    return a;
}

bool synch(int A[], int N, int T) {
    if(T == T_EOF) return true;
    for(int i = 0; i < N; i++)
        if(A[i] == T) return true;
    return false;
}

int get_number_a(int p) {
    struct Number *n = (struct Number*)p;
    return n->a;
}

int check_add_blue_node(int entry) {
    char* name = ((struct Symbol*)entry)->name;
    struct StackItem *s = check_stack(name);
    if(s) {
        report_error(parse_line, SEMERR_PROCINSCOPE);
        return 0;
    }
    struct StackItem *parent = get_blue_node();
    struct StackItem *n = push_blue_node(name);
    if(parent) {
        cllist_append(&parent->subprocs, (int)n);
    }
    return n;
}

int check_add_green_node(int entry, int type, int offset, int width) {
    char* name = ((struct Symbol*)entry)->name;
    struct StackItem *s = check_stack_scope(name);
    struct StackItem *b = get_blue_node();
    if(s) {
        report_error(parse_line, SEMERR_VARINSCOPE);
        return TYPE_ERR;
    }
    cllist_append(&allocated, (int)alloc_f((struct Symbol*)entry, type, offset, width, b->name));
    push_green_node(name, type);
    return type;
}

int add_subproc(int entry, int parent) {
    if(!parent) return 0;
    struct StackItem *subproc = (struct StackItem*)entry;
    //struct StackItem *proc = (struct StackItem*)parent;
    //cllist_append(&proc->subprocs, (int)subproc);
    return 1;
}

int find_in_scope(int entry, int parent) {
    if(!entry) return 0;
    struct Symbol *e = (struct Symbol*)entry;
    struct StackItem *b = (struct StackItem*)parent;//get_blue_node();
    struct CLList *i = b->subprocs;
    if(strcmp(e->name, b->name) == 0) return (int)b;
    while(i) {
        b = (struct StackItem*)i->p;
        if(strcmp(e->name, b->name) == 0) return (int)b;
        if(b = find_in_scope(entry, (int)b)) return (int)b;
        i = i->next;
    }
    return 0;
}

int in_scope(int entry) {
    return find_in_scope(entry, (int)get_blue_node());
}

struct Alloc *alloc_f(struct Symbol* s, int type, int offset, int width, char* proc) {
    struct Alloc *a = (struct Alloc*)malloc(sizeof(struct Alloc));
    a->s = s;
    a->type = type;
    a->offset = offset;
    a->width = width;
    a->proc = proc;
    return a;
}

void write_allocation_table() {
    struct CLList *c = allocated;
    const char* fname = cstrcat(PROG_NAME, "_alloc_table.txt");
    FILE *f = fopen(fname, "w");
    fprintf(f, "%10s %10s %4s %6s %6s\n", "Scope", "Symbol", "Type", "Offset", "Width");
    while(c) {
        struct Alloc *a = (struct Alloc*)c->p;
        fprintf(f, "%10s %10s %4d %6d %6d\n", a->proc, a->s->name, a->type, a->offset, a->width);
        c = c->next;
    }
    fclose(f);
    return;
}


int lookup(int *p) {
    if(!p) return TYPE_ERR;
    struct Symbol *s = (struct Symbol*)p;
    struct StackItem *si = check_stack(s->name);
    if(!si) {
        report_error(parse_line, SEMERR_NOTINSCOPE);
        return TYPE_ERR;
    }
    else {
        return si->type;
    }
}

int get_number_type(int *entry) {
    return *entry;
}

int push_params(int entry) {
    free(param_list);
    param_list = 0;
    //struct Symbol *s = (struct Symbol*)entry;
    struct StackItem *si = (struct StackItem*)entry;
    if(!si) {
        report_error(parse_line, SEMERR_PROCNOTINSCOPE);
        return TYPE_ERR;
    }
    struct CLList *param = si->params;
    while(param) {
        //struct StackItem* p = (struct StackItem*)param->p;
        cllist_push(&param_list, param->p);
        param = param->next;
    }
    return TYPE_VOID;
}

int pop_param(int err) {
    if(err == TYPE_ERR) return TYPE_ERR;
    if(!param_list) {
        report_error(parse_line, SEMERR_TOOMANYPARAMS);
        return TYPE_ERR;
    }
    struct StackItem* s = (struct StackItem*)cllist_pop(&param_list);
    return s->type;
}

int pop_params(int err) {
    if(err == TYPE_ERR) return TYPE_ERR;
    if(param_list) {
        report_error(parse_line, SEMERR_NOTENOUGHPARAMS);
        free(param_list);
        param_list = 0;
        return TYPE_ERR;
    }
    free(param_list);
    param_list = 0;
    return TYPE_VOID;
}
