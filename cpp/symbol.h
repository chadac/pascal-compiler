#ifndef SYMBOL_H
#define SYMBOL_H

#include "global.h"
#include "type.h"
#include "cllist.h"
#include "cstring.h"

struct Symbol {
    char* name;
};

struct Symbol* sym_f(char* name);

//symbol table operations
struct CLList* sym_table;

struct Symbol* get_symbol(const char* name);
struct Symbol* add_symbol(const char *name);

#endif // SYMBOL_H

