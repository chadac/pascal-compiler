#include "listing.h"
#include "global.h"
#include "error.h"

struct CLList* source = 0;

struct CLList* read_source_file() {
    if(source) return source;
    char* sourcefile = cstrcat(PROG_NAME,".pas");
    FILE *fs = fopen(sourcefile, "r");
    if(!fs) {
        printf("ERROR: No source file found for file name %s",sourcefile);
        exit(1);
    }
    do {
        char* line = malloc(73);
        fgets(line, 72, fs);
        if(feof(fs)) break;
        cllist_append(&source, line);
    } while (!feof(fs));
    fclose(fs);
    return source;
}

void write_listing_file() {
    printf("writing listing file...\n\n");
    const char* listfile = cstrcat(PROG_NAME, "_listing.txt");
    FILE *fl = fopen(listfile, "w");
    struct CLList* cline = source;
    int lnum = 1;
    while(cline) {
        const char* line = (const char*)cline->p;
        fprintf(fl, "%d. %s",lnum,line);
        printf("%d. %s",lnum,line);
        struct CLList* errors = get_errors(lnum);
        while(errors) {
            struct Error *err = (struct Error*)errors->p;
            fprintf(fl, "Error %d: %s\n", err->eid, err->msg);
            printf("Error %d: %s\n", err->eid, err->msg);
            errors = errors->next;
        }
        cline = cline->next;
        lnum++;
    }
    printf("finished!\n");
}
