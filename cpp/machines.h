#ifndef MACHINES_H
#define MACHINES_H

#include "tokens.h"


/* Identifier machines */
struct Token *machine_idres(int line, char **f, char **l);

/* Operator machines */
struct Token *machine_relop(int line, char **f, char **l);
struct Token *machine_addop(int line, char **f, char **l);
struct Token *machine_mulop(int line, char **f, char **l);

/* Type machines */
struct Token *machine_nint(int line, char **f, char **l);
struct Token *machine_nreal(int line, char **f, char **l);
struct Token *machine_nlreal(int line, char **f, char **l);
struct Token *machine_string(int line, char **f, char **l);

/* Other machines */
struct Token *machine_whitespace(int line, char **f, char **l);
struct Token *machine_catchall(int line, char **f, char **l);

#endif
