#include "error.h"
#include "cstring.h"
#include "tokens.h"
#include "parse.h"

struct CLList* etable = 0;

bool init_error_reporting() {
    return true;
}

bool report_error(int line, int ERR_ID) {
    struct Error *ne = error_f(line,ERR_ID,get_error_msg(ERR_ID));
    cllist_append(&etable, (int)ne);
}

struct CLList* get_errors(int line) {
    struct CLList *lerrs = 0;
    struct CLList *ce = etable;
    if(!ce) return 0;
    do {
        struct Error *e = (struct Error*)ce->p;
        if(e->line == line) cllist_append(&lerrs, (int)e);
        ce = ce->next;
    } while(ce);
    return (struct CLList*)lerrs;
}

const char* get_error_msg(int ERR_ID) {
    if(2000 <= ERR_ID && ERR_ID < 4000) return gen_error_msg(ERR_ID);
    switch (ERR_ID) {
    case LEXERR_UNREC:
        return "LEXERR: Unrecognized symbol";
    case LEXERR_LONGINT:
        return "LEXERR: Integer too large";
    case LEXERR_LONGREAL:
        return "LEXERR: Real too large";
    case LEXERR_LONGE:
        return "LEXERR: Longreal too large";
    case LEXERR_INVALIDINT:
        return "LEXERR: Invalid integer";
    case LEXERR_INVALIDREAL:
        return "LEXERR: Invalid real";
    case LEXERR_INVALIDE:
        return "LEXERR: Invalid longreal";
    case LEXERR_LEADZERO:
        return "LEXERR: Leading zeros";
    case LEXERR_TAILZERO:
        return "LEXERR: Trailing zeros";
    case LEXERR_LONGRSID:
        return "LEXERR: ID is too long";
    case SYNERR_UNREC:
        return "SYNERR: Unexpected token";
    case SEMERR_NOTINSCOPE:
        return "SEMERR: Attempt to access variable undeclared in scope";
    case SEMERR_PROCINSCOPE:
        return "SEMERR: Procedure already declared in scope";
    case SEMERR_VARINSCOPE:
        return "SEMERR: Variable already declared in scope";
    case SEMERR_TOOMANYPARAMS:
        return "SEMERR: Too many parameters provided in declaration of procedure";
    case SEMERR_NOTENOUGHPARAMS:
        return "SEMERR: Not enough parameters provided in declaration of procedure";
    case SEMERR_PROCNOTINSCOPE:
        return "SEMERR: Procedure not found inside scope";
//    case SYNERR_UNREC:
//        return cstrcat("SYNERR: Unexpected token: ", tok->lexeme);
//    case SYNERR_PROG:
//        return cstrcat("SYNERR: program: ", tok->lexeme);
//    case SYNERR_PROG1:
//        return cstrcat("SYNERR: program': ", tok->lexeme);
//    case SYNERR_PROG2:
//        return cstrcat("SYNERR: program'': ", tok->lexeme);
//    case SYNERR_INDENTLIST:
//        return cstrcat("SYNERR: identifer_list: ", tok->lexeme);
//    case SYNERR_INDENTLIST1:
//        return cstrcat("SYNERR: identifier_list': ", tok->lexeme);
//    case SYNERR_DECLS:
//        return cstrcat("SYNERR: declarations: ", tok->lexeme);
//    case SYNERR_DECLS1:
//        return cstrcat("SYNERR: declarations': ", tok->lexeme);
//    case SYNERR_TYPE:
//        return cstrcat("SYNERR: type: ", tok->lexeme);
//    case SYNERR_STDTYPE:
//        return cstrcat("SYNERR: standard_type: ", tok->lexeme);
//    case SYNERR_SUBPDECLS:
//        return cstrcat("SYNERR: subprogram_declarations: ", tok->lexeme);
//    case SYNERR_SUBPDECLS1:
//        return cstrcat("SYNERR: subprogram_declarations': ", tok->lexeme);
//    case SYNERR_SUBPDECL:
//        return cstrcat("SYNERR: subprogram_declaration: ", tok->lexeme);
//    case SYNERR_SUBPDECL1:
//        return cstrcat("SYNERR: subprogram_declarations': ", tok->lexeme);
//    case SYNERR_SUBPDECL2:
//        return cstrcat("SYNERR: subprogram_declarations'': ", tok->lexeme);
//    case SYNERR_SUBPHEAD:
//        return cstrcat("SYNERR: subprogram_head: ", tok->lexeme);
//    case SYNERR_SUBPHEAD1:
//        return cstrcat("SYNERR: subprogram_head': ", tok->lexeme);
//    case SYNERR_ARGS:
//        return cstrcat("SYNERR: arguments: ", tok->lexeme);
//    case SYNERR_PARAMLIST:
//        return cstrcat("SYNERR: parameter_list: ", tok->lexeme);
//    case SYNERR_PARAMLIST1:
//        return cstrcat("SYNERR: parameter_list': ", tok->lexeme);
//    case SYNERR_CMPDSTMT:
//        return cstrcat("SYNERR: compound_statement: ", tok->lexeme);
//    case SYNERR_CMPDSTMT1:
//        return cstrcat("SYNERR: compound_statement': ", tok->lexeme);
//    case SYNERR_OSTMTS:
//        return cstrcat("SYNERR: optional_statements: ", tok->lexeme);
//    case SYNERR_STMTLST:
//        return cstrcat("SYNERR: statement_list: ", tok->lexeme);
//    case SYNERR_STMTLST1:
//        return cstrcat("SYNERR: statement_list': ", tok->lexeme);
//    case SYNERR_STMT:
//        return cstrcat("SYNERR: statement: ", tok->lexeme);
//    case SYNERR_STMT1:
//        return cstrcat("SYNERR: statement': ", tok->lexeme);
//    case SYNERR_VAR:
//        return cstrcat("SYNERR: variable: ", tok->lexeme);
//    case SYNERR_VAR1:
//        return cstrcat("SYNERR: variable': ", tok->lexeme);
//    case SYNERR_PROCSTMT:
//        return cstrcat("SYNERR: procedure_statement: ", tok->lexeme);
//    case SYNERR_PROCSTMT1:
//        return cstrcat("SYNERR: procedure_statement': ", tok->lexeme);
//    case SYNERR_EXPRLIST:
//        return cstrcat("SYNERR: expression_list: ", tok->lexeme);
//    case SYNERR_EXPRLIST1:
//        return cstrcat("SYNERR: expression_list': ", tok->lexeme);
//    case SYNERR_EXPR:
//        return cstrcat("SYNERR: expression: ", tok->lexeme);
//    case SYNERR_EXPR1:
//        return cstrcat("SYNERR: expression': ", tok->lexeme);
//    case SYNERR_SEXPR:
//        return cstrcat("SYNERR: simple_expression: ", tok->lexeme);
//    case SYNERR_SEXPR1:
//        return cstrcat("SYNERR: simple_expression': ", tok->lexeme);
//    case SYNERR_TERM:
//        return cstrcat("SYNERR: term: ", tok->lexeme);
//    case SYNERR_TERM1:
//        return cstrcat("SYNERR: term': ", tok->lexeme);
//    case SYNERR_FACTOR:
//        return cstrcat("SYNERR: factor: ", tok->lexeme);
//    case SYNERR_FACTOR1:
//        return cstrcat("SYNERR: factor': ", tok->lexeme);
//    case SYNERR_SIGN:
//        return cstrcat("SYNERR: sign: ", tok->lexeme);
    default:
        return "Unrecognized error";
    }
}

struct Error *error_f(int line, int eid, char* msg) {
    struct Error *n = (struct Error*)malloc(sizeof(struct Error));
    n->line = line;
    n->eid = eid;
    n->msg = msg;
    return n;
}
