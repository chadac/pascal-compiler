#ifndef PARSE_H
#define PARSE_H

#include "stack.h"
#include "global.h"

struct Token* tok;
int parse_line;
void parse();
int match(unsigned int T);
bool synch(int A[], int N, int T);
int lookup(int *p);
int get_number_type(int *entry);
int get_number_a(int p);

int check_add_blue_node(int entry);
int check_add_green_node(int entry, int type, int offset, int width);
int add_subproc(int entry, int parent);
int in_scope(int entry);

struct Alloc {
    struct Symbol* s;
    int type;
    int offset;
    int width;
    char* proc;
};
struct CLList* allocated;
struct Alloc *alloc_f(struct Symbol* s, int type, int offset, int width, char* proc);
void write_allocation_table();


//stuff related to parameter checking
struct CLList *param_list;
int push_params(int entry);
int pop_param(int err);
int pop_params(int err);

#endif // PARSE_H
