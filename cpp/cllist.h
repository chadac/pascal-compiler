#ifndef CLLIST_H
#define CLLIST_H

struct CLList {
    int p;
    struct CLList *next;
    struct CLList *prev;
};

struct CLList *cllist_f();
//struct CLLIst *cllist_f(int item);

//int llist_add(CLList **root, int p, int at);
int cllist_append(struct CLList **root, int p);
int cllist_pop(struct CLList **root);
int cllist_push(struct CLList **root, int p);

int cllist_at(struct CLList **root, int at);
int cllist_last(struct CLList **root);

#endif // CLLIST_H
