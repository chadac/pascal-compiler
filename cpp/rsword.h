#ifndef RSWORD_H
#define RSWORD_H

#include "global.h"

struct RsWord {
    const char *word;
    int tag_id;
    int attr;
};

struct RsWord *rsword_f(const char* word, int tag_id, int attr);

void rstable_i();
struct RsWord *rstable_find_word(char* fword);
void rstable_print();
void _rstable_add_item(unsigned char *word, int tag_id, int attr);
void _rstable_fopen(const char* fname);
int rstable_test();

#endif // RSWORD_H
