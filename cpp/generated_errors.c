#include "generated_errors.h"

const char* gen_error_msg(int ERR_ID) {
     switch(ERR_ID) {
          case SYNERR_5:
               return "SYNERR: Expected T_PROG.";
          case SYNERR_6:
               return "SYNERR: Expected T_PROC, T_BEG, T_VAR.";
          case SYNERR_7:
               return "SYNERR: Expected T_PROC, T_BEG.";
          case SYNERR_8:
               return "SYNERR: Expected T_ID.";
          case SYNERR_9:
               return "SYNERR: Expected T_COM, T_RPAR.";
          case SYNERR_10:
               return "SYNERR: Expected T_VAR, T_EP.";
          case SYNERR_11:
               return "SYNERR: Expected T_VAR, T_BEG, T_PROC.";
          case SYNERR_12:
               return "SYNERR: Expected T_REAL, T_INT, T_ARR.";
          case SYNERR_13:
               return "SYNERR: Expected T_INT, T_REAL.";
          case SYNERR_14:
               return "SYNERR: Expected T_EP, T_PROC.";
          case SYNERR_15:
               return "SYNERR: Expected T_PROC, T_BEG.";
          case SYNERR_16:
               return "SYNERR: Expected T_PROC.";
          case SYNERR_17:
               return "SYNERR: Expected T_PROC, T_BEG, T_VAR.";
          case SYNERR_18:
               return "SYNERR: Expected T_PROC, T_BEG.";
          case SYNERR_19:
               return "SYNERR: Expected T_PROC.";
          case SYNERR_20:
               return "SYNERR: Expected T_LPAR, T_SC.";
          case SYNERR_21:
               return "SYNERR: Expected T_LPAR.";
          case SYNERR_22:
               return "SYNERR: Expected T_ID.";
          case SYNERR_23:
               return "SYNERR: Expected T_SC, T_RPAR.";
          case SYNERR_24:
               return "SYNERR: Expected T_BEG.";
          case SYNERR_25:
               return "SYNERR: Expected T_ID, T_BEG, T_IF, T_CALL, T_WH, T_END.";
          case SYNERR_26:
               return "SYNERR: Expected T_ID, T_BEG, T_IF, T_CALL, T_WH.";
          case SYNERR_27:
               return "SYNERR: Expected T_ID, T_BEG, T_IF, T_CALL, T_WH.";
          case SYNERR_28:
               return "SYNERR: Expected T_SC, T_END.";
          case SYNERR_29:
               return "SYNERR: Expected T_ID, T_CALL, T_BEG, T_WH, T_IF.";
          case SYNERR_30:
               return "SYNERR: Expected T_SC, T_END, T_EL.";
          case SYNERR_31:
               return "SYNERR: Expected T_ID.";
          case SYNERR_32:
               return "SYNERR: Expected T_ASS, T_LBR.";
          case SYNERR_33:
               return "SYNERR: Expected T_CALL.";
          case SYNERR_34:
               return "SYNERR: Expected T_END, T_EL, T_SC, T_LPAR.";
          case SYNERR_35:
               return "SYNERR: Expected T_PLS, T_ID, T_MIN, T_LPAR, T_NUM, T_NOT.";
          case SYNERR_36:
               return "SYNERR: Expected T_COM, T_RPAR.";
          case SYNERR_37:
               return "SYNERR: Expected T_LPAR, T_ID, T_MIN, T_PLS, T_NUM, T_NOT.";
          case SYNERR_38:
               return "SYNERR: Expected T_DO, T_EL, T_RPAR, T_RBR, T_END, T_COM, T_TH, T_SC, T_REL.";
          case SYNERR_39:
               return "SYNERR: Expected T_ID, T_NUM, T_LPAR, T_NOT, T_PLS, T_MIN.";
          case SYNERR_40:
               return "SYNERR: Expected T_ADD, T_DO, T_EL, T_REL, T_RPAR, T_RBR, T_END, T_COM, T_TH, T_SC.";
          case SYNERR_41:
               return "SYNERR: Expected T_ID, T_NUM, T_LPAR, T_NOT.";
          case SYNERR_42:
               return "SYNERR: Expected T_MUL, T_DO, T_EL, T_REL, T_RPAR, T_RBR, T_END, T_COM, T_ADD, T_SC, T_TH.";
          case SYNERR_43:
               return "SYNERR: Expected T_NUM, T_NOT, T_LPAR, T_ID.";
          case SYNERR_44:
               return "SYNERR: Expected T_DO, T_EL, T_REL, T_MUL, T_RPAR, T_RBR, T_END, T_COM, T_ADD, T_SC, T_TH, T_LPAR, T_LBR.";
          case SYNERR_45:
               return "SYNERR: Expected T_PLS, T_MIN.";
          case SEMERR_1:
               return "SEMERR: Attempt to assign variable to expression of different type.";
          case SEMERR_2:
               return "SEMERR: Expected boolean in while expression.";
          case SEMERR_3:
               return "SEMERR: Expected boolean for if expression.";
          case SEMERR_4:
               return "SEMERR: Attempt to access index of non-array type.";
          case SEMERR_5:
               return "SEMERR: Attempt to access array with non-integer type.";
          case SEMERR_6:
               return "SEMERR: No such procedure in scope";
          case SEMERR_7:
               return "SEMERR: Invalid argument type supplied to procedure.";
          case SEMERR_8:
               return "SEMERR: Invalid argument type supplied to procedure.";
          case SEMERR_9:
               return "SEMERR: Invalid type parameters supplied.";
          case SEMERR_10:
               return "SEMERR: Minus operator applied to incorrect operands";
          case SEMERR_11:
               return "SEMERR: Plus operator applied to incorrect operands";
          case SEMERR_12:
               return "SEMERR: Or operator applied to non-boolean operands";
          case SEMERR_13:
               return "SEMERR: And operator applied to non-boolean operands";
          case SEMERR_14:
               return "SEMERR: Times operator applied to non-real operands";
          case SEMERR_15:
               return "SEMERR: Integer division operator applied to non-integer operands";
          case SEMERR_16:
               return "SEMERR: Division operator applied to non-real operands";
          case SEMERR_17:
               return "SEMERR: Factor must be of boolean type.";
          case SEMERR_18:
               return "SEMERR: Procedures do not have return values";
          case SEMERR_19:
               return "SEMERR: Attempt to access non-array variable.";
          case SEMERR_20:
               return "SEMERR: Array index must be an int.";
          default:
               return "Unrecognized error.";
     }
}
