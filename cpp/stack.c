#include "stack.h"
#include "global.h"
#include "cstring.h"
#include "type.h"
#include "symbol.h"

struct StackItem* stackitem_f(int color, const char *name, int type) {
    struct StackItem* i = (struct StackItem*)malloc(sizeof(struct StackItem));
    i->color = color;
    i->name = cstrcpy(name);
    i->type = type;
    i->params = 0;
    i->subprocs = 0;
    return i;
}

void stackitem_d(struct StackItem *s) {
    free(s->name);
    free(s);
}

struct StackItem* check_params(struct StackItem *node, const char* name) {
    struct CLList *item = node->params;
    while(item) {
        struct StackItem *s = (struct StackItem*)(item->p);
        if(strcmp(s->name,name) == 0)
            return s;
        item = item->next;
    }
    return 0;
}

struct StackItem* check_stack(const char *name) {
    struct CLList *item = stack_list;
    while(item) {
        struct StackItem *s = (struct StackItem*)item->p;
        if(strcmp(s->name,name) == 0)
            return s;
        struct StackItem *p = check_params(s, name);
        if(p)
            return p;
        //if(s->color == STACK_BLUE_NODE)
        //    return 0;
        item = item->next;
    }
    return 0;
}

struct StackItem* check_stack_scope(const char *name) {
    struct CLList *item = stack_list;
    while(item) {
        struct StackItem *s = (struct StackItem*)item->p;
        if(strcmp(s->name,name) == 0)
            return s;
        struct StackItem *p = check_params(s, name);
        if(p)
            return p;
        //if(s->color == STACK_BLUE_NODE)
        //    return 0;
        item = item->next;
    }
    return 0;
}

int push_blue_node(const char *name) {
    struct StackItem* i = stackitem_f(STACK_BLUE_NODE, name, TYPE_VOID);
    cllist_push(&stack_list, i);
    return (int)i;
}

int push_green_node(const char *name, int type) {
    struct StackItem* i = stackitem_f(STACK_GREEN_NODE, name, type);
    cllist_push(&stack_list, i);
    return type;
}

struct StackItem* pop_stack() {
    struct StackItem* i = (struct StackItem*)cllist_pop(&stack_list);
    return i;
}

struct StackItem* pop_blue_node() {
    struct StackItem* i;
    do {
        i = pop_stack();
    } while(stack_list && i->color != STACK_BLUE_NODE);
    return i;
}

struct StackItem* get_blue_node() {
    struct CLList* s = stack_list;
    while(s) {
        struct StackItem *i = (struct StackItem*)s->p;
        if(i->color == STACK_BLUE_NODE) return i;
        s = s->next;
    }
    return 0;
}

struct StackItem* find_node(const char* name) {
    struct CLList* s = stack_list;
    while(s) {
        struct StackItem *i = (struct StackItem*)s->p;
        if(strcmp(i->name, name) == 0) return i;
        s = s->next;
    }
    return 0;
}

struct StackItem* find_blue_node(const char* name) {
    struct CLList* s = stack_list;
    while(s) {
        struct StackItem *i = (struct StackItem*)s->p;
        if(strcmp(i->name, name) == 0 && i->color == STACK_BLUE_NODE) return i;
        s = s->next;
    }
    return 0;
}

struct StackItem* find_green_node(const char* name) {
    struct CLList* s = stack_list;
    while(s) {
        struct StackItem *i = (struct StackItem*)s->p;
        if(strcmp(i->name, name) == 0 && i->color == STACK_GREEN_NODE) return i;
        s = s->next;
    }
    return 0;
}


int add_param(int entry, int type) {
    struct StackItem* s = get_blue_node();
    struct Symbol *i = (struct Symbol*)entry;
    cllist_append(&s->params, stackitem_f(STACK_GREEN_NODE, i->name, type));
    return type;
}
