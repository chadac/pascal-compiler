#ifndef STACK_H
#define STACK_H

#include "cllist.h"

#define STACK_BLUE_NODE 1 //procedures or subprocedures
#define STACK_GREEN_NODE 2 //member variables

//stuff related to the stack
struct StackItem {
    int color;
    int type;
    char* name;
    struct CLList* params;
    struct CLList* subprocs;
};

struct CLList *stack_list;

struct StackItem* stackitem_f(int color, const char* name, int type);
void stackitem_d(struct StackItem* s);

struct StackItem* check_stack(const char *name);
struct StackItem* check_stack_scope(const char *name);
int push_blue_node(const char* name);
int push_green_node(const char* name, int type);

struct StackItem* pop_stack();
struct StackItem* pop_blue_node();
struct StackItem* get_blue_node();
struct StackItem* find_node(const char* name);
struct StackItem* find_blue_node(const char* name);
struct StackItem* find_green_node(const char* name);

int add_param(int entry, int type);

#endif // STACK_H

