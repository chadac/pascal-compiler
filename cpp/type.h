#ifndef TYPE_H
#define TYPE_H

#define TYPE_NONE 0
#define TYPE_ERR 1
#define TYPE_VOID 2
#define TYPE_INT 3
#define TYPE_REAL 4
#define TYPE_AINT 5
#define TYPE_AREAL 6
#define TYPE_BOOLEAN 7

#endif // TYPE_H
