#include "parse.h"
#include "tokens.h"
#include "error.h"

int* n_prog(int n_prog_1_offset);

int* n_progp(int n_progp_1_parent,int n_progp_1_offset);

int* n_progpp(int n_progpp_1_offset,int n_progpp_1_parent);

int* n_idtl();

int* n_idtlp();

int* n_decl(int n_decl_1_offset);

int* n_declp(int n_declp_1_offset);

int* n_type();

int* n_stdt();

int* n_spds(int n_spds_1_parent,int n_spds_1_offset);

int* n_spdsp(int n_spdsp_1_offset,int n_spdsp_1_parent);

int* n_spd(int n_spd_1_offset,int n_spd_1_parent);

int* n_spdp(int n_spdp_1_offset,int n_spdp_1_parent);

int* n_spdpp(int n_spdpp_1_offset,int n_spdpp_1_parent);

int* n_sph(int n_sph_1_offset,int n_sph_1_parent);

int* n_sphp(int n_sphp_1_offset,int n_sphp_1_i);

int* n_args(int n_args_1_offset);

int* n_plst();

int* n_plstp();

int* n_cmpd();

int* n_cmpdp();

int* n_ostmt();

int* n_stmtlst();

int* n_stmtlstp();

int* n_stmt();

int* n_stmtp(int n_stmtp_1_i);

int* n_var();

int* n_varp(int n_varp_1_i);

int* n_proc();

int* n_procp(int n_procp_1_i);

int* n_exprl(int n_exprl_1_i);

int* n_exprlp(int n_exprlp_1_i);

int* n_expr();

int* n_exprp(int n_exprp_1_i);

int* n_sexpr();

int* n_sexprp(int n_sexprp_1_i);

int* n_term();

int* n_termp(int n_termp_1_i);

int* n_fac();

int* n_facp(int n_facp_1_i);

int* n_sign();

