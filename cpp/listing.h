#ifndef LISTING_H
#define LISTING_H
#include "cllist.h"

struct CLList* read_source_file();
void write_listing_file();

#endif // LISTING_H
