#include "machines.h"
#include "rsword.h"
#include "tokens.h"
#include "error.h"
#include "symbol.h"
#include "type.h"

struct Token *machine_idres(int line, char **f, char **l) {
    //i forget what this does; figure out later
    if (!isalpha(**l))
        return 0;
    else ++*l;

    while (**l) {
        if (isalpha(**l) || isdigit(**l))
            ++*l;
        else
            break;
    }

    unsigned const int len = *l - *f;
    char* str = malloc(sizeof(char) * (len+1));
    strncpy(str, *f, len);
    str[len] = 0;

    if (len > 10) {
        return token_f(line, str, T_LEXERR, LEXERR_LONGRSID);
    }
    struct RsWord *rw; //why is this her?e????
    if ((rw = rstable_find_word(str))) {
        return token_f(line, str, rw->tag_id, rw->attr);
    }
    else {
        //INCOMPLETE: add this to the symbol table, return pointer
        //TODO: above
        int a = (int)add_symbol(str);
        return token_f(line, str, T_ID, a);
    }
}

struct Token *machine_relop(int line, char **f, char **l) {
    if (**l == '<') {
        ++*l;
        if (**l == '=') {
            ++*l;
            return token_f(line, "<=", T_REL, ROP_LE);
        }
        else if (**l == '>') {
            ++*l;
            return token_f(line, "<>", T_REL, ROP_NEQ);
        }
        else {
            return token_f(line, "<", T_REL, ROP_LT);
        }
    }
    else if (**l == '>') {
        ++*l;
        if (**l == '=') {
            ++*l;
            return token_f(line, ">=", T_REL, ROP_GE);
        }
        else {
            return token_f(line, ">", T_REL, ROP_GT);
        }
    }
    else if (**l == '=') {
        ++*l;
        return token_f(line, "=", T_REL, ROP_EQ);
    }
    else if (**l == ':') {
        ++*l;
        if (**l == '=') {
            ++*l;
            return token_f(line, ":=", T_ASS, 0);
        }
        return token_f(line, ":", T_C, 0);
    }
    return 0;
}

struct Token *machine_addop(int line, char **f, char **l) {
    if (**l == '+') {
        ++*l;
        return token_f(line, "+", T_ADD, AOP_PLUS);
    }
    else if (**l == '-') {
        ++*l;
        return token_f(line, "-", T_ADD, AOP_MINUS);
    }
    return 0;
}

struct Token *machine_mulop(int line, char **f, char **l) {
    if (**l == '*') {
        ++*l;
        return token_f(line, "*", T_MUL, MOP_TIMES);
    }
    else if (**l == '/') {
        ++*l;
        return token_f(line, "/", T_MUL, MOP_DIV);
    }
    return 0;
}

struct Token *machine_nint(int line, char **f, char **l) {
    if (!isdigit(**l)) {
        return 0;
    }
    else ++*l;
    bool zflag = (**f == '0');

    while (**l) {
        if (isdigit(**l)) ++*l;
        else break;
    }
    int len = *l - *f;
    char* str = strcpyn(*f, len);
    if (len > 10) {
        return token_f(line, str, T_LEXERR, LEXERR_LONGINT);
    }
    else if (len > 1 && zflag) {
        return token_f(line, str, T_LEXERR, LEXERR_LEADZERO);
    }
    else {
        return token_f(line, str, T_NUM, (int)number_f(TYPE_INT, atoi(str), 0, 0));
    }
}
struct Token *machine_nreal(int line, char **f, char **l) {
    if (!isdigit(**l)) {
        return 0;
    }
    else ++*l;
    bool z1flag = **f == '0';

    while (**l) {
        if (isdigit(**l)) ++*l;
        else break;
    }
    if (**l != '.') {
        *l = *f;
        return 0;
    }
    int alen = *l - *f;
    char* a = strcpyn(*f, alen);
    ++*l;
    char* bf = *l;
    while (**l) {
        if (isdigit(**l)) ++*l;
        else break;
    }
    bool z2flag = (*(*l-1) == '0');
    int blen = *l - bf;
    char* str = strcpyn(*f, *l - *f);
    if (blen == 0) {
        *l = *f;
        return 0;
        //return token_f(line, str, TK_LEXERR, LEXERR_INVALIDREAL);
    }
    else if (alen > 5 || blen > 5) {
        free(a);
        return token_f(line, str, T_LEXERR, LEXERR_LONGREAL);
    }
    else if (alen > 1 && z1flag) {
        free(a);
        return token_f(line, str, T_LEXERR, LEXERR_LEADZERO);
    }
    else if (blen > 1 && z2flag) {
        free(a);
        return token_f(line, str, T_LEXERR, LEXERR_TAILZERO);
    }
    else {
        char* b = strcpyn(*f, blen);
        //free(a); free(b);
        return token_f(line, str, T_NUM, (int)number_f(TYPE_REAL, atoi(a),atoi(b),0));
    }
}
struct Token *machine_nlreal(int line, char **f, char **l) {
    if (!isdigit(**l)) {
        return 0;
    }
    else ++*l;
    bool z1flag = **f == '0';

    while (**l) {
        if (isdigit(**l)) ++*l;
        else break;
    }
    if (**l != '.') {
        *l = *f;
        return 0;
    }
    int alen = *l - *f;
    char* a = strcpyn(*f, alen);
    ++*l;
    char* bf = *l;
    while (**l) {
        if (isdigit(**l)) ++*l;
        else break;
    }
    bool z2flag = (*(*l-1) == '0');
    int blen = *l - bf;
    if (**l != 'E' || blen == 0) {
        *l = *f;
        return 0;
    }
    else ++*l;
    char* cf = *l;

    char sign = 1;
    if (**l == '-')
        sign = -1;
    if (**l == '+' || **l == '-')
        ++*l;

    bool z3flag = (**l == '0');
    while (**l) {
        if (isdigit(**l)) ++*l;
        else break;
    }

    int clen = *l - cf;
    char* str = strcpyn(*f, *l - *f);
    if (clen == 0) {
        free(a);
        free(str);
        *l = *f;
        return 0;
        //return token_f(line, str, TK_LEXERR, LEXERR_INVALIDE);
    }
    else if (alen > 5 || blen > 5 || clen > 2) {
        free(a);
        return token_f(line, str, T_LEXERR, LEXERR_LONGE);
    }
    else if (alen > 1 && z1flag) {
        free(a);
        return token_f(line, str, T_LEXERR, LEXERR_LEADZERO);
    }
    else if (blen > 1 && z2flag) {
        free(a);
        return token_f(line, str, T_LEXERR, LEXERR_TAILZERO);
    }
    else if (clen > 1 && z3flag) {
        free(a);
        return token_f(line, str, T_LEXERR, LEXERR_LEADZERO);
    }
    else {
        char* b = strcpyn(bf, blen);
        char* c = strcpyn(cf, clen);
        int ia = atoi(a), ib = atoi(b), ic = atoi(c);
        free(a); free(b); free(c);
        return token_f(line, str, T_NUM, (int)number_f(TYPE_REAL, ia, ib, ic));
    }
}
struct Token *machine_string(int line, char **f, char **l);

struct Token *machine_whitespace(int line, char **f, char **l) {
    while (**l) {
        if (**l != ' ' && **l != '\t')
            break;
        ++*l;
    }
    if (*l - *f == 0)
        return 0;
    char* str = strcpyn(*f, *l - *f);
    return token_f(line, str, T_EMPTY, 0);
}
struct Token *machine_catchall(int line, char **f, char **l) {
    if (**l == ';') {
        ++*l;
        return token_f(line, ";", T_SC, 0);
    }
    else if (**l == ',') {
        ++*l;
        return token_f(line, ",", T_COM, 0);
    }
    else if (**l == '(') {
        ++*l;
        return token_f(line, "(", T_LPAR, 0);
    }
    else if (**l == ')') {
        ++*l;
        return token_f(line, ")", T_RPAR, 0);
    }
    else if (**l == '[') {
        ++*l;
        return token_f(line, "[", T_LBR, 0);
    }
    else if (**l == ']') {
        ++*l;
        return token_f(line, "]", T_RBR, 0);
    }
    else if (**l == '+') {
        ++*l;
        return token_f(line, "+", T_PLS, 0);
    }
    else if (**l == "-") {
        ++*l;
        return token_f(line, "-", T_MIN, 0);
    }
    else if (**l == '.') {
        ++*l;
        if (**l == '.') {
            ++*l;
            return token_f(line, "..", T_DD, 0);
        }
        return token_f(line, ".", T_DOT, 0);
    }
    else {
        ++*l;
        char* str = strcpyn(*f, 1);
        return token_f(line, str, T_LEXERR, LEXERR_UNREC);
    }
}
